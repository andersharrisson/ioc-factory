The IOC Factory (FACT) is responsible for managing IOCs at the European Spallation Source (ESS). It is designed to 
maximize the Integrated Control System (ICS) divisionís productivity by providing a consistent and automated approach 
on how hundreds of IOCs are configured, generated, browsed and audited which would otherwise have to be performed 
manually. This system is part of the Work Unit 3.1 referring to the Controls Configuration Data Management which 
addresses the collection, storage, and distribution of static data needed to install, commission, and operate the 
control system. By managing, it specifically means the following use cases:

   - Configure IOC
   - Generate IOC
   - Browse IOC
   - Audit IOC

The "Configure IOC" allows the user to select a set of EPICS modules for a certain IOC to use to effectively interface 
its devices. This selection/configuration is stored in a persistence layer and it can later be used to generate the IOC.
In principle, every time that the topology of the IOC evolves (i.e. the layout of devices that the IOC interface 
changes) the user creates a new configuration to cope with this evolution.

The "Generate IOC" allows the generation (i.e. building) of an IOC from scratch according to a certain configuration 
selected by the user. The generated IOC is stored in the official repository server for development or production, 
depending on what the user has selected. The information about the generation is stored in a persistence layer to enable
the browse and audit IOC use cases.

The "Browse IOC" allows the user to retrieve, organize and display information about historical (i.e. past) generation 
of IOCs. It gives a broad view/understanding of when, how and why a certain IOC was generated in the official repository
server and by whom.

Finally, the "Audit IOC" enables the user to track the changes that an IOC (stored in the official repository server)
may have suffered. In other words, it provides him/her with a complete list of files/directories belonging to the IOC
that have been added, modified or removed.
