ALTER DATABASE "iocfactory" OWNER TO "wildfly";
\connect iocfactory
ALTER TABLE "configuration" OWNER TO "wildfly";
ALTER TABLE "device_configuration" OWNER TO "wildfly";
ALTER TABLE "device_parameter" OWNER TO "wildfly";
ALTER TABLE "generate_entry" OWNER TO "wildfly";
ALTER TABLE "ioc" OWNER TO "wildfly";
ALTER TABLE "iocenvironment" OWNER TO "wildfly";
ALTER TABLE "iocfact_setup" OWNER TO "wildfly";
ALTER TABLE "log_entry" OWNER TO "wildfly";
ALTER TABLE "schema_version" OWNER TO "wildfly";
ALTER TABLE "user_setting" OWNER TO "wildfly";
ALTER SEQUENCE "hibernate_sequence" OWNER TO "wildfly";
