/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */


function MacroUpdater (expValueTooltipStyle, csTooltipStyle, dataTableId) {
    this.expValueTooltipStyle = expValueTooltipStyle;
    this.csTooltipStyle = csTooltipStyle;
    this.dataTableId = dataTableId;

    this.updateParamsTblContents = function (xhr, status, args) {
        var paramsUpdate = $.parseJSON(args.paramsUpdate);

        var expTooltipElems = {};
        var csTooltipElems = {};

        $(this.expValueTooltipStyle).each(function (i, t) {
            var tooltip = $(t);
            expTooltipElems[tooltip.text()] = tooltip;
        });

        $(this.csTooltipStyle).each(function (i, t) {
            var tooltip = $(t);
            csTooltipElems[tooltip.text()] = tooltip;
        });

        $(this.dataTableId + ' tr').each(function (i, r) {
            var row = $(r);
            var rowKey = row.attr('data-rk');

            var valueElem = $(row.children()[5]);
            var expValueElem = $(row.children()[6]);
            var csElem = $(row.children()[7]);

            var data = paramsUpdate[rowKey];

            valueElem.find('.ui-cell-editor-output').text(data.value);
            expValueElem.children("span").text(data.expValue);
            csElem.children("span").text(data.csStatus);
            row.removeClass("errorHighlight warnHighlight highlightHighlight").addClass(data.rowStyle);

            // Set tooltip html
            expTooltipElems[rowKey].next().html(data.expValueHtml);

            // Set cs html
            csTooltipElems[rowKey].next().html(data.csStatusRendered);
        });
    }
}

ParamsMacroUpdater = new MacroUpdater('.param_exp_value_tooltip', '.param_cs_tooltip', '#paramsForm\\:paramsTbl_data');

function refreshDevicesAndParams() {
    PF('globalsTbl').filter();
    PF('devicesTbl').filter();
    PF('paramsTbl').filter();
}

function refreshBelowIOCs() {
    PF('configsTbl').filter();
    refreshDevicesAndParams();
}

function refreshAll() {
    PF('iocsTbl').filter();
    refreshBelowIOCs();
}


function handleGlobalAddRequest(xhr, status, args) {
    if (args.validationFailed) {
        PF('addGlobalDlg').getJQ().effect("shake");
    } else {
        PF('addGlobalDlg').hide();
        refreshDevicesAndParams();
    }
}

function handleGlobalEditRequest(xhr, status, args) {
    if (args.validationFailed) {
        PF('editGlobalDlg').getJQ().effect("shake");
    } else {
        PF('editGlobalDlg').hide();
        refreshDevicesAndParams();
    }
}

function handleRemoveParameter(xhr, status, args) {
    // If deletion successful update the filters (update the table)
    if (!args.validationFailed) {
        PF('paramsTbl').filter();
    }
}

function handleRenameDevRequest(xhr, status, args) {
    if (args.validationFailed) {
        PF('renameDevDlg').getJQ().effect("shake");
    } else {
        PF('renameDevDlg').hide();
        refreshDevicesAndParams();
    }
}

function handleDeviceEdit(xhr, status, args) {
    if (args.validationFailed) {
        PF('editDeviceDlg').getJQ().effect("shake");
    } else {
        PF('editDeviceDlg').hide();
        refreshDevicesAndParams();
    }
}

function handleEEESwitch() {
    PF('confirmEEESwitchDlg').show();
}

function handleMenuSwitch() {
    PF('confirmMenuSwitchDlg').show();
}