/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

var queryParam = (function (a) {
    if (a === "")
        return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p = a[i].split('=', 2);
        if (p.length === 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));


window.onload = function () {
    if (queryParam['showmessages'] === 'Production') {
        PF('prodMessages').show();
    } else if (queryParam['showmessages'] === 'Sandbox') {
        PF('sboxMessages').show();
    }
};

function handleAddRequest(xhr, status, args) {
    if (args.validationFailed) {
        PF('addEnvDlg').getJQ().effect("shake");
    } else {
        PF('addEnvDlg').hide();
    }
}

function handleEditRequest(xhr, status, args) {
    if (args.validationFailed) {
        PF('editEnvDlg').getJQ().effect("shake");
    } else {
        PF('editEnvDlg').hide();
    }
}