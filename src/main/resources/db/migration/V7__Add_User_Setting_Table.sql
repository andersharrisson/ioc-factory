CREATE TABLE user_setting
(
  id bigint NOT NULL,
  setting_key character varying(255),
  username character varying(255),
  setting_value character varying(255),
  CONSTRAINT user_setting_pkey PRIMARY KEY (id),
  CONSTRAINT idx_user_setting_username_key UNIQUE (username, setting_key)
);
