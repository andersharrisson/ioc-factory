/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.Optional;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.ui.RepositorySelector;

@ApplicationScoped
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Named
@PermitAll
/**
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class RepositoryProducer {
    private final static Logger LOG = Logger.getLogger(RepositoryProducer.class.getCanonicalName());

    private final static String SESSION_REPO_PREFIX = "iocfactory.repository.";
    private final static String SESSION_REPO_HASH_PREFIX = "iocfactory.repository.hash.";
    

    @Inject private transient CachedSetupService setupService;
    @Inject private Instance<RepositorySelector> repoSelectorSource;

    @Inject private Instance<SecurityService> securityService;
    @Inject private Instance<HttpSession> httpSession;

    @Inject private PeriodicRepoScanner periodicScanner;
   
    public Repository getRepository() {
        return getSpecificRepo(getCurrentEEEType());
    }

    /**
     * Called during ViewScope initialization to update the repo
     */
    public void checkRepoChanged() {
        if (!httpSession.isUnsatisfied()) {
            final EEEType eeeType = getCurrentEEEType();
            
            final Optional<Pair<Repository, String>> sessionRepoAndHash = getSessionRepoAndHash(eeeType);
            final Pair<Repository, String> appRepoAndHash = periodicScanner.getAppScopedRepoAndHash(eeeType);
            final Repository appRepo = appRepoAndHash.getLeft();
            final String appRepoHash = appRepoAndHash.getRight();
                        
            if (!sessionRepoAndHash.isPresent() || !StringUtils.equals(appRepoHash, 
                    sessionRepoAndHash.get().getRight())) {
                setSessionRepo(eeeType, appRepo, appRepoHash);
            }
        }
    }
    
    public synchronized Repository getSpecificRepo(final EEEType eeeType) {
        // If session exists
        if (!httpSession.isUnsatisfied()) {
            final Optional<Pair<Repository, String>> sessionRepoAndHash = getSessionRepoAndHash(eeeType);
                        
            if (sessionRepoAndHash.isPresent()) {
                return sessionRepoAndHash.get().getLeft();
            } else {
                final Pair<Repository, String> appRepoAndHash = periodicScanner.getAppScopedRepoAndHash(eeeType);
                setSessionRepo(eeeType, appRepoAndHash.getLeft(), appRepoAndHash.getRight());
                return appRepoAndHash.getLeft();
            }
        } else {
            return periodicScanner.getAppScopedRepoAndHash(eeeType).getLeft();
        }
    }

    public void refreshSessionRepository(final EEEType eeeType) {
        // Trigger manual check
        if (periodicScanner.manualCheck(eeeType)) {        
            final Pair<Repository, String> appRepoAndHash = periodicScanner.getAppScopedRepoAndHash(eeeType);
            setSessionRepo(eeeType, appRepoAndHash.getLeft(), appRepoAndHash.getRight());
        }
    }
    
    private EEEType getCurrentEEEType() {
        EEEType toUse = EEEType.PRODUCTION;
        if (repoSelectorSource.isUnsatisfied()) {
            LOG.warning("RepositorySelector dependency unsatisfied (Session scope unavailable?). " +
                    "Defaulting to PRODUCTION EEE Repo always.");
        } else {
            toUse = repoSelectorSource.get().getSelectedEee();
        }
        return toUse;
    }
    
    private Optional<Pair<Repository, String>> getSessionRepoAndHash(final EEEType type) {
        Repository sessionRepo;
        String sessionHash;
        try {
            final HttpSession hs = httpSession.get();
            sessionRepo = (Repository) hs.getAttribute(SESSION_REPO_PREFIX + type.getName());
            sessionHash = (String) hs.getAttribute(SESSION_REPO_HASH_PREFIX + type.getName());
        } catch (IllegalStateException is) {
            // This happens when we have logged out, just return null to signal not available
            sessionRepo = null;
            sessionHash = null;
        }
        return sessionRepo != null ? Optional.of(new ImmutablePair<>(sessionRepo, sessionHash)) : Optional.empty();
    }
    
    
    private void setSessionRepo(final EEEType type, final Repository repo, final String hash) {
        if (!httpSession.isUnsatisfied()) {
            final HttpSession hs = httpSession.get();
            hs.setAttribute(SESSION_REPO_PREFIX + type.getName(), repo);
            hs.setAttribute(SESSION_REPO_HASH_PREFIX + type.getName(), hash);
        }
    }
}
