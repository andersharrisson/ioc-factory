/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.Date;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.service.cdievents.SetupSaved;
import se.esss.ics.iocfactory.util.Util;

/**
 * Service that is used to store and retrieve IOC Factory configuration
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Stateless
@PermitAll
public class SetupService {
    @PersistenceContext private EntityManager em;

    @EJB private LogService logService;

    @Inject @SetupSaved private Event<Date> setupSavedEvent;

    /**
     * Loads the IOC Factory configuration into {@link IOCFactSetup} object
     *
     * Persists an empty if none found
     * @return the loaded configuration
     */
    public IOCFactSetup loadSetup() {
        IOCFactSetup factSetup = (IOCFactSetup) Util.getSingleResult(
                em.createQuery("from IOCFactSetup s"));

        if (factSetup==null) {
            factSetup=new IOCFactSetup();
            // set defaults
            factSetup.setEpicsBaseDirectory("please change");
            factSetup.setSandboxBaseDirectory("please change");
            factSetup.setCcdbDependencyPropery("EPICSModule");
            factSetup.setIocDeviceTypes("IOC");
            factSetup.setCcdbOsProperty("OperatingSystem");
            factSetup.setCcdbSnippetProperty("EPICSSnippet");
            factSetup.setCcdbHostnameProperty("Hostname");
        }

        return factSetup;
    }

    /**
     * Saves the setup
     * @param setup to save
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION})
    public void saveSetup(IOCFactSetup setup) {
        em.merge(setup);

        // Log audit trail
        logSetup(setup);

        setupSavedEvent.fire(new Date());
    }

    /**
     * Load environments from database
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<IOCEnvironment> loadEnvs() {
        return em.createQuery("from IOCEnvironment e").getResultList();
    }

    /**
     * Adds new environment to the db
     * @param env the new env to add to the db
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION})
    public void addEnv(IOCEnvironment env) {
        em.persist(env);

        // Log audit trail
        logSetup(null);

        setupSavedEvent.fire(new Date());
    }

    /**
     * Saves an IOC environment
     * @param env to save
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION})
    public void saveEnv(IOCEnvironment env) {
        em.merge(env);

        // Log audit trail
        logSetup(null);

        setupSavedEvent.fire(new Date());
    }

    /**
     * Removes an IOC environment from storage
     * @param env to remove
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION})
    public void removeEnv(IOCEnvironment env) {
        final IOCEnvironment mergedEnv = em.merge(env);
        em.remove(mergedEnv);

        // Log audit trail
        logSetup(null);

        setupSavedEvent.fire(new Date());
    }

    private void logSetup(IOCFactSetup setup) {
        logService.logSettingsSaved(setup != null ? setup : loadSetup(), loadEnvs());
    }
}
