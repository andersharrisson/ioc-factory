/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.PlaceholderTrait;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.YamlHelper;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class MachineUniqueVerifier extends ConfigVerifierBase implements ConfigVerifier {
    private static final Logger LOG = Logger.getLogger(MachineUniqueVerifier.class.getName());
    private static final String VERIFIER_CATEGORY = "Macros";

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper<>(false);

    @Inject
    private transient MachineLevelVerifierUtil machineLevelUtil;

    @Override
    public int getPriority() { return 350; }
    
    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }     

    @Override
    public void verifyImpl(final Configuration config, final Repository repo,  final IOCEnvironment env) {
        // 1st step: get all machine unique field names in this config, stored UPPER CASE in the set
        final Set<String> machineUniqueParamNames = extractMachineUniqueParamSet(config, env);
    
        final Map<String, Integer> occurencesMap = new HashMap<>();
        final Map<String, Set<String>> valuesSet = new HashMap<>();
        final Map<String, List<String>> valuesList = new HashMap<>();
        
        // Init datastructures
        machineUniqueParamNames.forEach(paramName -> {
            occurencesMap.put(paramName, 0);
            valuesSet.put(paramName, new HashSet<>());
            valuesList.put(paramName, new ArrayList<>());
        });
        
        // count occurences in Generate Entries and fill set
        machineLevelUtil.otherGenerateEntriesStream(config, env).
                flatMap(ge -> extractGenerateEntryMacrosWithNames(ge, machineUniqueParamNames)).
                forEach(pair -> countUniquesAndOccurences(pair.getLeft(), pair.getRight(), 
                        occurencesMap, valuesSet, valuesList));
     
        // count occurences in this IOC
        extractParametersWithNames(config, machineUniqueParamNames).
                forEach(param -> countUniquesAndOccurences(param.getName(), param.getValue(), occurencesMap, 
                        valuesSet, valuesList));
        
        // process results
        for (String paramName : machineUniqueParamNames) {
            final Integer numOfOccurences = occurencesMap.get(paramName);
            final Set<String> occurences = valuesSet.get(paramName);
            
            if (numOfOccurences != occurences.size()) {
                addVerifyMessage(ConsistencyMsgLevel.ERROR, "Machine-unique macro %s does not have unique value on the "
                        + "machine (hostname). Values are: %s", paramName,
                        valuesList.get(paramName).stream().map(val -> String.format("\"%s\"", val)).
                                collect(Collectors.joining("; ")));
            }
        }
    }
    
    private static <T> void countUniquesAndOccurences(String name, String value, 
            final Map<String, Integer> occurencesMap, 
            final Map<String, Set<String>> values, 
            final Map<String, List<String>> valuesList) {
        if (!StringUtils.isEmpty(value)) {
            occurencesMap.put(name, occurencesMap.get(name) + 1);
            values.get(name).add(value);
            valuesList.get(name).add(value);
        }
    }

    
    private Stream<Pair<String, String>> extractGenerateEntryMacrosWithNames(GenerateEntry ge, Set<String> uniqueOnes) {
        final JsonNode tree = YAML_HELPER.readTree(ge.getDump());
       
        
        return Stream.concat(
                macroElemStreamFromTopNode(tree, "environment"), 
                macroElemStreamFromTopNode(tree, "parameters")
        ).filter(entry -> entry.getValue() != null && entry.getValue().isTextual() && 
                uniqueOnes.contains(StringUtils.upperCase(entry.getKey()))).
                map(entry -> new ImmutablePair<>(StringUtils.upperCase(entry.getKey()), entry.getValue().asText()));
    }
    
    private Stream<Parameter> extractParametersWithNames(Configuration config, Set<String> uniqueOnes) {
        return MacroVerifier.paramsStreamFromConfig(config).
                filter(p -> uniqueOnes.contains(StringUtils.upperCase(p.getName())));
    }
    
    private static Stream<Map.Entry<String,JsonNode>> macroElemStreamFromTopNode(final JsonNode topNode, 
            String nodeNames) {
        if (topNode == null) {
            return Stream.empty();
        } else {
            return topNode.findValues(nodeNames).stream().
                    flatMap(jsonNode -> Util.streamFromIterator(jsonNode.fields()));
        }
    }
    
    
    private static Set<String> extractMachineUniqueParamSet(Configuration config, IOCEnvironment env) {
        return MacroVerifier.paramsStreamFromConfig(config).
                filter(p -> p.getTraits() != null && p.getTraits().contains(PlaceholderTrait.UNIQUE_MACHINE)).
                map(p -> StringUtils.upperCase(p.getName())).collect(Collectors.toSet());
    }

    /**
     * Compares two generate entries. Bigger is considered the one with higher revision (1st) and if same revision,
     * higher entity id (newer entry, 2nd)
     *
     * @param l
     * @param r
     * @return
     */
    public static int genEntryComparator(final GenerateEntry l, final GenerateEntry r) {
        final int revComp = Integer.compare(l.getConfigRevision(), r.getConfigRevision());
        if (revComp != 0) {
            return revComp;
        } else {
            return Long.compare(l.getId(), r.getId());
        }
    }




    
}
