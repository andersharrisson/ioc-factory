/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.ConfigVerifyMessages;
import se.esss.ics.iocfactory.model.ConfigVerifyStatus;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.verifiers.ConfigVerifier;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class VerifyService {
    @Inject
    private Instance<ConfigVerifier> verifiersInstance;
    
    public List<ConfigVerifyStatus> verifyConfiguration(final Configuration config, final IOCEnvironment env) {
        // Resolve macros
        final MacroResolver macroResolver = new MacroResolver();
        macroResolver.setConfiguration(config);
        macroResolver.resolveMacros();

        // Result messages
        final ConfigVerifyMessages messages = new ConfigVerifyMessages();

        // Invoke all verifiers
        final List<ConfigVerifier> verifiers = new ArrayList<>();
        verifiersInstance.forEach(confVerifier -> verifiers.add(confVerifier));
        verifiers.sort((l, r) -> Integer.compare(l.getPriority(), r.getPriority()));

        final Repository repo = Util.getRepository();
        verifiers.forEach(verifier -> verifier.verify(config, messages, repo, env));
        return messages.getMessages();
    }
}
