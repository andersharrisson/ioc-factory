/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.service.verifiers.dependency.ConfigDependencies;
import se.esss.ics.iocfactory.service.verifiers.dependency.DependencyUtils;
import se.esss.ics.iocfactory.service.verifiers.dependency.ModuleSatisfiedPair;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class DependencyVerifier extends ConfigVerifierBase implements ConfigVerifier {
    private static final Logger LOG = Logger.getLogger(DependencyVerifier.class.getName());
    private static final String VERIFIER_CATEGORY = "Dependencies";

    @Override
    public int getPriority() { return 200; }

    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }   
    
    @Override
    public void verifyImpl(final Configuration config, final Repository repo,  final IOCEnvironment env) {

        final ConfigDependencies deps = DependencyUtils.flatConfigDependencies(config, repo);

        addUnsatisfiedDepsMessages(deps);
        addDepConflictMessages(deps);
    }
    
          
    private void addUnsatisfiedDepsMessages(final ConfigDependencies deps) {
        deps.getAllDeps().entrySet().stream().
                filter(entry -> !entry.getValue().isSatisfied()).forEach(entry -> {                    
            LOG.severe(                    
                    addVerifyMessage(ConsistencyMsgLevel.ERROR, 
                            "Unsatisfied dependency %s for module %s", entry.getKey().toString(),
                            DependencyUtils.formatModuleString(entry.getValue().getModule()))
            );
        });
    }

    private void addDepConflictMessages(final ConfigDependencies deps) {
        // Loop state, since deps are sorted by Module first, we'll collect adjecent same modules and 
        // call checkDependencyConflict when module changes
        final DepConflictState state = new DepConflictState();

        for (final Map.Entry<ModuleDependency, ModuleSatisfiedPair> entry : deps.getAllDeps().entrySet()) {
            final ModuleDependency dep = entry.getKey();
            if (!state.moduleName.equals(dep.getName())) {
                // We've went trough all modules with the name, now check
                if (!state.moduleName.isEmpty()) {
                    checkDependencyConflict(state);
                }

                // Reset the state
                state.resetState(dep.getName());
            }
            handleNextDependencyCheck(dep, state);
        }
        if (!state.moduleName.isEmpty()) {
            checkDependencyConflict(state);
        }
    }

    private void checkDependencyConflict(final DepConflictState state) {
        /* 3 conflict scenarios
            1 mix of number versioned & string versioned dependency versions
            2 more than then one explicit dependency for a module (
            3 fixed dep (not greater) and disjunct greater than dep at the same time
         */
        
        // Scenario 1
        if (!state.namedDeps.isEmpty() && !state.numberedDeps.isEmpty()) {
            LOG.severe(
                    addVerifyMessage(ConsistencyMsgLevel.ERROR, 
                            "Named dependencies %s encountered in conjunction with numbered dependencies %s",
                            state.namedDeps.stream().map(ModuleDependency::toString).
                                    collect(Collectors.joining(" ; ")),
                            state.numberedDeps.stream().map(ModuleDependency::toString).
                                    collect(Collectors.joining(" ; ")))
            );
        } else if (state.namedDeps.isEmpty() && !state.numberedDeps.isEmpty()) {
            // Scenario 3
            if (state.highestEqualTo != Long.MIN_VALUE && state.lowestGreaterThan != Long.MAX_VALUE &&
                    state.highestEqualTo < state.lowestGreaterThan) {
                LOG.severe(
                        addVerifyMessage(ConsistencyMsgLevel.ERROR, "Conflicting dependencies: %s and %s",
                                (new ModuleDependency(state.moduleName,
                                        VersionConverters.moduleVersionToString(state.highestEqualTo),
                                        false)).toString(),
                                (new ModuleDependency(state.moduleName,
                                        VersionConverters.moduleVersionToString(state.lowestGreaterThan), 
                                        true)).toString())
                );                
            
            // Scenario 2
            } else if (state.exactNumberedDeps.size() >= 2) {
                LOG.severe(
                        addVerifyMessage(ConsistencyMsgLevel.ERROR, "Conflicting dependencies: %s",
                                state.exactNumberedDeps.stream().map(ModuleDependency::toString).
                                        collect(Collectors.joining(", ")))
                );
            }
        }
    }

    private static void handleNextDependencyCheck(final ModuleDependency dep, final DepConflictState state) {        
        if (VersionConverters.isModuleVersionNamed(dep.getVersion())) {
            state.namedDeps.add(dep);
        } else {
            final long depVersion = VersionConverters.getModuleConverter().fromString(dep.getVersion(), false);
            state.numberedDeps.add(dep);
            if (!dep.isGreater()) {
                // collect exact versions
                state.exactNumberedDeps.add(dep);                
            }
            
            if (dep.isGreater() && depVersion < state.lowestGreaterThan) {
                state.lowestGreaterThan = depVersion;
            } else if (!dep.isGreater() && depVersion > state.highestEqualTo) {
                state.highestEqualTo = depVersion;
            }
        }        
    }

    private static class DepConflictState {
        String moduleName = "";
        long highestEqualTo = Long.MIN_VALUE;
        long lowestGreaterThan = Long.MAX_VALUE;
        
        SortedSet<ModuleDependency> namedDeps = new TreeSet<>(DependencyUtils::compareDependency);
        SortedSet<ModuleDependency> numberedDeps = new TreeSet<>(DependencyUtils::compareDependency);
        
        List<ModuleDependency> exactNumberedDeps = new ArrayList<>();

        void resetState(final String depName) {
            moduleName = depName;
            highestEqualTo = Long.MIN_VALUE;
            lowestGreaterThan = Long.MAX_VALUE;
            
            namedDeps.clear();
            numberedDeps.clear();
            exactNumberedDeps.clear();
        }
    }
}
