/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Configuration_;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.GenerateEntry_;
import se.esss.ics.iocfactory.util.LazyLoadUtil;

/**
 * Provides access to the table containing IOC generate log entries.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class BrowseService {

    @PersistenceContext
    private EntityManager em;

    /***
     * Get single entry by id.
     * 
     * @param id table key
     * @return GenerateEntry mapped table row to GenerateEntry entity
     */
    public GenerateEntry getIocEntryById(long id) {
        return em.find(GenerateEntry.class, id);
    }

    /***
     * Get all entries from the table. Use with caution.
     * 
     * @return list of GenerateEntry entry values
     */
    public List<GenerateEntry> getGenerateEntries() {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<GenerateEntry> gcq = cb.createQuery(GenerateEntry.class);
        final Root<GenerateEntry> geRoot = gcq.from(GenerateEntry.class);
        gcq.select(geRoot);
        gcq.orderBy(cb.asc(geRoot.get(GenerateEntry_.iocName)), cb.desc(geRoot.get((GenerateEntry_.id))));
        return em.createQuery(gcq).getResultList();
    }

    /***
     * Counts all data (filtered) in table.
     * 
     * @param filters type GenerateEntryFilterValues (nullable) - filter fields and values
     * @return integer - count of all rows in query
     * @throws Exception
     */
    public long getIocEntryCount(GenerateEntryFilterValues filters) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<?> gcq = cb.createQuery();
        final Root<GenerateEntry> countRoot = gcq.from(GenerateEntry.class);
        if (filters != null) {
            addfiltersToQuery(filters, cb, gcq, countRoot);
        }
        gcq.multiselect(cb.count(countRoot));

        final TypedQuery<Long> query = (TypedQuery<Long>) em.createQuery(gcq);

        // in subselect query getSingleResult can throw NoResultsException
        final List<Long> listCount = query.getResultList();
        if (listCount.isEmpty()) {
            return 0;
        }
        return listCount.get(0);
    }

    /***
     * 
     * Retrieves Sorted, filtered and limited list of rows from Generate_Entry table.
     * 
     * @param first starting row
     * @param pageSize number of rows to fetch
     * @param sortField (nullable) name of sorted field
     * @param sortOrder (nullable) type of sort order
     * @param filters (nullable) filter values wrapped in a class
     * @return list of GenerateEntry values
     * @throws Exception
     */
    public List<GenerateEntry> getIocEntries(int first, int pageSize, GenerateEntrySortColumns sortColumns, IndependentSortOrder sortOrder,
            GenerateEntryFilterValues filters) {

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<?> gcq = cb.createQuery();
        final Root<GenerateEntry> iocEntry = gcq.from(GenerateEntry.class);

        if (filters != null) {
            addfiltersToQuery(filters, cb, gcq, iocEntry);
        }
        addSortingToQuery(sortColumns, sortOrder, cb, gcq, iocEntry);

        gcq.multiselect(iocEntry);
        final TypedQuery<GenerateEntry> query = (TypedQuery<GenerateEntry>) em.createQuery(gcq); // try to find more type safe solution
        query.setFirstResult(first);
        query.setMaxResults(pageSize);

        return query.getResultList();
    }

   
    /***
     * Add sorting to Criteria API query.
     * 
     * @param sortCol on which column we sort
     * @param sortOrder order of sorting
     * @param cb criteria builder context
     * @param gcq criteria query context
     * @param iocEntry query root
     */
    private void addSortingToQuery(GenerateEntrySortColumns sortCol, IndependentSortOrder sortOrder, CriteriaBuilder cb, CriteriaQuery<?> gcq,
            Root<GenerateEntry> iocEntry) {
        if (sortCol == null) {
            return;
        }

        switch (sortCol) {
        case IOCNAME:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.iocName), cb, gcq);
            break;
        case OS:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.os), cb, gcq);
            break;
        case HOSTNAME:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.hostname), cb, gcq);
            break;
        case DESC:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.desc), cb, gcq);
            break;
        case USER:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.user), cb, gcq);
            break;
        case PRODUCTION:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.production), cb, gcq);
            break;
        case TARGETPATH:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.targetPath), cb, gcq);
            break;
        case CONFIGREVISION:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.configRevision), cb, gcq);
            break;
        case ENVIROMENT:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.environment), cb, gcq);
            break;
        case GENERATEDDATE:
            LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(GenerateEntry_.generatedDate), cb, gcq);
            break;
        }
    }

    /***
     * 
     * Adds filter fields and values to query.
     * 
     * @param filters (nullable) wrapped values of filters in GenerateEntryFilterValues
     * @param cb criteria builder context
     * @param gcq criteria query context
     * @param rootEntry query root entry
     */
    private void addfiltersToQuery(GenerateEntryFilterValues filters, CriteriaBuilder cb, CriteriaQuery<?> gcq, Root<GenerateEntry> rootEntry) {
        final List<Predicate> predicates = new ArrayList<Predicate>();

        if (filters.getIocName() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.iocName), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getIocName()) + "%"));
        }
        if (filters.getOs() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.os), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getOs()) + "%"));
        }
        if (filters.getHostname() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.hostname), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getHostname()) + "%"));
        }
        if (filters.getDesc() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.desc), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getDesc()) + "%"));
        }
        if (filters.getUser() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.user), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getUser()) + "%"));
        }
        if (filters.getTargetPath() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.targetPath), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getTargetPath()) + "%"));
        }
        if (filters.getEnvironment() != null) {
            predicates.add(cb.like(rootEntry.get(GenerateEntry_.environment), "%" + LazyLoadUtil.escapeSqlLikeChars(filters.getEnvironment()) + "%"));
        }

        // boolean
        if (filters.getIsProduction() != null) {
            predicates.add(cb.equal(rootEntry.get(GenerateEntry_.production), filters.getIsProduction()));
        }

        // Integer
        if (filters.getConfigRevision() != null) {
            predicates.add(cb.equal(rootEntry.get(GenerateEntry_.configRevision), filters.getConfigRevision()));
        }

        // DateTime
        if (filters.getGeneratedDate() != null) {
            predicates.add(cb.greaterThanOrEqualTo(rootEntry.get(GenerateEntry_.generatedDate), filters.getGeneratedDate()));
        }

        // consistency status
        if (filters.getConsistencyStatus() != null) {
            addConsistencyStatusFilter(filters, cb, gcq, rootEntry, predicates);
        }

        gcq.where(predicates.toArray(new Predicate[] {}));
    }

    /***
     * Consistency status filter is based on join (id=config_id) Between generate_entry table and Configuration table. Consistency between
     * configurations is realized with comparing hash values of both files. Filter for deleted entries is realized with subquery - we look for
     * generate_entry table values that are not in Configuration table.
     * 
     * @param filters (nullable) wrapped values of filters in GenerateEntryFilterValues
     * @param cb criteria builder context
     * @param gcq criteria query context
     * @param rootEntry query root entry
     * @param predicates reference to list of query where condition predicates
     */
    private void addConsistencyStatusFilter(GenerateEntryFilterValues filters, CriteriaBuilder cb, CriteriaQuery<?> gcq, Root<GenerateEntry> rootEntry,
            List<Predicate> predicates) {
        final Root<Configuration> configRoot = gcq.from(Configuration.class);

        switch (filters.getConsistencyStatus()) {
        case CLEAN:
            predicates.add(cb.equal(rootEntry.get(GenerateEntry_.configId), configRoot.get(Configuration_.id)));
            predicates.add(cb.equal(rootEntry.get(GenerateEntry_.generatedHash), configRoot.get(Configuration_.hash)));
            break;
        case CONFIG_DIFFER:
            predicates.add(cb.equal(rootEntry.get(GenerateEntry_.configId), configRoot.get(Configuration_.id)));
            predicates.add(cb.notEqual(cb.coalesce(rootEntry.get(GenerateEntry_.generatedHash), "null"), cb.coalesce(configRoot.get(Configuration_.hash), "null")));
            break;
        case CONFIG_DELETED:
            Subquery<Long> subQuery = gcq.subquery(Long.class);
            Root<Configuration> configRootSub = subQuery.from(Configuration.class);
            subQuery.select(configRootSub.get(Configuration_.id));
            predicates.add(cb.not(rootEntry.get(GenerateEntry_.configId).in(subQuery)));
            gcq.groupBy(rootEntry.get(GenerateEntry_.id));
            break;
        }
    }

}
