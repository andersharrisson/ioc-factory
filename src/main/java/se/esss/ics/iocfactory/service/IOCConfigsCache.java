/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.enterprise.event.Observes;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.service.cdievents.IOCRemoved;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;

/**
 * A class that caches the loaded {@link Configuration}s and {@link IOC}s in the {@link ViewScoped} scope
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ViewScoped
public class IOCConfigsCache implements Serializable {
    @Inject private transient ConfigurationService configService;
    @Inject private transient IOCService iocService;
    @PersistenceContext private transient EntityManager em;

    private final Map<String, List<Configuration>> configsByIoc = new HashMap<>();
    private final Map<Long, Configuration> configById = new HashMap<>();

    private List<IOC> iocs = null;
    private Map<String, IOC> iocByName = null;

    /**
     * Invalidates the whole cache
     */
    public void clear() {
        configsByIoc.clear();
        configById.clear();
        iocs = null;
        iocByName = null;
    }

    public List<IOC> getIOCs() throws ConfigClientException {
        ensureIocs();
        return iocs;
    }

    public Map<String, IOC> getIOCsMap() throws ConfigClientException {
        ensureIocs();
        return iocByName;
    }

    /**
     * Returns an IOC object if this is a valid IOC, otherwise null
     *
     * @param iocName the IOC name
     * @throws ConfigClientException
     * @return a valid IOC object or null
     */
    public IOC getIOCByName(String iocName) throws ConfigClientException {
        final IOC result = getIOCsMap().get(iocName);
        return result != null && !result.getConsistencyStatus().isError() ? result : null;
    }

    /** Gets a list of configurations for a given IOC by first checking the cache
     * @param ioc
     * @return
     */
    public List<Configuration> getConfigurationsForIOC(final IOC ioc) {
        if (ioc==null || StringUtils.isEmpty(ioc.getName())) {
            return Collections.emptyList();
        }

        List<Configuration> mapEntry = configsByIoc.get(ioc.getName());
        if (mapEntry==null) {
            LOG.info(String.format("Loading configs for IOC %s", ioc.getName()));
            mapEntry = configService.getConfigurationsForIOC(ioc.getName());
            configsByIoc.put(ioc.getName(), mapEntry);
            mapEntry.forEach(c -> configById.put(c.getId(), c));
        } else {
            LOG.info(String.format("Cache hit for IOC %s", ioc.getName()));
        }
        return mapEntry;
    }
    private static final Logger LOG = Logger.getLogger(IOCConfigsCache.class.getName());

    /** Gets a configuration by Id by first checking the cache
     *
     * @param id
     * @return
     */
    public Configuration getConfigurationById(Long id) {
        final Configuration cached = configById.get(id);
        if (cached == null) {
            return configService.getConfigurationById(id);
        }
        return cached;
    }

    /**
     * Invalidates the cached configurations for a given {@link IOC}
     * Should be invoked via the {@link IOCUpdated} event.
     * ]
     * @param ioc
     */
    public void clearConfigCacheEntryForIOC(@Observes @IOCUpdated final IOC ioc) {
        if (ioc==null || StringUtils.isEmpty(ioc.getName())) {
            return;
        }

        final List<Configuration> iocConfigs = configsByIoc.get(ioc.getName());
        if (iocConfigs != null) {
            iocConfigs.forEach(c -> configById.remove(c.getId()));
        }

        configsByIoc.remove(ioc.getName());

        reloadIocFromDb(ioc);
    }

    /**
     * Handles the {@link IOCRemoved} event
     *
     * @param ioc
     */
    public void iocRemoved(@Observes @IOCRemoved final IOC ioc) {
        if (iocs != null && iocByName != null) {
            iocByName.remove(ioc.getName());
            iocs.remove(ioc);
        }

        clearConfigCacheEntryForIOC(ioc);
    }

    private void ensureIocs() throws ConfigClientException {
        if (iocs == null || iocByName == null) {
            final Pair<List<IOC>, Map<String, IOC>> pair = iocService.getIOCs();
            iocs = pair.getLeft();
            iocByName = pair.getRight();
        }
    }

    private void reloadIocFromDb(IOC ioc) {
        final IOC dbIoc = em.find(IOC.class, ioc.getName());
        // Copy the last config rev
        if (dbIoc != null && iocByName != null) {
            final IOC cachedIOC = iocByName.get(ioc.getName());
            cachedIOC.setLastConfigRevision(dbIoc.getLastConfigRevision());
        }
    }
}
