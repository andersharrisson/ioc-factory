/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.ConfigVerifyMessages;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Repository;

/**
 *  An interface for classes that implement configuration verification functionality
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public interface ConfigVerifier {
    /**
     * Execution priority of this verifier. Lower numbers have higher priority.
     *
     * As a rule of thumb please define them as multiples of 100
     *
     * @return the execution priority for this verifier
     */
    int getPriority();

    /**
     * Represents the category of this verifier
     * 
     * @return 
     */
    String getCategory();
    
    /**
     * Implement this function to provide Verify functionality for a configuration
     *
     * The error messages / warning should be added to the errMsgs list parameter
     *
     * @param config the configuration for which the check is being done
     * @param errMsgs a collection messages where errors are reported
     * @param repo repository used for the current verification
     * @param env environment for which the verification is being done
     */
    void verify(final Configuration config,
            final ConfigVerifyMessages errMsgs,
            final Repository repo,
            final IOCEnvironment env);
}
