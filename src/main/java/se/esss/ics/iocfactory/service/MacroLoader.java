/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryMessage;
import se.esss.ics.macrolib.MacroException;
import se.esss.ics.macrolib.MacroLib;
import se.esss.ics.macrolib.MacroStorage;

import static se.esss.ics.iocfactory.model.PlaceholderTrait.*;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroLoader {
    private static final Logger LOG = Logger.getLogger(MacroLoader.class.getName());

    private final Repository repo;
    private final File scannedFile;

    private final ParameterPlaceholderLoader placeholderLoader;

    private final MacroLib macroLib = new MacroLib();

    public MacroLoader(Repository repo, File scannedFile) {
        this.repo = repo;
        this.scannedFile = scannedFile;

        this.placeholderLoader = new ParameterPlaceholderLoader(repo, scannedFile);
    }

    /**
     * Loads the macros inside the file specified by the constructor
     *
     * @return triple of MacroStorage, list of macros represented with {@link ParameterPlaceholder} 
     */
    public Pair<MacroStorage, List<ParameterPlaceholder>> loadMacros() {
        final List<ParameterPlaceholder> placeholderList  = placeholderLoader.loadParameterPlaceholders();

        final Map<String, ParameterPlaceholder> loadedPlaceholders = placeholderList.
                stream().collect(Collectors.toMap(k -> StringUtils.upperCase(k.getName()), Function.identity()));

        try {
            macroLib.parseMacros(scannedFile);
        } catch (MacroException me) {
            LOG.log(Level.WARNING, me.getMessage(), me);
            repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.ERROR, me.getMessage(), scannedFile));
            return new ImmutablePair<>(new MacroStorage(), Collections.emptyList());
        }

        final List<ParameterPlaceholder> result = macroLib.getMacroStorage().
                getOrderedMacros().stream().
                // Filter out Runtime entries
                filter(macroEntry -> {
                    final ParameterPlaceholder placeholder
                            = loadedPlaceholders.get(StringUtils.upperCase(macroEntry.getName()));
                    return placeholder == null || !placeholder.getTraits().contains(RUNTIME);
                }).
                map(macroEntry -> {
                    final ParameterPlaceholder placeholder
                            = loadedPlaceholders.get(StringUtils.upperCase(macroEntry.getName()));
                    if (placeholder == null) {
                        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.WARN,
                                String.format("No metadata for macro %s. Defaulting to STRING data type.",
                                        macroEntry.getName()), scannedFile));
                        return new ParameterPlaceholder(macroEntry.getName(), ParameterType.STRING, "", EnumSet.of(PLAIN));
                    } else {
                        placeholder.setMacroEntry(macroEntry);
                        return placeholder;
                    }
                }).collect(Collectors.toList());

        // Mark with error messages placeholders for which there were no macros found
        loadedPlaceholders.entrySet().stream().map(entry -> entry.getValue()).
                filter(placeholder -> placeholder.getMacroEntry() == null).
                forEach(placeholder ->
                    repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.WARN,
                        String.format("Metadata entry for non-existent macro %s.", placeholder.getName()),
                                scannedFile))
                );

        return new ImmutablePair<>(macroLib.getMacroStorage(), result);
    }
}
