/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 * 
 * This file is part of IOC Factory.
 * 
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers.dependency;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.commons.lang3.ObjectUtils;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;

/**
 *
 * @author mpavleski
 */
public class DependencyUtils {
    
    public static ConfigDependencies flatConfigDependencies(final Configuration config, final Repository repo) {
        final ConfigDependencies result = new ConfigDependencies(); 
        config.getDevices().stream().filter(dev -> !dev.getConsistencyStatus().isError()).forEach(dev -> {
            DependencyUtils.flatDependencies(dev.getSupportModule(), repo, result);
        });
        return result;
    }
    
    private static void flatDependencies(final Module module, final Repository repo, final ConfigDependencies deps) {
        final Set<Module> visitedModules = new HashSet<>();
        flatDependenciesInternal(module, visitedModules, repo, deps);
    }

    private static void flatDependenciesInternal(final Module module, final Set<Module> visitedModules, 
            final Repository repo, final ConfigDependencies deps) {
        if (!visitedModules.contains(module)) {
            visitedModules.add(module);

            module.getDependencies().stream().forEach(dep -> deps.addSatisfiedDep(dep, module));

            module.getDependencies().stream().
                    flatMap(dep -> findDependentModule(module, dep, repo, deps)).forEach(depMod -> {
                if (!visitedModules.contains(depMod)) {
                    flatDependenciesInternal(depMod, visitedModules, repo, deps);
                }
            });
        }
    }
    
    /**
     * The function finds a latest module in the repository matching a dependency
     *
     * @param basicModule a module with spec (os, epics) to be used as spec for searching
     * @param dep the dependency for which a satisfying module will be returned (if found)
     * @param repo repository used for searching
     * @param deps all deps
     * @return
     */
    public static Stream<Module> findDependentModule(final Module basicModule, final ModuleDependency dep,
            final Repository repo, final ConfigDependencies deps) {
        final List<Module> candidateModules
                = repo.getModulesForSpec(dep.getName(), basicModule.getOs(), basicModule.getEpicsVersion());

        final Optional<Module> candidateModule = candidateModules.stream().filter(candidate -> {
            if (dep.isGreater()) {
                return ModuleVersionsComparator.
                        comparePrioritizeNumbered(candidate.getModuleVersion(), dep.getVersion()) > -1;
            } else {
                return ModuleVersionsComparator.
                        comparePrioritizeNumbered(candidate.getModuleVersion(), dep.getVersion()) == 0;
            }
        }).max((lm, rm) -> ModuleVersionsComparator.comparePrioritizeNumbered(lm.getModuleVersion(),
                rm.getModuleVersion()));

        if (candidateModule.isPresent()) {
            return Stream.of(candidateModule.get());
        } else {
            deps.addUnsatisfiedDep(dep, basicModule);
            return Stream.empty();
        }
    }
    
    /**
     * Formats a module string to look like a path in EEE
     * 
     * @param module
     * @return 
     */
    public static String formatModuleString(final Module module) {
        return String.format("%s/%s/%s/lib/%s", module.getName(), module.getModuleVersion(),
                VersionConverters.epicsVersionToString(module.getEpicsVersion()),
                module.getOs());
    }

    /**
     * We have this comparator function to enable storing {@link ModuleDependency} in a {@link TreeSet} for sorted
     * printing
     *
     * @param l
     * @param r
     * @return
     */
    public static int compareDependency(final ModuleDependency l, final ModuleDependency r) {
        if (l == r) {
            return 0;
        } else if (l != null && r == null) {
            return 1;
        } else if (l == null && r != null) {
            return -1;
        } else {
            // Compare name
            final int nameCpr = ObjectUtils.compare(l.getName(), r.getName());
            if (nameCpr == 0) {
                final int verCpr = ModuleVersionsComparator.comparePrioritizeNumbered(l.getVersion(), r.getVersion());
                if (verCpr == 0) {
                    return Boolean.compare(l.isGreater(), r.isGreater());
                } else {
                    return verCpr;
                }
            } else {
                return nameCpr;
            }
        }
    }
}
