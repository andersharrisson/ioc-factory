/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup
public class PeriodicRepoScanner {
    private static final Logger LOG = Logger.getLogger(PeriodicRepoScanner.class.getName());
    private final Map<EEEType, String> repoHashes = new ConcurrentHashMap<>();
    private final Map<EEEType, Repository> repos = new ConcurrentHashMap<>();
    
    @Inject
    private SetupService setupService;

    /**
     * Triggers initial scan
     */
    @PostConstruct
    public void init() {
        LOG.info("INITIAL scanning of the EEE repositories");
        final IOCFactSetup setup = setupService.loadSetup();
        for (final EEEType eeeType : EEEType.values()) {
            internalCheckRepo(eeeType, setup, false);
        }
    }

    @Schedule(minute = "*/5", hour = "*", persistent = false)
    public void checkForChanges() {
        final IOCFactSetup setup = setupService.loadSetup();
        for (final EEEType eeeType : EEEType.values()) {
            internalCheckRepo(eeeType, setup, true);
        }
    }

    /**
     * Trigger manual scan whether Repo changed
     * 
     * @param eeeType
     * @return true if change detected
     */
    public boolean manualCheck(final EEEType eeeType) {

        final IOCFactSetup setup = setupService.loadSetup();
        return internalCheckRepo(eeeType, setup, false);
    }
    
    public Pair<Repository, String> getAppScopedRepoAndHash(final EEEType eeeType) {
        synchronized(this) {
            return new ImmutablePair<>(repos.get(eeeType), repoHashes.get(eeeType));
        }
    }
    
    private boolean internalCheckRepo(final EEEType eeeType, final IOCFactSetup setup, 
            final boolean triggerWebSockets) {
        final String oldHash = repoHashes.get(eeeType);

        final Repository repo = Util.loadSpecificRepo(eeeType, setup);
        final String newHash = RepositoryDump.computeRepositoryHash(repo);
        
        if (!StringUtils.equals(oldHash, newHash)) {
            // Synchronized to update both data structures in one go
            synchronized(this) {
                repoHashes.put(eeeType, newHash);
                repos.put(eeeType, repo);

                if (triggerWebSockets) {
                    triggerWebSocketEvent(eeeType);
                }
            }
            
            LOG.info(oldHash == null ?
                    String.format("First periodic scan of %s repo, storing the %s hash, repo %d",
                    eeeType.getName(), newHash, repo.hashCode()) :
                    String.format("Change detected in %s repo, old hash %s, new hash %s, repo %d",
                    eeeType.getName(), oldHash, newHash, repo.hashCode()));
            return true;
        } else {
            return false;
        }
    }
    
    private static void triggerWebSocketEvent(final EEEType eeeType) {
        final EventBusFactory eventBusFactory = EventBusFactory.getDefault();
        if (eventBusFactory != null) {
            final EventBus eventBus  = eventBusFactory.eventBus();
            if (eventBus != null) {
                eventBus.publish("/repoChange", eeeType.getName());
            }
        }
    }

}
