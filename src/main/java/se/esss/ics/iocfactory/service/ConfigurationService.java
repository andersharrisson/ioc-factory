/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import se.esss.ics.iocfactory.model.*;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;
import se.esss.ics.iocfactory.util.Util;


/**
 * A service that performs operations on IOC configuraitons - {@link Configuration}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
@PermitAll
public class ConfigurationService {
    private static final Logger LOG = Logger.getLogger(ConfigurationService.class.getName());

    private static final String ENV_MODULE_NAME = "environment";

    @PersistenceContext private EntityManager em;

    @Inject private ConfigClientService configClientService;
    @Inject private IOCService iocService;
    @Inject private IOCConfigsCache iocCache;

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB private GenerateService generateService;
    @EJB private LogService logService;

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper(true);

    /**
     * Retrieves a list of persisted configurations, correctly sorted by IOC, revision (last rev to first rev)
     * for the given {@link List} of IOCs (@{link IOC})
     *
     * The provided configuration objects only contain the persisted data (they are not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}
     *
     * Note: Not yet used as of 2015-12-08
     *
     * @param iocs a list of IOCs for which to retrieve the configurations
     * @return a list of matching configurations
     */
    public List<Configuration> getConfigurationsForIOCs(List<String> iocs) {
        final List<Configuration> configs = (List<Configuration>) em.createQuery("select c from Configuration c "
                + "where c.ioc.name in :iocs order by c.revision DESC").
                setParameter("ioc_name", iocs).getResultList();
        // Validate configuration
        checkConsistency(configs);

        return configs;
    }

    /**
     * Retrieves a list of persisted configurations in the correctly sorted order by revision (last rev to first rev)
     * for a given IOC name
     *
     * The provided configuration objects only contain the persisted data (they are not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}.
     *
     * @param ioc the name of the IOC for which the configurations are to be retrieved
     * @return a list of matching configurations
     */
    @SuppressWarnings("unchecked")
    public List<Configuration> getConfigurationsForIOC(String ioc) {
        final List<Configuration> configs = (List<Configuration>) em.createQuery("select c from Configuration c "
                + "where c.ioc.name = :ioc_name order by c.revision DESC").
                setParameter("ioc_name", ioc).getResultList();
        // Validate configuration
        checkConsistency(configs);

        return configs;
    }

    /**
     * Retrieves a persisted configuration by a given configurationraiton id.
     *
     * The provided configuration object only contains the persisted data (it is not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}.
     *
     * @param id
     * @return
     */
    public Configuration getConfigurationById(Long id) {
        return (Configuration)Util.getSingleResult(em.createQuery("select c from Configuration c where c.id = :id").
                setParameter("id", id));
    }

    /**
     * Persists a newly created configuration metadata. Used when new configuration is created.
     *
     * This method does not take into account the run-time constructed configuration topology (devices, macros, globals)
     * using hte CCDB & the repository structure. It should be used only for saving the metadata for a new
     * configuration.
     *
     * For full configuration save, please use
     * {@link ConfigurationService#updateConfiguration(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public void saveNewConfiguration(Configuration config) {
        // Persist the IOC if it is not persisted in the database
        config.setIoc( iocService.saveIOC(config.getIoc()) );

        // Make sure to update last revision on the IOC
        updateIOCLastRevision(config);

        config.setLastEdit(new Date());

        em.persist(config);

        // Audit log entry
        logService.logConfigAdded(config);
    }

    /**
     * EJB Roles secured version of
     * {@link ConfigurationService#updateConfiguration(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config the configuration to be updated
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public void updateConfigurationSecured(Configuration config) {
        updateConfiguration(config);

        // Audit log entry
        logService.logConfigSaved(config);
    }

    /**
     * Updates a configuration in the database, honoring all the dependent entities (global macros, devices, device
     * macros) - System non secured updates, for secured (EJB Roles respected) please use
     * {@link ConfigurationService#updateConfigurationSecured(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config
     */
    public void updateConfiguration(Configuration config) {
        config.setLastEdit(new Date());
        config.setHash(YAML_HELPER.getEntityDigestQuiet(config));
        config.setOutputHash(generateService.calculateConfigurationOutputHash(config));
        internalMergeConfiguration(config);
    }

    /**
     * Validates a configuration
     *
     * @param config
     */
    public List<ConsistencyStatusValue> validateConfiguration(Configuration config) {
        return validateModuleVersions(config).collect(Collectors.toList());
    }

    /**
     * Fully loads a configuration from the database, including the dependent entities (global macros, devices, device
     * macros)>
     * Performs merge operation with current CCDB and repository data.
     * Marks inconsistencies in the entity topology.
     * @param configId
     * @return
     * @throws ConfigClientException
     */
    public Configuration loadConfiguration(Long configId) throws ConfigClientException {
        final Configuration config = eagerLoadAndDetach(em.find(Configuration.class, configId));

        procesIOCandDevices(config);

        return config;
    }

    /**
     * A utility method that retrieves a revision id for a new configuration for the given IOC name.
     *
     * It works by consulting the {@link IOC#lastConfigRevision} field in the IOC database table
     *
     * @param iocName the name of the IOC
     * @return a new revision number for a configuration for the given IOC
     */
    public int getNextConfigurationForIOC(String iocName) {
        // If IOC was persisted in DB, it contains last revision number used for configurations,
        // if not in db we'll assign 0 as such IOC was not yet persisted in the db
        return getNextConfigurationForIOC(em.find(IOC.class, iocName));
    }


    /**
     * Deletes a configuration. Only non commited configurations can be deleted.
     *
     * @param selectedConfigs the List of configs to delete
     *
     * @return true if configuration is deleted, false if it is commited
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public boolean deleteConfigurations(List<Configuration> selectedConfigs) {
        if (selectedConfigs.stream().anyMatch(c -> c.isCommited())) {
            return false;
        }

        selectedConfigs.forEach(config -> {
            final Configuration mergedConfig = em.merge(config);
            logService.logConfigDeleted(mergedConfig);
            em.remove(mergedConfig);
        });
        return true;
    }

    /**
     * Changes the module of a device configuration and reloads parameters to a new version
     *
     * @param device the device configuration
     * @param newVersion the new version
     * @throws ConfigClientException
     * @return true if the module was found and changed, false if the module version is incompatible with the dev.
     * module
     */
    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public boolean changeDeviceModuleVersion(DeviceConfiguration device, String newVersion) throws ConfigClientException {
        final Module supportModule = device.getSupportModule();
        if (supportModule == null) {
            throw new NullPointerException("changeDeviceModuleVersion: Device doesn't have support module defined.");
        }

        final Optional<Module> newModule = Util.getRepository().getModuleForSpec(supportModule.getName(), 
                supportModule.getOs(), supportModule.getEpicsVersion(), newVersion);

        if (newModule.isPresent()) {
            device.setSupportModule(newModule.get());
            
            validateDeviceModule(device, Util.getRepository());

            loadDeviceParameters(device);
            return true;
        } else {
            return false;
        }
    }

    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public boolean deleteInvalidDevices(List<DeviceConfiguration> devices) throws ConfigClientException {
        MutableObject<Configuration> lastRemovedConfigRef = new MutableObject<>(null);
        devices.stream().filter(dev -> dev.getConsistencyStatus().isError()).
                forEach(dev -> {
                    lastRemovedConfigRef.setValue(dev.getConfiguration());
                    lastRemovedConfigRef.getValue().getDevices().remove(dev);
                });

        if (lastRemovedConfigRef.getValue() != null) {
            procesIOCandDevices(lastRemovedConfigRef.getValue());
            return true;
        } else {
            return false;
        }
    }

    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public boolean renameInvalidDevice(DeviceConfiguration from, DeviceConfiguration to) throws ConfigClientException {
        if (from==null || to==null || !Objects.equals(from.getConfiguration(), to.getConfiguration())) {
            return false;
        }
        if (!from.getConsistencyStatus().isError()) {
            return false;
        }

        final Configuration config = from.getConfiguration();

        // merge config params
        mergeParameters(from, to);

        // remove from device
        config.getDevices().remove(from);

        // reload the to device
        validateDeviceModule(to, Util.getRepository());
        loadDeviceParameters(to);

        return true;
    }

    @RolesAllowed({IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION})
    public boolean copyConfiguration(Long srcId, String destIocName, String user) throws ConfigClientException {
        final IOC destIOC = iocCache.getIOCByName(destIocName);
        if (destIOC == null) {
            return false;
        }

        final Configuration srcLoaded = loadConfiguration(srcId);

        final Configuration dest = new Configuration();

        copyConfigMetadata(srcLoaded, dest, destIOC, user);

        saveNewConfiguration(dest);

        copyConfigGlobals(srcLoaded, dest);
        copyDevices(srcLoaded, dest);

        ensureHierarchy(dest);

        dest.setOutputHash(generateService.calculateConfigurationOutputHash(dest));

        return true;
    }

    private void copyConfigMetadata(Configuration src, Configuration dest, IOC targetIOC, String user) {
        dest.setComment(src.getComment());
        dest.setCommited(false);
        dest.setEpicsVersion(src.getEpicsVersion());
        dest.setIoc(targetIOC);
        dest.setLastEdit(new Date());
        dest.setOs(targetIOC.getOs());
        dest.setRevision(getNextConfigurationForIOC(targetIOC));
        dest.setUser(user);
        dest.setEnvVersion(src.getEnvVersion());
        dest.setProcServPort(src.getProcServPort());
    }

    private void copyConfigGlobals(Configuration src, Configuration dest) {
        src.getGlobals().forEach(srcGlobal -> {
            final Parameter destGlobal = new Parameter(srcGlobal.getName(), null, srcGlobal.getType(), 
                srcGlobal.getTraits());
            destGlobal.setConfiguration(dest);
            destGlobal.setValue(srcGlobal.getValue());

            dest.getGlobals().add(destGlobal);
        });
    }

    private static Stream<DeviceConfiguration> streamPersistedDevices(Configuration src) {
        return src.getDevices().stream().filter(srcDev ->
                srcDev.getId() != null &&
                !srcDev.getConsistencyStatus().contains(DeviceConsistencyStatus.DEVICE_IS_NEW));
    }
    
    private void copyDevices(Configuration src, Configuration dest) {
        streamPersistedDevices(src).forEach(srcDev -> {
            final DeviceConfiguration destDev = new DeviceConfiguration();

            destDev.setConfiguration(dest);
            destDev.setName(srcDev.getName());
            destDev.setSupportModule(srcDev.getSupportModule());
            destDev.setSnippets(new ArrayList<>(srcDev.getSnippets()));
            dest.getDevices().add(destDev);

            copyDeviceParams(srcDev, destDev);
        });

    }

    private void copyDeviceParams(DeviceConfiguration srcDev, DeviceConfiguration destDev) {
        srcDev.getParameters().stream().filter(srcParam -> srcParam.getId() != null).
                forEach(srcParam -> {
            final Parameter destParam = new Parameter(srcParam.getName(), 
                    srcParam.getSnippetName(), srcParam.getType(), srcParam.getTraits());
            destParam.setDeviceConfiguration(destDev);
            destParam.setValue(srcParam.getValue());
            destParam.setDesc(srcParam.getDesc());
            
            destDev.getParameters().add(destParam);
        });
    }

    private void mergeParameters(DeviceConfiguration from, DeviceConfiguration to) {
        final Iterator<Parameter> iter = from.getParameters().iterator();

        while (iter.hasNext()) {
            final Parameter fromParam = iter.next();

            // Remove dest param if found with the same space
            final Optional<Parameter> toParam = filterParamsWithSpec(to.getParameters(), fromParam.getName(),
                    fromParam.getSnippetName(), fromParam.getType()).findAny();
            if (toParam.isPresent()) {
                toParam.get().setDeviceConfiguration(null);
                to.getParameters().remove(toParam.get());
            }

            // Move from param to the other device
            fromParam.setDeviceConfiguration(to);
            to.getParameters().add(fromParam);
            iter.remove();
        }
    }

    private void procesIOCandDevices(Configuration config) throws ConfigClientException {
        updateConfigIOC(config);
        final Repository repo = Util.getRepository();

        final List<DeviceConfiguration> persistedDevices = config.getDevices();
        final List<IOCDevice> ccdbDevices = configClientService.getDevicesForIOC(config.getIoc().getName());
        final Map<String, IOCDevice> ccdbDeviceByName = ccdbDevices.stream().
                collect(Collectors.toMap(IOCDevice::getName, Function.identity()));
        
        final Iterator<DeviceConfiguration> persistedIterator = persistedDevices.iterator();
        while (persistedIterator.hasNext()) {
            final DeviceConfiguration persistedDevice = persistedIterator.next();

            // Should always have module
            if (persistedDevice.getSupportModule()==null) {
                persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_NOT_DEFINED);
            } else {
                validateDeviceModule(persistedDevice, ccdbDeviceByName.get(persistedDevice.getName()), repo);
            }
        }

        // Add devices that are not persisted at the end:
        ccdbDevices.stream().
            // All devices that are not in the persisted devices
            filter(ccdbDevice -> !persistedDevices.stream().anyMatch(persistedDevice ->
                            Objects.equals(ccdbDevice.getName(), persistedDevice.getName()) &&
                            Objects.equals(ccdbDevice.getModuleName(), persistedDevice.getSupportModule().getName())
               )).forEach(ccdbDevice -> {
                persistedDevices.add(createBlankDeviceConfigFromCCDBDevice(config, ccdbDevice));
            });

        // Process parameters
        persistedDevices.stream().forEach(persistedDevice -> loadDeviceParameters(persistedDevice));
    }

    private DeviceConfiguration createBlankDeviceConfigFromCCDBDevice(final Configuration config, 
            final IOCDevice ccdbDevice)  {
        final DeviceConfiguration newDeviceConfig = new DeviceConfiguration();
        newDeviceConfig.setConfiguration(config);
        newDeviceConfig.setName(ccdbDevice.getName());  
        newDeviceConfig.setCcdbDevice(ccdbDevice);
        
        // Will prioritize to have numbered modules first
        final Optional<Module> repoModule = Util.getRepository().getLatestModuleForSpec(ccdbDevice.getModuleName(),
                config.getOs(), config.getEpicsVersion(), ModuleVersionsComparator::comparePrioritizeNumbered);

        if (repoModule.isPresent()) {
            newDeviceConfig.setSupportModule(repoModule.get());
        } else {
            // Set for display purposes
            newDeviceConfig.setSupportModule(new Module(ccdbDevice.getModuleName()));            
            newDeviceConfig.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST);
        }
        return newDeviceConfig;
    }

    private void validateDeviceModule(DeviceConfiguration persistedDevice, IOCDevice ccdbDevice, Repository repo) {
        persistedDevice.getConsistencyStatus().clear();
        persistedDevice.setCcdbDevice(ccdbDevice); 
        
        if (ccdbDevice == null) {
            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.DEVICE_IS_ORPHANED);
        } else if (!Objects.equals(ccdbDevice.getModuleName(), persistedDevice.getSupportModule().getName())) {
            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.DIFFERENT_MODULE);
        } else {
            final Optional<Module> repoModule = repo.getModuleFromRepo(persistedDevice.getSupportModule());
            if (repoModule.isPresent()) {
                persistedDevice.setSupportModule(repoModule.get());
            } else {
                persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST);
            }            
        }
    }
    
    private void validateDeviceModule(DeviceConfiguration device, Repository repo) throws ConfigClientException {        
        final IOCDevice ccdbDevice = configClientService.getDevicesForIOC(device.getConfiguration().getIoc().getName()).
                stream().filter(ccdbDev -> Objects.equals(ccdbDev.getName(), device.getName())).findAny().orElse(null);
        validateDeviceModule(device, ccdbDevice, repo);
    }
        
    private Configuration eagerLoadAndDetach(Configuration config) {
        if (config == null) {
            return null;
        }

        // Get global params
        config.getGlobals().stream().forEach(Parameter::getName);

        // get all collections down the way, update OS & EPICS version from config for the modules
        config.getDevices().stream().forEach(dc -> {
            dc.getSupportModule().getName();
            
            dc.getSupportModule().setOs(config.getOs());
            dc.getSupportModule().setEpicsVersion(config.getEpicsVersion());

            dc.getParameters().stream().forEach(Parameter::getName);
        });

        // Detach is cascaded
        em.detach(config);
        return config;
    }

    private void loadDeviceParameters(DeviceConfiguration persistedDevice) {
        // Clear consistency status as this might be invoked on already loaded parameters
        persistedDevice.getParameters().forEach(param -> param.getConsistencyStatus().clear());

        if (!persistedDevice.getConsistencyStatus().isError()) {
            // disregard empty loaded params
            removeEmptyLoadedParameters(persistedDevice);
            
            // Get the list of loaded parameters, and put an empty list to build instead
            final List<Parameter> persistedParameters = persistedDevice.getParameters();
            final List<Parameter> newParametersList = new ArrayList<>();
            persistedDevice.setParameters(newParametersList);
            
            final Set<Parameter> processedPersisted = new HashSet<>();
            final List<Snippet> deviceSnippets = new ArrayList<>();
            persistedDevice.setSnippets(deviceSnippets);
                        
            for (final String persistedSnippet : persistedDevice.getCcdbDevice().getSnippets()) {
                Optional<Snippet> deviceSnippet = persistedDevice.getSupportModule().getSnippets().stream().
                        filter(snippet -> StringUtils.equals(persistedSnippet,
                        snippet.getName())).findFirst();
                if (deviceSnippet.isPresent()) {
                    deviceSnippets.add(deviceSnippet.get());        
                    loadDeviceParametersForValidSnippet(persistedDevice, persistedParameters, deviceSnippet.get(),
                            processedPersisted);
                }
            }
            
            // Handle persisted parameters that are not reported to be part of the config
            persistedParameters.stream().filter(persistedParameter
                    -> !processedPersisted.contains(persistedParameter)).forEach(persistedParameter -> {
                persistedParameter.setDesc("");
                persistedParameter.getConsistencyStatus().add(ParameterConsistencyStatus.PERSISTED_INVALID);
                newParametersList.add(persistedParameter);
            });
            
            // For all parameters update the device reference
            newParametersList.forEach(param -> param.setDeviceConfiguration(persistedDevice));
        } else{
            markInvalidParameters(persistedDevice);
        }
        
    }

    private void removeEmptyLoadedParameters(final DeviceConfiguration persistedDevice) {
        final Iterator<Parameter> iter = persistedDevice.getParameters().iterator();
        while (iter.hasNext()) {
            final Parameter param = iter.next();
            if (StringUtils.isEmpty(param.getValue())) {
                iter.remove();
            }
        }
    }

    private Stream<Parameter> filterParamsWithSpec(List<Parameter> parameters, String name, String snippetName,
            ParameterType type) {
         return parameters.stream().
                    filter(persistedParameter -> type  == persistedParameter.getType() &&
                            Objects.equals(persistedParameter.getSnippetName(), snippetName) &&
                            StringUtils.equalsIgnoreCase(name, persistedParameter.getName()));
    }

    private void loadDeviceParametersForValidSnippet(DeviceConfiguration persistedDevice, 
            List<Parameter> persistedParameters, Snippet snippet, Set<Parameter> processedPersisted) {
        final List<ParameterPlaceholder> placeholders = snippet.getPlaceholders();
        final List<Parameter> deviceParameters = persistedDevice.getParameters();
        
        // Add all parameters relevant according to placeholders, do it in ConfClient Placeholder order to
        // keep ordering as in definitions
        placeholders.stream().forEach(placeholder ->
            filterParamsWithSpec(persistedParameters, placeholder.getName(), snippet.getName(), 
                    placeholder.getType()).findAny().ifPresent(persistedParameter -> {
                        // update desc as it is not persisted
                        persistedParameter.setDesc(placeholder.getDesc());
                        persistedParameter.setMacroEntry(placeholder.getMacroEntry());
                        persistedParameter.setSnippet(snippet);
                        persistedParameter.setTraits(placeholder.getTraits());
                        deviceParameters.add(persistedParameter);
                        
                        processedPersisted.add(persistedParameter);
                    })
        );

        // Add possible remaining non-matched placeholders
        placeholders.stream().filter(placeholder -> !filterParamsWithSpec(deviceParameters, placeholder.getName(),
                snippet.getName(), placeholder.getType()).anyMatch(
                        parameter -> placeholder.getType() == parameter.getType() &&
                                Objects.equals(placeholder.getName(), parameter.getName()))).
                forEach( placeholder -> deviceParameters.add(emptyParameterFromPlaceholder(placeholder, snippet)) );        
    }

    private void markInvalidParameters(DeviceConfiguration persistedDevice) {
        persistedDevice.getParameters().forEach(param ->
                param.getConsistencyStatus().add(ParameterConsistencyStatus.INVALID_DEVICE_CONFIG));

    }

    private static Parameter emptyParameterFromPlaceholder(ParameterPlaceholder placeholder, Snippet snippet) {
        final Parameter result = new Parameter(placeholder, snippet);
        result.getConsistencyStatus().add(ParameterConsistencyStatus.PARAM_IS_NEW);
        return result;
    }

    private static void ensureHierarchy(Configuration config) {
        config.getGlobals().forEach(global -> {
            global.setConfiguration(config);
            global.setDeviceConfiguration(null);
        });

        // Don't save devices which have errors
        final Iterator<DeviceConfiguration> devIter = config.getDevices().iterator();
        while(devIter.hasNext()) {
            final DeviceConfiguration devConfig = devIter.next();

            if (devConfig.getConsistencyStatus().isError()) {
                devIter.remove();
            }
        }

        config.getDevices().forEach(dev -> {
            dev.setConfiguration(config);
            dev.getParameters().stream().forEach(param -> {
                param.setConfiguration(null);
                param.setDeviceConfiguration(dev);
            });
        });
    }

    private void internalMergeConfiguration(Configuration config) {
        ensureHierarchy(config);
        em.merge(config);
    }

    private void updateIOCLastRevision(Configuration config) {
        int iocLastRevNo = config.getIoc().getLastConfigRevision();
        int configRevNo = config.getRevision();

        if (configRevNo>iocLastRevNo) {
            config.getIoc().setLastConfigRevision(configRevNo);
        }
    }

    private void checkConsistency(List<Configuration> configs) {
        final Repository repository = Util.getRepository();
        configs.stream().forEach(config -> {
            final ConsistencyStatusSet status = config.getConsistencyStatus();

            status.clear();

            // Check if EPICS version is valid
            if (!repository.getEpicsVersions().contains(config.getEpicsVersion())) {
                status.add(ConfigurationConsistencyStatus.INVALID_EPICS_VERSION);
            }

            // Check if OS version is valid
            if (!repository.getOsVersions().contains(config.getOs())) {
                status.add(ConfigurationConsistencyStatus.INVALID_OS_VERSION);
            }

            // Check if Env version is valid
            final Optional<Module> envModule = repository.getModuleForSpec(ENV_MODULE_NAME, config.getOs(),
                    config.getEpicsVersion(), config.getEnvVersion());
            if (!envModule.isPresent()) {
                status.add(ConfigurationConsistencyStatus.INVALID_ENV_VERSION);

                final List<Module> envs = repository.getModulesForSpec(ENV_MODULE_NAME, config.getOs(),
                    config.getEpicsVersion());
                if (envs.isEmpty()) {
                    status.add(ConfigurationConsistencyStatus.NO_ENVIRONMENTS_FOR_OS_EPICS);
                }
            }
        });
    }

    /**
     * Only the IOC name is stored in persisted storage.
     * This method populates the IOC reference transient fields in the loaded configuration with data from the CCDB.
     * @param config The configuration for which to update the IOC object transient fields
     * @throws ConfigClientException
     */
    private void updateConfigIOC(Configuration config) throws ConfigClientException {
        final IOC ccdbIoc = iocCache.getIOCByName(config.getIoc().getName());

        if (ccdbIoc != null) {
            IOCService.updateTransientIOCProperties(ccdbIoc, config.getIoc());
        } else {
            config.getConsistencyStatus().add(ConfigurationConsistencyStatus.INVALID_IOC);
        }
    }

    private int getNextConfigurationForIOC(IOC ioc) {
        return ioc != null ? ioc.getLastConfigRevision() + 1 : 0;
    }

    private Stream<ConsistencyStatusValue> validateModuleVersions(Configuration config) {
        // Map from Module Name to Map of Module Version and list of configurations for that version
        // Type aliases would have been nice in Java :(
        final Map<String, Map<String, List<DeviceConfiguration>>> modules = new TreeMap<>();

        // Fill the map
        config.getDevices().stream().forEach(device -> {
            final Module devModule = device.getSupportModule();
            if (devModule != null) {
                final String moduleName = devModule.getName();
                final String moduleVersion = devModule.getModuleVersion();

                Map<String, List<DeviceConfiguration>> versionDevMap = modules.get(moduleName);
                if (versionDevMap == null) {
                    versionDevMap = new HashMap<>();
                    modules.put(moduleName, versionDevMap);
                }

                List<DeviceConfiguration> devList = versionDevMap.get(moduleVersion);
                if (devList == null) {
                    devList = new ArrayList<>();
                    versionDevMap.put(moduleVersion, devList);
                }

                devList.add(device);
            }
        });

        // Check for duplicates and add formated messages
        return modules.entrySet().stream().
                filter(entry -> entry.getValue().size() > 1).
                map(entry -> {
                    return new ConsistencyStatusValue(
                            ConsistencyMsgLevel.ERROR,
                            String.format("Different versions of the module %s used:\n%s",
                                    entry.getKey(),
                                    formatVersions(entry.getValue())
                            ));
                });
    }

    private String formatVersions(final Map<String, List<DeviceConfiguration>> entry) {
        return entry.entrySet().stream().map(inner -> {
            return String.format("%s version with device(s) %s",
                    inner.getKey(),
                    inner.getValue().stream().map(dev -> "\t" + dev.getName()).collect(Collectors.joining(", ")));
        }).collect(Collectors.joining("\n"));
    }
}
