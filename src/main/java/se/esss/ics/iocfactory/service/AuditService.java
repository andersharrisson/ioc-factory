/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.base.Objects;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.AuditEntry;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.util.Util;

/**
 * Service providing functionality for auditing IOCs (comparing the deployed/generated configuration) to the saved
 * configuration
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class AuditService {
    private static final Logger LOG = Logger.getLogger(AuditService.class.getName());

    @Inject private transient IOCConfigsCache iocCache;
    @Inject private transient CachedSetupService setupService;

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB private transient ConfigurationService configService;
    @Inject private transient GenerateService generateService;

    @PersistenceContext private transient EntityManager em;

    /** Retrieves a list of audit entries which is a cartesian product between IOCs and environments
     *
     * Obviously not all IOCs will have complete audit entries.
     * @return
     * @throws ConfigClientException
     */
    public List<AuditEntry> getAuditEntries() throws ConfigClientException {
        // Collect the entries in a map accessable by IOC
        final Map<String, GenerateEntry> genEntries = getLastGenerateEntries().stream().
                collect(Collectors.toMap(
                        ge -> generateIocEnvKey(ge.getIocName(), ge.getEnvironment()),
                        Function.identity()
                ));

        final List<IOC> iocs = iocCache.getIOCs();
        iocs.sort((l,r) -> l.getName().compareTo(r.getName()));

        final List<IOCEnvironment> envs = setupService.loadEnvs();
        envs.sort((l,r) -> l.getName().compareTo(r.getName()));

        final List<AuditEntry> result = new ArrayList<>();

        iocs.forEach(ioc -> {
            envs.forEach(env -> {
                final GenerateEntry generateEntry = genEntries.get(generateIocEnvKey(ioc.getName(), env.getName()));
                if (generateEntry != null) {
                    final AuditEntry auditEntry = new AuditEntry(result.size(), ioc, env.getName(), generateEntry);

                    checkDiffers(auditEntry, generateEntry);
                    result.add(auditEntry);
                }
            });
        });

        return result;
    }

    public Pair<String, String> getEnvAndStFromDisk(AuditEntry auditEntry) {
        final File path = new File(auditEntry.getGenerateEntry().getTargetPath());
        final File env = new File(path, GenerateService.SHELL_OUTPUT_FILENAME);
        final File st = new File(path, GenerateService.OUTPUT_FILENAME);
        try {
            return new ImmutablePair(
                    FileUtils.readFileToString(env, StandardCharsets.US_ASCII),
                    FileUtils.readFileToString(st, StandardCharsets.US_ASCII)
            );
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error while reading the env.sh file on disk", ex);
            return new ImmutablePair("", "");
        }
    }

    public Pair<String, String> getEnvAndStFromConf(AuditEntry auditEntry) throws ConfigClientException,
            GenerateService.GenerationException {
        final Configuration config = configService.loadConfiguration(auditEntry.getGenerateEntry().getConfigId());
        return generateService.previewConfiguration(config);
    }

    private void setDiskModificationTime(AuditEntry auditEntry) {
        final File path = new File(auditEntry.getGenerateEntry().getTargetPath());
        final File env = new File(path, GenerateService.SHELL_OUTPUT_FILENAME);
        final File st = new File(path, GenerateService.OUTPUT_FILENAME);

        final Optional<Long> lastModified = Stream.of(path, env, st).
                filter(File::exists).
                map(File::lastModified).
                max(Long::compare);
        auditEntry.setDiskModificationTime(lastModified.isPresent() ? new Date(lastModified.get()) : null);
    }

    private void checkDiffers(AuditEntry auditEntry, GenerateEntry ge) {
        final Configuration config = em.find(Configuration.class, ge.getConfigId());
        if (config == null) {
            throw new IllegalArgumentException("Should not happen. Audit entry configuration not found in Db.");
        }

        final File path = new File(ge.getTargetPath());
        if (path.exists() && path.isDirectory()) {
            boolean different = !Objects.equal(config.getOutputHash(), calculatePathHash(auditEntry, path));

            auditEntry.setConfigDifferent(different);
            if (different) {
                auditEntry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.WARN,
                        String.format("Configuration in the directory differs from the configuration revision %d",
                                ge.getConfigRevision())));
            }

            setDiskModificationTime(auditEntry);
        } else {
            auditEntry.setConfigDifferent(true);
            auditEntry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                    "IOC deployment directory missing."));
        }
    }

    private static String calculatePathHash(AuditEntry auditEntry, File path) {
        final MessageDigest digest = Util.getSHA1MessageDigest();

        try {
            digestFile(digest, new File(path, GenerateService.SHELL_OUTPUT_FILENAME), auditEntry, true);
            digestFile(digest, new File(path, GenerateService.OUTPUT_FILENAME), auditEntry, false);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return "";
        }

        final String digestString = Hex.encodeHexString(digest.digest());
        LOG.info("File system hash: " + digestString);
        return digestString;
    }

    private static void digestFile(final MessageDigest digest, File path, AuditEntry auditEntry, boolean isEnv)
            throws IOException {

        if (!path.exists() || !path.isFile()) {
            auditEntry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                    String.format("The %s file is missing",
                            isEnv ? GenerateService.SHELL_OUTPUT_FILENAME : GenerateService.OUTPUT_FILENAME)));
            return;
        }

        if (!path.canRead()) {
            auditEntry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                    String.format("The %s file is unreadable by IOC Factory",
                            isEnv ? GenerateService.SHELL_OUTPUT_FILENAME : GenerateService.OUTPUT_FILENAME)));
            return;
        }

        try (final FileInputStream fis = new FileInputStream(path)) {
            DigestUtils.updateDigest(digest, fis);
        } catch (IOException ex) {
            auditEntry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                    String.format("Error while reading the %s file",
                            isEnv ? GenerateService.SHELL_OUTPUT_FILENAME : GenerateService.OUTPUT_FILENAME)));
            LOG.log(Level.SEVERE,
                    String.format("Error while digesting the file %s", path.getAbsolutePath()), ex);
            throw ex;
        }
    }

    private static String generateIocEnvKey(String iocName, String envName) {
        return String.format("%s###%s", iocName, envName);
    }

    private List<GenerateEntry> getLastGenerateEntries() {
        // This subquery returns the id of the last deployment for a IOC (joins GenerateEntry, IOC, IOCEnvironment, Configuration)
        // tables. The configuration id must also exist.
        final String subQuery = "SELECT MAX(ge.id) FROM GenerateEntry AS ge, IOC AS ioc, "
                + "         IOCEnvironment AS env, Configuration AS conf "
                + "WHERE ge.iocName = ioc.name AND ge.environment = env.name AND ge.configId = conf.id "
                + "GROUP BY ge.iocName, ge.environment";

        List<GenerateEntry> result = em.createQuery("SELECT ge FROM GenerateEntry AS ge "
                + "WHERE ge.id IN ( " + subQuery + ") "
                + "ORDER BY ge.iocName, ge.environment").getResultList();

        return result;
    }
}
