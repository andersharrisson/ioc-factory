/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.google.common.collect.ImmutableList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.util.Util;

/**
 * Methods used for inputand  output of a model entity to a YAML text representation
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 * @param <T> model entity object type
 *
 */
public class YamlHelper<T> {
    private static final Logger LOG = Logger.getLogger(YamlHelper.class.getName());
    private final ObjectMapper MAPPER;

    public YamlHelper(boolean ident) {
        final YAMLFactory yamlFactory = new YAMLFactory();
        yamlFactory.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);

        MAPPER = new ObjectMapper(yamlFactory);

        MAPPER.setSerializationInclusion(Include.NON_EMPTY).
                configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).
                setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE).
                setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        if (ident) {
            MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public JsonNode readTree(final String dump) {
        try {
            return MAPPER.readTree(dump);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "readTree failed", ex);
            return null;
        }
    }

    /**
     * Writes the given object using the provided {@link Writer}
     *
     * @param obj the obj to be dumped to YAML form
     * @param outputWriter the writer to be used
     * @throws IOException
     */
    public void writeEntity(T obj, Writer outputWriter) throws IOException {
        MAPPER.writeValue(outputWriter, obj);
    }

    /**
     * Writes the given object as a {@link String}
     *
     * @param obj
     * @return
     */
    public String writeEntityAsString(T obj) {
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            LOG.log(Level.SEVERE, "Yaml dump failed", ex);
            throw new RuntimeException("Yaml dump failed", ex);
        }
    }

    /**
     * Utility method that gets a YAML dump of a entity in ASCII encoded byte array.
     *
     * Does not throw exceptions, returns an empty byte array if an IO Exception during YAML writing occurs.
     *
     * @param obj
     * @return
     */
    public String getEntityDumpQuiet(T obj) {
        final StringWriter writer = new StringWriter(8 * 1024);
        try {
            writeEntity(obj, writer);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return "";
        }
        return writer.toString();
    }

    /**
     * Utility method that dumps the entity to YAML format and also calculates the resulting digest of the YAML
     *
     * The digests are used to quickly compare objects between {@link GenerateEntry} generate entries and
     * persisted objects
     *
     * @param entity
     * @return a {@link Pair} where the left element is the byte array dump and the right element is a String
     * representing the hash in hex encoded form
     * @throws IOException
     */
    public Pair<String, String> getEntityDumpAndDigest(T entity) throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final Writer stringWriter = new StringWriter(16 * 1024);
                
        final MessageDigest digest = writeEntityUsingWriter(entity, stringWriter);
        return new ImmutablePair<>(stringWriter.toString(), Hex.encodeHexString(digest.digest()));
    }

    /**
     * Calculates a digest for a object based on its YAML representation
     *
     * @param config
     * @return a {@link String} digest containing the hash in hex encoded form
     * @throws IOException
     */
    public String getEntityDigest(T config) throws IOException {
        return Hex.encodeHexString( writeEntityUsingWriter(config, 
                new OutputStreamWriter(new NullOutputStream())).digest() );
    }

    public ObjectMapper getMapper() {
        return MAPPER;
    }

    /**
     * Calculates the digest for a model entity based on its YAML representation
     *
     * As opposed to {@link YamlHelper#getEntityDigest(java.lang.Object){@code null} value
     * is returned.
     *
     * @param obj
     * @return
     */
    public String getEntityDigestQuiet(T obj) {
        try {
            return getEntityDigest(obj);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private MessageDigest writeEntityUsingWriter(T entity, Writer writer)
            throws IOException {
        final MessageDigest digest = Util.getSHA1MessageDigest();
        final OutputStreamWriter digestWriter = new OutputStreamWriter(new DigestOutputStream(new NullOutputStream(), 
                digest));
        final JoinedWriter joinedWriter = new JoinedWriter(ImmutableList.of(writer, digestWriter));
        try {
            writeEntity(entity, joinedWriter);
        } catch(IOException e) {
            throw e;
        }
        return digest;
    }
    
    private static class JoinedWriter extends Writer {
        private final List<Writer> writers;

        public JoinedWriter(List<Writer> writers) {
            this.writers = writers;
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            for (final Writer w : writers) {
                w.write(cbuf, off, len);
            }
        }

        @Override
        public void flush() throws IOException { for (final Writer w : writers) { w.flush(); } }

        @Override
        public void close() throws IOException { for (final Writer w : writers) { try { w.close(); } finally{} } }
    }
}
