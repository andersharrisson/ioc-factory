/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCDevice;

/**
 * A dummy implementation of a {@link ConfigClientService}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Stateless
@SuppressWarnings({
    // String literals should not be duplicated
    "squid:S1192"
})
public class DummyConfigClientService implements ConfigClientService {
    @Override
    public List<IOC> getAllIOCs() {

        return ImmutableList.of(
                new IOC("ISrc-01:Ctrl-IOC-01", "ISRC_IOC", "SL6-x86_64", "somehost",
                        "This is IOC ISrc-01:Ctrl-IOC-01 of type ISRC_IOC"),
                new IOC("LEBT-01:Ctrl-IOC-01", "LEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC LEBT-01:Ctrl-IOC-01 of type LEBT_IOC"),
                new IOC("LEBT-01:Ctrl-IOC-02", "LEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC LEBT-01:Ctrl-IOC-02 of type LEBT_IOC"),
                new IOC("LEBT-02:Ctrl-IOC-01", "LEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC LEBT-02:Ctrl-IOC-01 of type LEBT_IOC"),
                new IOC("LEBT-02:Ctrl-IOC-02", "LEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC LEBT-02:Ctrl-IOC-02 of type LEBT_IOC"),
                new IOC("MEBT-010LWU:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-010LWU:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-020LWU:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-020LWU:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-030LWU:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-030LWU:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-040LWU:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-040LWU:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-010Crm:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-010Crm:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-010Crm:Ctrl-IOC-02", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-010Crm:Ctrl-IOC-02 of type MEBT_IOC"),
                new IOC("MEBT-020Crm:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-020Crm:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-020Crm:Ctrl-IOC-02", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-020Crm:Ctrl-IOC-02 of type MEBT_IOC"),
                new IOC("MEBT-030Crm:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-030Crm:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-030Crm:Ctrl-IOC-02", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-030Crm:Ctrl-IOC-02 of type MEBT_IOC"),
                new IOC("MEBT-040Crm:Ctrl-IOC-01", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is IOC MEBT-040Crm:Ctrl-IOC-01 of type MEBT_IOC"),
                new IOC("MEBT-040Crm:Ctrl-IOC-02", "MEBT_IOC", "SL6-x86_64", "somehost",
                        "This is MEBT-040Crm:Ctrl-IOC-02 of type MEBT_IOC")
        );
    }

    @Override
    public List<IOCDevice> getDevicesForIOC(String iocName) {
        return Arrays.asList(
                new IOCDevice("Dev-01:1", "Type1", "tpmac", ImmutableList.of("st"), "Device desc for Dev-01:1"),
                new IOCDevice("Device2", "Type2", "tpmac" , ImmutableList.of("st"), "Device desc for Device2"),
                new IOCDevice("Device3", "Type3", "tpmac" , ImmutableList.of("st"), "Device desc for Device3"),
                new IOCDevice("Device4", "Type4", "tpmac" , ImmutableList.of("st"), "Device desc for Device4"));
    }
}
