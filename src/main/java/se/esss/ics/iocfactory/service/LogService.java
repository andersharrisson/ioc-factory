/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.log.ConfigAddedEntryV1;
import se.esss.ics.iocfactory.model.log.ConfigGeneratedV1;
import se.esss.ics.iocfactory.model.log.LogEntry;
import se.esss.ics.iocfactory.model.log.LogEntryType;
import se.esss.ics.iocfactory.model.log.LogEntry_;
import se.esss.ics.iocfactory.model.log.SettingsSavedV1;
import se.esss.ics.iocfactory.util.LazyLoadUtil;

/**
 * Provides logging into a database table for auditing purposes
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class LogService {

    private static final YamlHelper YAML_HELPER = new YamlHelper(false);

    @PersistenceContext
    private EntityManager em;

    @Inject
    private SecurityService securityService;
    
    /***
     * Get single entry by id
     * 
     * @param id table key
     * @return LogEntry mapped table row to LogEntry entity
     */
    public LogEntry getLogEntryById(Long id) {
        return em.find(LogEntry.class, id);
    }

    /***
     * Retrieves Sorted, filtered and limited list of rows from Log_Entry table
     * 
     * @param first starting row
     * @param pageSize number of rows to fetch
     * @param sortColumn column to sort (nullable)
     * @param sortOrder independent order of sorting (nullable)
     * @param filters (nullable) wrapped filter values
     * @return list of LogEntry values
     */
    public List<LogEntry> getLogEntries(int first, int pageSize, LogSortColumns sortColumn, 
            IndependentSortOrder sortOrder, LogFilterValues filters) {

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<LogEntry> gcq = cb.createQuery(LogEntry.class);
        final Root<LogEntry> iocEntry = gcq.from(LogEntry.class);

        if (filters != null) {
            addfiltersToQuery(filters, cb, gcq, iocEntry);
        }
        addSortingToQuery(sortColumn, sortOrder, cb, gcq, iocEntry);

        final TypedQuery<LogEntry> query = em.createQuery(gcq);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    /***
     * 
     * Adds filter fields and values to query
     * 
     * @param filters (nullable) wrapped values of filters in LogFilterValues class
     * @param cb criteria builder context
     * @param gcq criteria query context
     * @param rootEntry query root entry
     * @throws Exception any Exception
     */
    private void addfiltersToQuery(LogFilterValues filters, CriteriaBuilder cb, 
            CriteriaQuery<?> gcq, Root<LogEntry> rootEntry) {
        final List<Predicate> predicates = new ArrayList<Predicate>();
        // string filters
        if (filters.getDetails() != null) {
            predicates.add(cb.like(rootEntry.get(LogEntry_.details), "%" + 
                    LazyLoadUtil.escapeSqlLikeChars(filters.getDetails()) + "%"));
        }
        if (filters.getUser() != null) {
            predicates.add(cb.like(rootEntry.get(LogEntry_.user), "%" + 
                    LazyLoadUtil.escapeSqlLikeChars(filters.getUser()) + "%"));
        }

        // eval
        if (filters.getType() != null) {
            predicates.add(cb.equal(rootEntry.get(LogEntry_.type), filters.getType()));
        }

        // DateTime
        if (filters.getLogTimestamp() != null) {
            predicates.add(cb.greaterThanOrEqualTo(rootEntry.get(LogEntry_.timestamp), filters.getLogTimestamp() ));
        }
        gcq.where(predicates.toArray(new Predicate[] {}));
    }

    /***
     * Counts all data (filtered) in table
     * 
     * @param filters type LogFilterValues (nullable) - filter fields and values
     * @return integer - count of all rows in query
     * @throws Exception
     */
    public long getLogEntryCount(LogFilterValues filters) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Long> gcq = cb.createQuery(Long.class);
        final Root<LogEntry> countRoot = gcq.from(LogEntry.class);
        if (filters != null) {
            addfiltersToQuery(filters, cb, gcq, countRoot);
        }
        gcq.select(cb.count(countRoot));
        return em.createQuery(gcq).getSingleResult();
    }

    /***
     * 
     * * Add sorting to Criteria API query
     * 
     * @param sortCol on which column we sort
     * @param sortOrder order of sorting
     * @param cb criteria builder context
     * @param gcq criteria query context
     * @param iocEntry query root
     */
    private void addSortingToQuery(LogSortColumns sortCol, IndependentSortOrder sortOrder, CriteriaBuilder cb, 
            CriteriaQuery<LogEntry> gcq, Root<LogEntry> iocEntry) {
        if (sortCol == null) {
            return;
        }
        switch (sortCol) {
            case USER:
                LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(LogEntry_.user), cb, gcq);
                break;
            case DETAILS:
                LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(LogEntry_.details), cb, gcq);
                break;
            case TYPE:
                LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(LogEntry_.type), cb, gcq);
                break;
            case TIMESTAMP:
                LazyLoadUtil.addSortingToCaQuery(sortOrder, iocEntry.get(LogEntry_.timestamp), cb, gcq);
                break;
            default:
                throw new AssertionError(sortCol.name());
        }
    }

    /***
     * Get all entries from the table. Use with caution.
     * 
     * @return list of LogEntry entry values
     */
    public List<LogEntry> getLogEntries() {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<LogEntry> gcq = cb.createQuery(LogEntry.class);
        return em.createQuery(gcq).getResultList();
    }

    public void logConfigAdded(Configuration config) {
        final LogEntry logEntry = new LogEntry(securityService.getUsername(), LogEntryType.CONFIG_ADDED_V1,
                YAML_HELPER.writeEntityAsString(new ConfigAddedEntryV1(config)));
        em.persist(logEntry);
    }

    public void logConfigDeleted(Configuration config) {
        final LogEntry logEntry = new LogEntry(securityService.getUsername(), LogEntryType.CONFIG_DELTED_V1,
                YAML_HELPER.writeEntityAsString(new ConfigAddedEntryV1(config)));
        em.persist(logEntry);
    }

    public void logConfigSaved(Configuration config) {
        final LogEntry logEntry = new LogEntry(securityService.getUsername(), LogEntryType.CONFIG_SAVED_V1, 
                YAML_HELPER.writeEntityAsString(config));
        em.persist(logEntry);
    }

    public void logConfigGenerated(Configuration config, GenerateEntry ge, String desc) {
        final LogEntry logEntry = new LogEntry(securityService.getUsername(), LogEntryType.CONFIG_GENERATED_V1,
                YAML_HELPER.writeEntityAsString(new ConfigGeneratedV1(config, ge, desc)));
        em.persist(logEntry);
    }

    public void logSettingsSaved(IOCFactSetup setup, List<IOCEnvironment> envs) {
        final LogEntry logEntry = new LogEntry(securityService.getUsername(), LogEntryType.SETTINGS_SAVED_V1,
                YAML_HELPER.writeEntityAsString(new SettingsSavedV1(setup, envs)));
        em.persist(logEntry);
    }

}
