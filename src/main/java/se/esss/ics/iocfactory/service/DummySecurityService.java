/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Alternative;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Dummy implementation of {@link SecurityService} with one hard coded user supported
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@SessionScoped
@Alternative
public class DummySecurityService implements SecurityService, Serializable {
    private static final Logger LOG = Logger.getLogger(DummySecurityService.class.getName());

    private boolean loggedIn = false;
    private String username;


    @Override
    public boolean doLogin(String user, String password) {
        this.username = user;
        try {
            getRequest().login(user, password);
            loggedIn = true;
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, null, ex);
            username = null;
            loggedIn = false;
        }

        return loggedIn;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void doLogout() {
        try {
            getRequest().logout();
            getRequest().getSession().invalidate();
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        loggedIn = false;
        username = null;
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public boolean canAdminister() {
        return getRequest().isUserInRole(IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION);
    }

    @Override
    public boolean canConfigureIOCs() {
        return getRequest().isUserInRole(IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION);
    }

    @Override
    public boolean canDeployNonProduction() {
        return getRequest().isUserInRole(IOCFactoryRBACDefinitions.DEPLOY_NON_PRODUCTION);
    }

    @Override
    public boolean canDeployProduction() {
        return getRequest().isUserInRole(IOCFactoryRBACDefinitions.DEPLOY_PRODUCTION);
    }

    private HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
}
