/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.esss.ics.iocfactory.model.UserSetting;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class Settings {
    @PersistenceContext
    private EntityManager em;

    @Inject
    private SecurityService securityService;

    public String getSetting(final String key) {
        final String username = securityService.getUsername();
        final UserSetting us = getUserSetting(username, key);
        return us != null ? us.getValue() : null;
    }

    public void setSetting(final String key, final String value) {
        final String username = securityService.getUsername();
        final UserSetting existing = getUserSetting(username, key);
        if (existing != null) {
            existing.setValue(value);
            em.merge(existing);
        } else if (username != null) {
            em.persist(new UserSetting(username, key, value));
        }
    }

    private UserSetting getUserSetting(final String username, final String key) {
        if (key != null && username != null) {
            return (UserSetting) Util.getSingleResult(
                    em.createQuery("select us from UserSetting us where us.username = :username and us.key = :key").
                    setParameter("username", username).
                    setParameter("key", key)
            );
        } else {
            return null;
        }
    }
}
