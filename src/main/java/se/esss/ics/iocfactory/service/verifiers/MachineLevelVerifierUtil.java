/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 * 
 * This file is part of IOC Factory.
 * 
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.service.BrowseService;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author mpavleski
 */
public class MachineLevelVerifierUtil {
    @Inject
    private transient BrowseService browseService;
    
    /**
     * Retrieves a stream of all generate entries that are deployed for other IOCs on the same hostname and environment
     * 
     * @param config
     * @param env
     * @return 
     */
    public Stream<GenerateEntry> otherGenerateEntriesStream(final Configuration config, final IOCEnvironment env) {
        final List<GenerateEntry> generateEntries = browseService.getGenerateEntries();
        
        final Map<String, List<GenerateEntry>> byIocName = generateEntries.stream().
                filter(ge -> Objects.equals(ge.getEnvironment(), env.getName()) &&
                        Objects.equals(ge.getHostname(), config.getIoc().getHostname()) &&
                        !Objects.equals(ge.getIocName(), config.getIoc().getName())).
                collect(Collectors.groupingBy(GenerateEntry::getIocName));
        
        return byIocName.entrySet().stream().flatMap(entry -> Util.streamFromOptional(entry.getValue().stream().max(
                        MachineLevelVerifierUtil::genEntryComparator)));
    }
    
     /**
     * Compares two generate entries. Bigger is considered the one with higher revision (1st) and if same revision,
     * higher entity id (newer entry, 2nd)
     *
     * @param l
     * @param r
     * @return
     */
    public static int genEntryComparator(final GenerateEntry l, final GenerateEntry r) {
        final int revComp = Integer.compare(l.getConfigRevision(), r.getConfigRevision());
        if (revComp != 0) {
            return revComp;
        } else {
            return Long.compare(l.getId(), r.getId());
        }
    }
}
