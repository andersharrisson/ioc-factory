/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.base.Objects;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.INVALID_OS;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.IOC_NOT_IN_CCDB;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.NEW_IOC;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.cdievents.IOCRemoved;
import se.esss.ics.iocfactory.util.Util;

/**
 * A service that provides access to the IOC configurations stored in the DB
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 *
 */
@Stateless
public class IOCService {
    @PersistenceContext private EntityManager em;

    @Inject private transient ConfigClientService configClient;

    @Inject
    @IOCRemoved
    private transient Event<IOC> iocRemovedEvent;


    /**
     * Gets JPA managed version of an IOC
     *
     * The method checks in DB if there is already an IOC like that (name & os).
     *
     * @param ioc the IOC object
     * @return managed JPA ioc object
     */
    public IOC saveIOC(IOC ioc) {
        final IOC result = (IOC) Util.getSingleResult(em.createQuery("select ioc from IOC ioc where "
                + "ioc.name = :ioc_name").
                setParameter("ioc_name", ioc.getName()));

        if (result != null) {
            // Update transient stuff
            updateTransientIOCProperties(ioc, result);
            return result;
        } else {
            return em.merge(ioc);
        }
    }

    public static void updateTransientIOCProperties(IOC src, IOC dest) {
        dest.setDesc(src.getDesc());
        dest.setOs(src.getOs());
        dest.setHostname(src.getHostname());
        dest.setType(src.getType());

        dest.getConsistencyStatus().clear();
        src.getConsistencyStatus().forEach(cs -> dest.getConsistencyStatus().add(cs));
    }

    /**
     * Loads and processes all IOCs from database, merging information with the CCDB
     *
     * @return a pair containing both list and map (by ioc name) representation of the IOCS
     *
     * @throws ConfigClientException
     */
    public Pair<List<IOC>, Map<String, IOC>> getIOCs() throws ConfigClientException {
        // Get IOCs from DB & detach entities
        final List<IOC> dbIOCs = (List<IOC>) em.createQuery("from IOC i", IOC.class).getResultList();

        dbIOCs.stream().forEach(ioc -> em.detach(ioc));
        final Map<String, IOC> dbIOCsMap = dbIOCs.stream().collect(Collectors.toMap(IOC::getName, ioc -> ioc));

        // Get IOCs from CCDB
        final List<IOC> ccdbIOCs = configClient.getAllIOCs();

        // Update DB IOCs transient properties from the CCDB ones
        ccdbIOCs.stream().forEach((ccdbIOC) -> {
            final IOC dbIOC = dbIOCsMap.get(ccdbIOC.getName());
            if (dbIOC != null) {
                updateTransientIOCProperties(ccdbIOC, dbIOC);
            }
        });

        // Make sure to add IOCs in CCDB not in DB to the DB map
        ccdbIOCs.stream().filter(ccdbIOC -> !dbIOCsMap.containsKey(ccdbIOC.getName())).
            forEach(ccdbIOC -> {
            ccdbIOC.getConsistencyStatus().add(NEW_IOC);

            dbIOCs.add(ccdbIOC);
            dbIOCsMap.put(ccdbIOC.getName(), ccdbIOC);
        });

        // update in CCDB for all IOCs in the map
        dbIOCs.forEach(ioc -> {
            final Optional<IOC> ccdbIOCResult = ccdbIOCs.stream().
                    filter(ccdbIOC -> Objects.equal(ioc.getName(), ccdbIOC.getName())).
                    findFirst();

            if (ccdbIOCResult.isPresent()) {
                ioc.setOs(ccdbIOCResult.get().getOs());
            } else {
                ioc.getConsistencyStatus().add(IOC_NOT_IN_CCDB);
            }
        });

        // Check OS
        final Repository repo = Util.getRepository();
        dbIOCs.stream().forEach((IOC ioc) -> {
            if (!repo.getOsVersions().contains(ioc.getOs())) {
                ioc.getConsistencyStatus().add(INVALID_OS);
            }
        });

        return new ImmutablePair<>(dbIOCs, dbIOCsMap);
    }

    /**
     * Counts the number of configurations in the DB for a given IOC name
     *
     * @param iocName
     * @return
     */
    public Long countIOCDbConfigs(String iocName) {
        return (Long) em.createQuery("SELECT COUNT(c) FROM Configuration c WHERE c.ioc.name = :iocName").
                setParameter("iocName", iocName).getSingleResult();
    }

    /**
     * Deletes an IOC.
     *
     * Only possible for IOCs that have been fully loaded by this service and has
     * {@link IOCConsistencyStatus#IOC_NOT_IN_CCDB} status
     *
     * @param ioc
     * @return true if the delete operation succeeds
     */
    public boolean deleteIOC(IOC ioc) {
        if (ioc == null ||
                !ioc.getConsistencyStatus().contains(IOC_NOT_IN_CCDB)) {
            return false;
        }

        // Get all configurations for the IOC and delete them (Cascade on JPA level will clear all dep. entities)
        final List<Configuration> configs = (List<Configuration>) em.createQuery("SELECT c FROM Configuration c WHERE "
                + "c.ioc.name = :iocName").
                setParameter("iocName", ioc.getName()).getResultList();
        if (configs != null) {
            configs.forEach(config -> em.remove(config));
        }

        final IOC merged = em.merge(ioc);
        em.remove(merged);

        iocRemovedEvent.fire(ioc);

        return true;
    }

}
