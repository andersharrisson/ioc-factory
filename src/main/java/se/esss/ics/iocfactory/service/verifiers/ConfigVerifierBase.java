/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.ConfigVerifyMessages;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.Repository;

/**
 * Convenience base class, sub-classes can extend {@link ConfigVerifierBase#verifyImpl} to implement behavior and use 
 * facilities such as {@link ConfigVerifierBase#addVerifyMessage) 
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public abstract class ConfigVerifierBase implements ConfigVerifier {
    private ConfigVerifyMessages msgs;
    
    @Override
    public void verify(final Configuration config, final ConfigVerifyMessages msgs, final Repository repo,
            final IOCEnvironment env) { 
        this.msgs = msgs;
        verifyImpl(config, repo, env);
    } 
    
    public abstract void verifyImpl(final Configuration config,
            final Repository repo,
            final IOCEnvironment env);
    
    public String addVerifyMessage(ConsistencyMsgLevel level, String msgFormat, Object... args) {
        return msgs.addVerifyStatus(level, getCategory(), msgFormat, args);
    }    
}
