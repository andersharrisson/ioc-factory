/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import java.util.logging.Logger;
import java.util.stream.Collectors;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatus;
import se.esss.ics.iocfactory.model.Repository;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class DeviceVerifier extends ConfigVerifierBase implements ConfigVerifier {
    private static final Logger LOG = Logger.getLogger(DeviceVerifier.class.getName());
    private static final String VERIFIER_CATEGORY = "Devices";

    @Override
    public int getPriority() { return 100; }

    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }          

    @Override
    public void verifyImpl(Configuration config, Repository repo, IOCEnvironment env) {
        config.getDevices().forEach(dev -> {
            if (dev.getConsistencyStatus().isError()) {                
                addVerifyMessage(ConsistencyMsgLevel.ERROR, "Device %s has errors: %s", dev.getName(),
                        dev.getConsistencyStatus().stream().
                                filter(cs -> cs.getLevel() == ConsistencyMsgLevel.ERROR).
                                map(ConsistencyStatus::getDesc).collect(Collectors.joining("; ")));
            }
        });
    }
}
