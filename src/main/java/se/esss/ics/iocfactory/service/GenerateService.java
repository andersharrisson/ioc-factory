/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.Snippet;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;
import se.esss.ics.iocfactory.util.Util;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class GenerateService {
    private static final Logger LOG = Logger.getLogger(GenerateService.class.getName());

    private static final String NAME_PLACEHOLDER = "{iocname}";
    private static final String HOSTNAME_PLACEHOLDER = "{hostname}";
    private static final String REVISION_PLACEHOLDER = "{revisionnumber}";

    public static final String OUTPUT_FILENAME = "st.cmd";
    public static final String SHELL_OUTPUT_FILENAME = "env.sh";

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB private ConfigurationService configService;

    @Inject private CachedSetupService setupService;
    @Inject private SecurityService securityService;

    @EJB private LogService logService;

    @PersistenceContext private EntityManager em;

    private IOCFactSetup loadedSetup = new IOCFactSetup();

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper(true);

    /**
     * Generates IOC shell script for the given configuration and environment
     *
     * @param config the configuraiton object
     * @param env the environent object
     * @return true if the configuraiton object was frozen (if the env was production)
     *
     * @throws se.esss.ics.iocfactory.service.GenerateService.GenerationException
     */
    public boolean generateConfiguration(final Configuration config, final IOCEnvironment env, final String desc)
            throws GenerationException {
        checkPreconditions(config, env);

        loadedSetup = setupService.loadSetup();

        final File dir = new File(getDestFolder(config, env.getDirectory()));

        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new GenerationException("Failed to make the directory for the IOC");
            }
        } else if (!dir.isDirectory()) {
            throw new GenerationException("A file named " + dir.getAbsolutePath() + " already exists.");
        }

        final File targetEnvShFile = new File(dir, SHELL_OUTPUT_FILENAME);
        writeEnvironment(config, targetEnvShFile);

        final File targetIocShFile = new File(dir, OUTPUT_FILENAME);
        writeConfiguration(config, targetIocShFile);

        writeDbEntry(config, dir, env.getName(), env.isProduction(), desc);

        if (env.isProduction()) {
            config.setCommited(true);
            configService.updateConfiguration(config);
            return true;
        } else {
            return false;
        }
    }

    public Pair<String, String> previewConfiguration(Configuration config) throws GenerationException {
        loadedSetup = setupService.loadSetup();
        final String envConfig;
        final String stConfig;

        try (final StringWriter writer = new StringWriter()) {
            writeEnvironment(config, writer);
            envConfig = writer.toString();
        } catch (IOException e) {
            throw new GenerationException("IO error while generating preview for the " + SHELL_OUTPUT_FILENAME, e);
        }

        try (final StringWriter writer = new StringWriter()) {
            writeConfiguration(config, writer);
            stConfig = writer.toString();
        } catch (IOException e) {
            throw new GenerationException("IO error while generating preview for the " + OUTPUT_FILENAME, e);
        }

        return new ImmutablePair<>(envConfig, stConfig);
    }

    /** Calculates a configuration output hash (from generated env.sh and st.cmd) contents
     *
     * @param config the fully loaded configuration for which to calculate
     * @return the hex encoded SHA-1 hash of the output or {@code null} if an exception was thrown
     */
    public String calculateConfigurationOutputHash(Configuration config) {
        loadedSetup = setupService.loadSetup();
        final String envConfig;
        final String stConfig;

        final MessageDigest digest = Util.getSHA1MessageDigest();

        final DigestOutputStream dos = new DigestOutputStream(new NullOutputStream(), digest);
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(dos, StandardCharsets.US_ASCII), false)) {
            writeEnvironment(config, writer);
            writeConfiguration(config, writer);
        } catch(GenerationException | IOException e) {
            return "";
        }

        return Hex.encodeHexString(digest.digest());
    }

    public static String getDestFolder(final Configuration config, final String dirPattern) {
        final String hostname = config.getIoc().getHostname();
        final String iocName = config.getIoc().getName();

        String result = dirPattern;
        if (!StringUtils.isEmpty(hostname)) {
            result = result.replace(HOSTNAME_PLACEHOLDER, hostname);
        }

        if (!StringUtils.isEmpty(iocName)) {
            result = result.replace(NAME_PLACEHOLDER, iocName.replace(":", "_"));
        }

        result = result.replace(REVISION_PLACEHOLDER, Integer.toString(config.getRevision()));

        return result;
    }

    private static String escapePlaceholder(String placeholder) {
        return placeholder.replace("{", "").replace("}", "");
    }

    private void checkPreconditions(Configuration config, IOCEnvironment env) throws GenerationException {
        final String dirString = env.getDirectory();
        if (StringUtils.isEmpty(dirString)) {
            throw new GenerationException("IOC Environment " + escapePlaceholder(env.getName()) +
                    " has empty directory specificaton");
        } else if (!dirString.contains(NAME_PLACEHOLDER)) {
            throw new GenerationException("The IOC environment directory specification"
                + " does not contain the " + escapePlaceholder(NAME_PLACEHOLDER) + " placeholder.");
        } else if (config.getIoc()==null || StringUtils.isEmpty(config.getIoc().getName())) {
            throw new GenerationException("IOC is not specified");
        }
    }

    private void writeEnvironment(Configuration config, File targetEnvShFile) throws GenerationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(targetEnvShFile),
                StandardCharsets.US_ASCII)) {
            writeEnvironment(config, writer);
        } catch (IOException e) {
            throw new GenerationException("IO error while writing " + SHELL_OUTPUT_FILENAME, e);
        }
    }

    private void writeEnvironment(Configuration config, Writer writer) throws IOException {
        writer.append(String.format("PROCSERV_PORT=%d\n" +
                "EPICS_BASE=%s\n" +
                "EPICS_HOST_ARCH=%s\n" +
                "EPICS_MODULES_PATH=%s\n" +
                "ENVIRONMENT_VERSION=%s\n" +
                "BASE=%s\n" , config.getProcServPort(),
                constructEpicsBasePath(config),
                config.getOs(),
                constructModulesBasePath(config),
                config.getEnvVersion() == null ? getEnvironmentModuleLatestVersion() : config.getEnvVersion(),
                VersionConverters.getEpicsConverter().toString(config.getEpicsVersion())
                ));
    }

    private String constructEpicsBasePath(Configuration config) {
        return (new File(new File(loadedSetup.getEpicsBaseDirectory(), "bases"), "base-" +
                VersionConverters.getEpicsConverter().toString(config.getEpicsVersion()))).getAbsolutePath();
    }

    private String getEnvironmentModuleLatestVersion() {
        final Optional<String> lastVersion = Util.getRepository().getModulesForName("environment").stream().
                map(Module::getModuleVersion).max((a,b) -> ModuleVersionsComparator.compare(a,b));

        return lastVersion.isPresent() ? lastVersion.get() : "";
    }

    private String constructModulesBasePath(Configuration config) {
        return (new File(loadedSetup.getEpicsBaseDirectory(), "modules")).getAbsolutePath();
    }

    private void writeConfiguration(Configuration config, File targetIocShFile) throws GenerationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(targetIocShFile),
                StandardCharsets.US_ASCII)) {
            writeConfiguration(config, writer);
        } catch (IOException e) {
            throw new GenerationException("IO error while writing IOCSH script", e);
        }
    }

    private void writeConfiguration(Configuration config, Writer writer) throws IOException, GenerationException {
        writeHeaderComment(config, writer);

        writeGlobals(config, writer);

        for (DeviceConfiguration dev : config.getDevices()) {
            writeDeviceConfig(dev, writer);
        }
    }

    private void writeHeaderComment(Configuration config, Writer writer) throws GenerationException {
        try {
            writer.write("#\n");
            writer.write(String.format("# IOC: %s\n", config.getIoc()));
            writer.write(String.format("# Revision: %d\n", config.getRevision()));
            writer.write(String.format("# OS: %s, EPICS: %s\n#\n", config.getOs(),
                    VersionConverters.getEpicsConverter().toString(config.getEpicsVersion())));
            writer.write("#\n");
            writer.write(String.format("# Description: %s\n", config.getComment()));
            writer.write("#\n#\n\n");
        } catch (IOException ex) {
            throw new GenerationException(String.format("IO Error while writing configuration header for config id %d, "
                    + "IOC %s, revision %d", config.getId(), config.getIoc().getName(), config.getRevision()), ex);
        }
    }

    private void writeGlobals(Configuration config, Writer writer) throws IOException {
        writer.write(String.format("#\n# GLOBAL ENVIRONMENT\n#\n"));
        writeParameters(config.getGlobals(), writer, null);
        writer.write("\n\n");
    }

    private void writeDeviceConfig(DeviceConfiguration dev, Writer writer) throws IOException {
        // Do not output devices with errors
        if (dev.getConsistencyStatus().isError()) {
            return;
        }

        final String modName  = dev.getSupportModule().getName();

        final String modVersion = dev.getSupportModule().getModuleVersion();
        writer.write(String.format("#\n# Device: %s\n# Module: %s %s\n#   Deps:   %s\n#\n",
                dev.getName(), modName, modVersion, Util.formatModuleDependenceies(dev.getSupportModule())));
        writer.write(String.format("\n\nrequire %s, %s\n\n", modName, modVersion));
        
        for (final Snippet snippet : dev.getSnippets()) {
            writeParameters(dev.getParameters(), writer, snippet);           
                        final String snippetPath = snippet.getFile().getAbsolutePath();
            if (!StringUtils.isEmpty(snippetPath)) {
                writer.write(String.format("< %s\n\n", snippetPath));
            }
        }
    }
    
    /**
     * Writes parameters for a given snippet or the global parameters if the snippet is null
     * 
     * @param parameters
     * @param writer
     * @param snippet
     * @throws IOException 
     */
    private void writeParameters(final List<Parameter> parameters, final Writer writer, final Snippet snippet) 
            throws IOException {
        for (Parameter param : parameters) {
            if (snippet == null || Objects.equals(param.getSnippet(), snippet)) {
                final String paramValue = param.getValue();
                writer.write(String.format("epicsEnvSet(%s, \"%s\")\n", param.getName(),
                        paramValue != null ? paramValue : ""));
            }
        }
    }
    
    private void writeDbEntry(Configuration config, File dir, String envName, boolean inProd, String desc)
            throws GenerationException {
        final Pair<String, String> configDumpAndDigest;
        try {
            configDumpAndDigest = YAML_HELPER.getEntityDumpAndDigest(config);
        } catch (IOException ex) {
            throw new GenerationException("Failed while exporting the configuratioin to JSON and computing the SHA1 "
                    + "hash", ex);
        }
        final GenerateEntry entry = new GenerateEntry(
                config.getIoc().getName(),
                config.getIoc().getOs(),
                config.getIoc().getHostname(),
                config.getId(),
                config.getRevision(),
                securityService.getUsername(),
                new Date(),
                envName,
                inProd,
                dir.getAbsolutePath(),
                configDumpAndDigest.getRight(),
                configDumpAndDigest.getLeft(),
                desc
        );
        em.persist(entry);

        // Audit log entry
        logService.logConfigGenerated(config, entry, desc);
    }

    public static class GenerationException extends Exception {
        public GenerationException(String message) { super(message); }
        public GenerationException(String message, Throwable cause) { super(message, cause); }
    }
}
