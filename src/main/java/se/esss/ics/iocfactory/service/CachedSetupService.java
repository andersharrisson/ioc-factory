/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.service.cdievents.SetupSaved;

/**
 * {@link ApplicationScoped} cache for the {@link SetupService} methods
 *
 * Cache is invalidated when the IOC Factory setup is saved
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ApplicationScoped
public class CachedSetupService implements Serializable {
    @Inject private transient SetupService setupService;

    private IOCFactSetup setup = null;
    private List<IOCEnvironment> envs = null;

    /**
     * Cached version of {@link SetupService#loadSetup()}
     * @return
     */
    public IOCFactSetup loadSetup() {
        if (setup == null) {
            setup = setupService.loadSetup();
        }
        return setup;
    }

    /**
     * Cached version of {@link SetupService#loadEnvs()}
     * @return
     */
    public List<IOCEnvironment> loadEnvs() {
        if (envs == null) {
            envs = setupService.loadEnvs();
        }
        return envs;
    }

    /**
     * Observer method handling a {@link SetupSaved} event emitted when the cache needs to be invalidated
     *
     * Invalidates the cache
     * @param date bogus parameter
     */
    public void invalidateCache(@Observes @SetupSaved Date date) {
        envs = null;
        setup = null;
    }
}
