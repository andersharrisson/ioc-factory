/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.discs.client.CCDBClient;
import org.openepics.discs.client.impl.ResponseException;
import org.openepics.discs.conf.jaxb.InstallationSlot;
import org.openepics.discs.conf.jaxb.PropertyValue;
import org.openepics.discs.conf.jaxrs.client.DeviceTypeClient;
import org.openepics.discs.conf.jaxrs.client.InstallationSlotClient;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import se.esss.ics.iocfactory.model.IOCDevice;
import se.esss.ics.iocfactory.service.qualifiers.DirectConfigClient;
import se.esss.ics.iocfactory.util.Util;

/**
 * A dummy implementation of a {@link ConfigClientService}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Stateless
@DirectConfigClient
public class ConfigClientServiceImpl implements ConfigClientService {
    private static final Logger LOG = Logger.getLogger(ConfigClientServiceImpl.class.getName());

    public static final String CCDB_URL_PROPERTY = "iocfactory.ccdb.rest.url";
    public static final String CCDB_URL_DEFAULT = "http://localhost:8080/ccdb/";

    @Inject
    private transient CachedSetupService setupService;

    private static final CSVFormat STRING_LIST_FORMAT = CSVFormat.DEFAULT.
            withSkipHeaderRecord(true).withDelimiter(',').withIgnoreSurroundingSpaces().withQuote('"');

    private InstallationSlotClient slotClient;
    private DeviceTypeClient devTypeClient;

    /** Initializes the client from System properties
     */
    @PostConstruct
    public void init() {
        final Properties clientProps = new Properties();

        final String sysPropUrl = System.getProperty(CCDB_URL_PROPERTY, CCDB_URL_DEFAULT);

        clientProps.put(CCDBClient.PROPERTY_NAME_BASE_URL, Util.ensureTrailingSlash(sysPropUrl) + "rest");

        // Dummy values as they are required
        clientProps.put(CCDBClient.PROPERTY_NAME_USERNAME, "user");
        clientProps.put(CCDBClient.PROPERTY_NAME_PASSWORD, "pass");

        slotClient = CCDBClient.createInstallationSlotClient(clientProps);
        devTypeClient = CCDBClient.createDeviceTypeClient(clientProps);
    }



    /**
     * {@inheritDoc}
     *
     * @return
     * @throws ConfigClientException
     */
    @Override
    public List<IOC> getAllIOCs() throws ConfigClientException {
        final Pattern iocDevNameRegEx = Pattern.compile(setupService.loadSetup().getIocDeviceTypes());
        final IOCFactSetup loadedSetup = setupService.loadSetup();

        try {
            return devTypeClient.getAllDeviceTypes().
                    stream().map(devType -> devType.getName()).
                    filter(devType -> iocDevNameRegEx.matcher(devType).matches()).
                    flatMap(devTypeName -> slotClient.getInstallationSlots(devTypeName).stream()).
                    map(slot -> slotToIOC(slot, loadedSetup.getCcdbOsProperty(), loadedSetup.getCcdbHostnameProperty())).
                    collect(Collectors.toList());
        } catch(ResponseException re) {
            LOG.log(Level.SEVERE, re.getMessage(), re);
            throw new ConfigClientException(re);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param iocName
     * @return
     * @throws ConfigClientException
     */
    @Override
    public List<IOCDevice> getDevicesForIOC(String iocName) throws ConfigClientException {
        try {
            final IOCFactSetup setup = setupService.loadSetup();
            final String modulePropName = setup.getCcdbDependencyPropery();
            final String snippetPropName = setup.getCcdbSnippetProperty();
            
            final InstallationSlot iocSlot = slotClient.getInstallationSlot(iocName);

            final Map<String, InstallationSlot> slotCache = Stream.concat(
                    Stream.of(iocSlot), slotClient.getControlsChildren(iocName, true,
                    Collections.emptyList()).stream()).
                    collect(Collectors.toMap(InstallationSlot::getName, Function.identity()));

            final Set<String> propNames = ImmutableSet.of(modulePropName, snippetPropName);

            // To prevent repetition of the flattened stream
            final Set<String> visited = new HashSet<>();

            // The iocSlotStream will contain a device entry for the IOC itself if there is any
            final Stream<InstallationSlot> iocSlotStream =
                    Stream.of(iocSlot).filter( slot -> hasStringListPropertyValues(slot, propNames));

            // The conntrolled devices stream
            final Stream<InstallationSlot> devicesSlotStream =
                    flattenControlsSlots(iocSlot, slotCache).
                    filter(slot -> {
                        if (visited.contains(slot.getName())) {
                            return false;
                        } else {
                            visited.add(slot.getName());
                            return hasStringListPropertyValues(slot, propNames);
                        }
                    });

            return Stream.concat(iocSlotStream, devicesSlotStream).
                    map(slot -> slotToDevice(slot, modulePropName, snippetPropName)).
                    collect(Collectors.toList());
        
        } catch(ResponseException re) {
            LOG.log(Level.SEVERE, re.getMessage(), re);
            throw new ConfigClientException(re);
        }
    }
    
    private static InstallationSlot slotForName(final String slotName, final Map<String, InstallationSlot> slotCache) {
        return slotCache.get(slotName);
    }
    
    private static Stream<InstallationSlot> flattenControlsSlots(final InstallationSlot slot, 
            final Map<String, InstallationSlot> slotCache) {
        // A visited set to prevent infinite loops
        final Set<String> visited = new HashSet<>();
        return flattenControlsSlotsInternal(slot, visited, slotCache);
    }

    private static Stream<InstallationSlot> flattenControlsSlotsInternal(InstallationSlot slot,
            final Set<String> visited, final Map<String, InstallationSlot> slotCache) {
        if (visited.contains(slot.getName())) {
            return Stream.empty();
        } else {
            visited.add(slot.getName());

            return slot.getControls() != null ? Stream.concat(
                            slot.getControls().stream().map(slotName -> slotForName(slotName, slotCache)).
                                    filter(islot -> islot != null),
                            slot.getControls().stream().map(slotName -> slotForName(slotName, slotCache)).
                                    filter(islot -> islot != null).
                            flatMap(s -> flattenControlsSlotsInternal(s, visited, slotCache))
                    ) : Stream.empty();
        }
    }

    private IOC slotToIOC(InstallationSlot slot, String osPropName, String hostnamePropName) {
        if (slot.getParents() != null && slot.getParents().size() == 1) {
            final InstallationSlot parentSlot = slotClient.getInstallationSlot(slot.getParents().get(0));

            final String explicitHostname = getStringPropertyValue(parentSlot, hostnamePropName);

            return new IOC(StringUtils.trim(slot.getName()),
                    StringUtils.trim(slot.getDeviceType()),
                    getPropertyValue(parentSlot, osPropName).orElse(null),
                    StringUtils.isEmpty(explicitHostname) ? namingConvNameToHostname(parentSlot.getName()) :
                            explicitHostname,
                    StringUtils.trim(slot.getDescription()));
        } else {
            final IOC result = new IOC(StringUtils.trim(slot.getName()),
                    StringUtils.trim(slot.getDeviceType()), null, null,
                    StringUtils.trim(slot.getDescription()));
            result.getConsistencyStatus().add(IOCConsistencyStatus.INVALID_CCDB_STRUCTURE);
            return result;
        }
    }

    private static String namingConvNameToHostname(String convName) {
        final String[] elems = convName.split("\\:");
        ArrayUtils.subarray(elems, 1, elems.length);

        List<String> rotated;
        if (elems.length < 2) {
            rotated = Arrays.asList(elems);
        } else {
            rotated = new ArrayList<>();
            for (int i=1; i<elems.length; i++) {
                rotated.add(elems[i]);
            }
            rotated.add(elems[0]);
        }

        final StringBuilder sb = new StringBuilder();
        rotated.forEach(elem -> {
            sb.append(elem.toLowerCase());
            sb.append('-');
        });
        return sb.substring(0, sb.length()-1);
    }

    private static IOCDevice slotToDevice(InstallationSlot slot, String modulePropName, String snippetPropName) {
        return new IOCDevice(StringUtils.trim(slot.getName()),
                slot.getDeviceType()!=null ? StringUtils.trim(slot.getDeviceType()) : "",
                getStringListPropertyValue(slot, modulePropName),
                getStringListPropertiesValue(slot, snippetPropName),
                StringUtils.trim(slot.getDescription()));
    }

    private static Optional<String> getPropertyValue(InstallationSlot slot, String propName) {
        if (slot==null || slot.getProperties()==null) {
            return Optional.empty();
        }
        return slot.getProperties().stream().
                filter(prop -> StringUtils.equals(prop.getName(), propName)).
                findFirst().map(PropertyValue::getValue);
    }

    private static boolean hasStringListPropertyValues(InstallationSlot slot, Set<String> propNames) {
        if (slot==null || slot.getProperties()==null) {
            return false;
        }

        long matched = slot.getProperties().stream().
                filter(prop -> "Strings List".equals(prop.getDataType())).
                filter(prop -> propNames.contains(prop.getName()) && !getFirstCsvValue(prop.getValue()).isEmpty()).
                collect(Collectors.counting());

        return propNames.size() == matched;
    }

    
    
    private static String getStringListPropertyValue(InstallationSlot slot, String propName) {
        if (slot==null || slot.getProperties()==null) {
            return "";
        }

        Optional<String> propValue = slot.getProperties().stream().
                filter(prop -> "Strings List".equals(prop.getDataType())).
                filter(prop -> StringUtils.equals(prop.getName(), propName) && isNonEmptyList(prop.getValue())).
                findFirst().map(PropertyValue::getValue);
        if (propValue.isPresent()) {
            return StringUtils.trim(getFirstCsvValue(propValue.get()));
        } else {
            return "";
        }
    }
    
    private static List<String> getStringListPropertiesValue(InstallationSlot slot, String propName) {
        if (slot==null || slot.getProperties()==null) {
            return Collections.emptyList();
        }

        Optional<String> propValue = slot.getProperties().stream().
                filter(prop -> "Strings List".equals(prop.getDataType())).
                filter(prop -> StringUtils.equals(prop.getName(), propName) && isNonEmptyList(prop.getValue())).
                findFirst().map(PropertyValue::getValue);
        if (propValue.isPresent()) {
            return getCsvValueEntries(propValue.get());
        } else {
            return Collections.emptyList();
        }
    }

    private static String getStringPropertyValue(InstallationSlot slot, String propName) {
        if (slot==null || slot.getProperties()==null) {
            return "";
        }

        Optional<String> propValue = slot.getProperties().stream().
                filter(prop -> "String".equals(prop.getDataType())).
                filter(prop -> StringUtils.equals(prop.getName(), propName) && !"null".equals(prop.getValue())).
                findFirst().map(PropertyValue::getValue);
        if (propValue.isPresent()) {
            return StringUtils.trim(propValue.get());
        } else {
            return "";
        }
    }  
    
    private static String getFirstCsvValue(String value) {
        final CSVRecord record = getFirstCsvRecord(value);
        return record != null && record.size() > 0 ? StringUtils.trim(record.get(0)) : "";
    }
    
    private static List<String> getCsvValueEntries(String value) {
        final CSVRecord record = getFirstCsvRecord(value);
        if (record != null) {
            final ImmutableList.Builder<String> listBldr = ImmutableList.builder();
            for (final String entry : record) {
                if (!StringUtils.isEmpty(entry)) {
                    listBldr.add(StringUtils.trim(entry));
                }
            }
            return listBldr.build();
        } else {
            return Collections.emptyList();
        }
    }
    
    private static boolean isNonEmptyList(String value) {
        return value != null && !value.isEmpty() && !"null".equals(value) && value.startsWith("[") && 
                value.endsWith("]");
    }
    
    private static CSVRecord getFirstCsvRecord(String value) {
        if (isNonEmptyList(value)) {
            final String withoutSquareBrakets = value.substring(1,value.length()-1);
            final List<CSVRecord> records;
            try {
                records = CSVParser.parse(withoutSquareBrakets, STRING_LIST_FORMAT).getRecords();
                if (records == null || records.isEmpty()) {
                    return null;
                }
                return records.get(0);
            } catch (IOException ex) {
                LOG.log(Level.WARNING, ex.getMessage(), ex);
                return null;
            }
        } else {
            return null;
        }
    } 
}
