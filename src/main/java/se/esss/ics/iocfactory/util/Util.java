/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.ejb.EJBAccessException;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.commons.codec.digest.DigestUtils;
import org.omnifaces.util.Components;
import org.omnifaces.util.Messages;
import org.primefaces.context.RequestContext;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.RepositoryProducer;
import se.esss.ics.iocfactory.service.RepositoryScanner;

/**
 * A project wide utility function class.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class Util {
    private static final Logger LOG = Logger.getLogger(Util.class.getName());

    /**
     * Private constructor to prevent class instantiation
     */
    private Util() {
    }

    /**
     * {@link EntityManager} helper for getting a single result without exception handling
     *
     * @param query a {@link Query} object
     * @return the single value or <code>null</code> if not present
     */
    public static Object getSingleResult(Query query) {
        query.setMaxResults(1);
        List<?> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }

    public static Repository getRepository() {
         return CDI.current().select(RepositoryProducer.class).get().getRepository();
    }
    
    public static void checkRepoChanged() { CDI.current().select(RepositoryProducer.class).get().checkRepoChanged(); }
    
    /**
     * Used in UI controller code to invalidate a UIInput component with a given error message
     *
     * @param clientId the component id
     * @param message the error message
     */
    public static void invalidateComponent(String clientId, String message) {
        final UIInput input = (UIInput) Components.findComponent(clientId);
        if (input != null) {
            input.setValid(false);
            Messages.add(FacesMessage.SEVERITY_ERROR, clientId, escapeNewlinesAndTabsForHTML(message));
            FacesContext.getCurrentInstance().validationFailed();
            updateGrowl();
        }

    }

    public static void addGlobalInfo(String message, Object... params) {
        Messages.addGlobalInfo(escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    public static void addGlobalWarn(String message, Object... params) {
        Messages.addGlobalWarn(escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    public static void addGlobalError(String message, Object... params) {
        Messages.addGlobalError(escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    public static String ensureTrailingSlash(String str) {
        return str.endsWith("/") ? str : str + "/";
    }

    public static String urlEncode(String input) {
        try {
            return input != null ? URLEncoder.encode(input, "UTF-8") : null;
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("Cataclysm UTF-8 is not a standard charset on the platform", ex);
        }
    }

    /**
     * Pretty prints the dependencies of a module
     *
     * @param depList
     * @return
     */
    public static String formatModuleDependenceies(List<ModuleDependency> depList) {
        return depList.stream().map(dep
                -> String.format("%s-%s%s", dep.getName(),
                        dep.getVersion(),
                        dep.isGreater() ? "+" : "")).collect(Collectors.joining(", "));
    }

    /**
     * Pretty prints the dependencies of a module
     *
     * @param module
     * @return
     */
    public static String formatModuleDependenceies(Module module) {
        if (module == null) {
            return null;
        }
        return formatModuleDependenceies(module.getDependencies());
    }

    /**
     * Tests that a File is a readable directory
     *
     * @param dir the file to test
     * @return true if it is a readable directory
     */
    public static boolean testIfReadableDir(File dir) {
        return dir != null && dir.exists() && dir.isDirectory() && dir.canRead();
    }

    public static MessageDigest getSHA1MessageDigest() { return DigestUtils.getDigest("SHA-1"); }

    private static void updateGrowl() {
        RequestContext.getCurrentInstance().update("growl");
    }

    private static String escape(String s) {
        return s.replaceAll("\\\\", "\\\\\\\\").replaceAll("'", "\\\\'").replaceAll("\n", "\\\\n");
    }

    public static void showDialogInfo(String header, String message) {
        showDialog(FacesMessage.SEVERITY_INFO, header, message);
    }

    public static void showDialogError(String header, String message) {
        showDialog(FacesMessage.SEVERITY_ERROR, header, message);
    }

    public static void showDialog(Severity severity, String header, String message) {
        final FacesMessage facesMessage = new FacesMessage(severity, escape(header), escape(message));
        RequestContext.getCurrentInstance().showMessageInDialog(facesMessage);
    }

    public static OptionalProcedure callSecuredEJB(Logger log, Runnable processor) {
        try {
            processor.run();
        } catch (EJBAccessException accessException)
        {
            log.log(Level.SEVERE, "Access denied", accessException);
            addGlobalError("Access denied for the operation");
            return new OptionalProcedure(false);
        }
        return new OptionalProcedure(true);
    }

    public static String escapeNewlinesAndTabsForHTML(String input) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            final char val = input.charAt(i);
            if (val == '\n') {
                sb.append("<br />");
            } else if (val == '\t') {
                sb.append("&nbsp&nbsp&nbsp&nbsp");
            } else {
                sb.append(val);
            }
        }
        return sb.toString();
    }

    public static String getViewId() { return FacesContext.getCurrentInstance().getViewRoot().getViewId(); }

    public static Repository loadSpecificRepo(final EEEType eeeType, final IOCFactSetup setup) {
        LOG.fine(String.format("Loading NEW EEE %s Repo", eeeType.getName()));
        final String baseDir = eeeType == EEEType.PRODUCTION ? setup.getEpicsBaseDirectory() :
                setup.getSandboxBaseDirectory();
        final RepositoryScanner repoScanner = new RepositoryScanner(new File(baseDir != null ?
                baseDir : "invalid dir"));
        final Repository repo = repoScanner.scanRepository(null);
        repo.setLastScanUser("SYSTEM");
        return repo;
    }
    
    public static <T> Stream<T> streamFromOptional(final Optional<T> opt) {
        if (opt.isPresent()) {
            return Stream.of(opt.get());
        } else {
            return Stream.empty();
        }
    }
    
    public static <T> Stream<T> streamFromIterator(final Iterator<T> iter) {
        final Iterable<T> iterable = () -> iter;
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
