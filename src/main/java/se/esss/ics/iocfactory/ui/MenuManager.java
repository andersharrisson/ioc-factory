/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;


import java.io.Serializable;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Faces;
import se.esss.ics.iocfactory.ui.cdievents.MenuSwitched;

/**
 * Session scoped class which holds the selected EEE environment to use
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@ViewScoped
public class MenuManager implements Serializable {
    private static final String FACES_REDIRECT = "?faces-redirect=true";
    private boolean proceed = true;
    private String lastRequestedViewId = null;
    
    @Inject
    @MenuSwitched
    private Event<String> menuEvent;  

    /**
     * Method will invoke an {@link MenuSwitched} event and will proceed switching to the specified viewId
     * if not interrupted, by invocation to {@link MenuManager#cancelNavigation()}
     * 
     * @param viewId
     * @return 
     */
    public String fireMenuEvent(final String viewId) 
    {
        lastRequestedViewId = viewId;
        proceed = true;
        menuEvent.fire(viewId);
        return proceed ? viewId + FACES_REDIRECT : null;
    }
    
    public void cancelNavigation() { this.proceed = false; }
    
    public void continueNavigation() {
        if (lastRequestedViewId != null) {
            Faces.navigate(lastRequestedViewId + FACES_REDIRECT);
        }        
    }
}
