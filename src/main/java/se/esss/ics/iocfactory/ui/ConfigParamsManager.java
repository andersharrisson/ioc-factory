/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;
import se.esss.ics.iocfactory.ui.cdievents.ConfigCreated;
import se.esss.ics.iocfactory.ui.cdievents.ConfigSaved;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling addition of new configuration in the Configure screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class ConfigParamsManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(ConfigParamsManager.class.getName());

    private static final String ENV_MODULE_NAME = "environment";

    @Inject private transient ConfigurationService configService;
    @Inject private transient IOCConfigsCache configCache;

    @Inject private transient SecurityService securityService;
    
    @Inject private transient DetailsManager detailsManager;
    @Inject private transient IocsManager iocsManager;

    @Inject
    @IOCUpdated
    private transient Event<IOC> iocUpdated;

    @Inject
    @ConfigCreated
    private transient Event<Configuration> configCreated;

    @Inject
    @ConfigSaved
    private transient Event<Configuration> configSaved;

    private Configuration config = new Configuration();
    private List<Long> epicsVersions = Collections.emptyList();

    private List<String> envVersions = Collections.emptyList();

    public void prepareForAdd(IOC ioc) {
        loadEpicsVersions();

        config = new Configuration();

        config.setRevision( configService.getNextConfigurationForIOC(ioc.getName()) );
        config.setUser(securityService.getUsername());
        config.setIoc(ioc);
        config.setOs(ioc.getOs());
        config.setLastEdit(new Date());
        config.setComment("");
        config.setProcServPort(Configuration.DEFAULT_PROC_SERV_PORT);

        if (!epicsVersions.isEmpty()) {
            config.setEpicsVersion(epicsVersions.get(epicsVersions.size()-1));
        }

        updateEnvVersions();
    }

    public void prepareForEdit(Configuration conf) {
        loadEpicsVersions();

        config = conf;

        updateEnvVersions();
    }

    /**
     * Initializes the epicsVersions property. Gets a sorted list of EPICS versions that have at least one environment
     * module for the given OS (as defined by the selected IOC).
     */
    private void loadEpicsVersions() {
        final String iocOs = iocsManager.getSelectedIOC().getOs();
        final Repository repository = Util.getRepository();
        epicsVersions = repository.getEpicsVersions().stream().filter(epicsVersion ->
            !repository.getModulesForSpec(ENV_MODULE_NAME, iocOs, epicsVersion).isEmpty()
        ).collect(Collectors.toList());
        epicsVersions.sort((l,r) -> l.compareTo(r));
    }

    public void updateEnvVersions() {
        final List<Module> envModuleVersions = Util.getRepository().getModulesForSpec(ENV_MODULE_NAME,
                config.getOs(), config.getEpicsVersion());
        
        envVersions = envModuleVersions.stream().map(Module::getModuleVersion).
                sorted((l,r) -> ModuleVersionsComparator.comparePrioritizeNumbered(l, r)).
                collect(Collectors.toList());

        if (envVersions.isEmpty()) {
            config.setEnvVersion(null);
        } else if (config.getEnvVersion() == null) {
            config.setEnvVersion(envVersions.get(envVersions.size() - 1));
        } else {
            // Validate that the current selected version is valid for the current list
            if (envVersions.indexOf(config.getEnvVersion()) == -1) {
                config.setEnvVersion(null);
            }
        }
    }

    public void addConfiguration() {
        Util.callSecuredEJB(LOG, () -> {
            configService.saveNewConfiguration(config);
        }).ifSuccessful(() -> {
            iocUpdated.fire(config.getIoc());
            configCreated.fire(config);
        });
    }

    public void editConfiguration() {
        Util.callSecuredEJB(LOG, () -> {
            configService.updateConfigurationSecured(config);
        }).ifSuccessful(() -> {
            configSaved.fire(config);
            detailsManager.reloadConfiguration();
        });
    }

    public Configuration getConfig() { return config; }
    public List<Long> getEpicsVersions() { return epicsVersions; }
    public List<String> getEnvVersions() { return envVersions; }
}
