/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.	
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.LazyDataModel;
import se.esss.ics.iocfactory.model.log.LogEntry;
import se.esss.ics.iocfactory.model.log.LogEntryType;
import se.esss.ics.iocfactory.model.log.LogLazyDataModel;
import se.esss.ics.iocfactory.service.YamlHelper;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * 
 */
@Named
@ViewScoped
public class LogManager implements Serializable {

    private static final long serialVersionUID = -2756088935648151119L;
    private static final YamlHelper YAML_HELPER = new YamlHelper(true);
    private static final Logger LOG = Logger.getLogger(LogManager.class.getName());

    @Inject
    private transient LogLazyDataModel lazyEntries;

    private Map<String, Object> logEntryTypesList = new HashMap<String, Object>();
    private LogEntry activeEntry;
    private String formattedEntry;

    @PostConstruct
    public void init() {
        // populate DDL entry type with enum values
        for (LogEntryType le : LogEntryType.values()) {
            logEntryTypesList.put(le.toString(), le.name());
        }
    }

    public void updateDetails() {
        if (activeEntry == null) {
            formattedEntry = null;
        } else {
            formattedEntry =  formatYaml(activeEntry.getDetails());
        }
    }

    private String formatYaml(String text) {
        try {
            final ObjectMapper mapper = YAML_HELPER.getMapper();
            final Object yaml = mapper.readValue(text, Object.class);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(yaml);
        } catch (IOException e) {
            throw new RuntimeException("IO Exception during yaml pretty print", e);
        }
    }
    
    public String getFormatedEntry(String text)
    {
        return formatYaml(text);
    }
    
    public String getFormattedEntry() {
        return formattedEntry;
    }

    public LazyDataModel<LogEntry> getLazyEntries() {
        return lazyEntries;
    }

    public LogEntry getActiveEntry() {
        return activeEntry;
    }

    public void setActiveEntry(LogEntry activeEntry) {
        this.activeEntry = activeEntry;
    }

    public Map<String, Object> getLogEntryTypesList() {
        return logEntryTypesList;
    }
}
