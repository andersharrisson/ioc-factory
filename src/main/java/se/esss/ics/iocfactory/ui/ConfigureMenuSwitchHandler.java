/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 * 
 * This file is part of IOC Factory.
 * 
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import javax.enterprise.event.Observes;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import se.esss.ics.iocfactory.ui.cdievents.MenuSwitched;

/**
 *
 * @author mpavleski
 */
@ViewScoped
@Named
public class ConfigureMenuSwitchHandler implements Serializable {
    @Inject
    private DetailsManager detailsManager; 
    
    @Inject
    private MenuManager menuManager;
    
    public void handleMenuSwitched(@Observes @MenuSwitched final String viewId) {
        if (detailsManager.isDirty()) {
            // If it is to the same page, cancel navigation silently, if it is to other, show dialog
            menuManager.cancelNavigation();
            if (!"/configure.xhtml".equals(viewId)) {
                RequestContext.getCurrentInstance().execute("handleMenuSwitch();");
            }
        }
     }
    
    public void saveConfiguration() {
        detailsManager.saveConfiguration(false);
        menuManager.continueNavigation();
    }

    public void discardConfiguration() {
        detailsManager.discardConfiguration(false);
        menuManager.continueNavigation();
    }
}
