/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import javax.inject.Named;
import se.esss.ics.iocfactory.model.MacroExpansion;
import se.esss.ics.iocfactory.model.Parameter;

/**
 * A class providing a helper method that gets only the first of the expanded values for a {@link Parameter} as a
 * {@link String}. Used in parameter expanded values display and filtering functionality.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
public class ExpansionHelper {
    private ExpansionHelper() {}

    /**
     * Returns an String expanded value of the first expansion value for a parameter. See {@link ExpansionHelper}
     * @param param
     * @return
     */
    public static String getSingleLineExpVal(Parameter param) {
        if (param != null && !param.getExpandedValues().isEmpty()) {
            final MacroExpansion firstExpanded = param.getExpandedValues().get(0);
            return firstExpanded.getExpandedValue();
        } else {
            return "";
        }
    }
}
