/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.TreeNode;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryStats;
import se.esss.ics.iocfactory.service.RepositoryDump;
import se.esss.ics.iocfactory.service.RepositoryProducer;
import se.esss.ics.iocfactory.service.SetupService;

/**
 * Handles statistics about the repository such as last scan time, hash, structure & messages
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@SessionScoped
public class RepoStatsManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(RepoStatsManager.class.getName());

    private static final String ERROR_CSS = "errorHighlight";
    private static final String WARN_CSS = "warnHighlight";
    private static final String HIGHLIGHT_CSS = "highlightHighlight";

    @Inject private transient SetupService setupService;
    @Inject private RepositoryProducer repoProducer;

    private final List<SelectItem> consistencyMsgLevels;

    private final Map<EEEType, RepositoryStats> repoStats = new EnumMap<>(EEEType.class);

    public RepoStatsManager() {
        consistencyMsgLevels = new ArrayList<>();
        consistencyMsgLevels.add(new SelectItem("", "Select one", "", false, false, true));
        consistencyMsgLevels.add(new SelectItem(ConsistencyMsgLevel.INFO));
        consistencyMsgLevels.add(new SelectItem(ConsistencyMsgLevel.WARN));
        consistencyMsgLevels.add(new SelectItem(ConsistencyMsgLevel.ERROR));

        repoStats.put(EEEType.PRODUCTION, new RepositoryStats());
        repoStats.put(EEEType.SANDBOX, new RepositoryStats());
    }

    public Repository getRepository(final EEEType type) { return repoProducer.getSpecificRepo(type); }

    public RepositoryStats getRepoStats(final EEEType type) {
        handleRepoReload(type);
        return repoStats.get(type);
    }

    public List<SelectItem> getConsistencyMsgLevels() { return consistencyMsgLevels; }

    /**
     * Retrieves a HTML CSS style information for a given {@link ConsistencyStatusValue}
     *
     * @param msg
     * @return
     */
    public String styleForMessage(ConsistencyStatusValue msg) {
        switch (msg.getLevel()) {
            case WARN:
                return WARN_CSS;
            case ERROR:
                return ERROR_CSS;
            case INFO:
                break;
            default:
                throw new IllegalArgumentException(String.format("Unknown Repository message level %s",
                        msg.getLevel().toString()));
        }

        return "";
    }

    public void initStructureView(final EEEType type) {
        final RepositoryStats stats = repoStats.get(type);
        stats.setSelectedNode(null);
        toggleAll(stats.getRepoTree(), false);
    }

    public void expandTreeNode(final EEEType type) {
        final RepositoryStats stats = repoStats.get(type);
        toggleAll(stats.getSelectedNode() != null ? stats.getSelectedNode() : stats.getRepoTree(), true);
    }

    public void colapseTreeNode(final EEEType type) {
        final RepositoryStats stats = repoStats.get(type);
        toggleAll(stats.getSelectedNode() != null ? stats.getSelectedNode() : stats.getRepoTree(), false);
    }

    private static void toggleAll(TreeNode node, boolean expanded) {
        if (node != null) {
            node.setExpanded(expanded);
            node.getChildren().forEach(child -> toggleAll(child, expanded));
        }
    }

    private void handleRepoReload(final EEEType type) {
        final RepositoryStats stats = repoStats.get(type);
        final Repository repo = getRepository(type);

        if (stats.getRepoHash() == null || !Objects.equals(stats.getLastRepoScanTime(), repo.getLastScanTime())) {
            RepositoryDump.dumpRepositoryStats(repo, stats);
        }
    }
}
