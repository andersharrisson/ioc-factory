/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.primefaces.context.RequestContext;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.ui.cdievents.ConfigsSelected;
import se.esss.ics.iocfactory.ui.cdievents.EeeSwitched;
import se.esss.ics.iocfactory.ui.cdiqualifiers.ConfigDirty;
import se.esss.ics.iocfactory.util.Util;

/**
 * This class handles the {@link EeeSwitched} events for the Configure screen
 *
 * CDI Instance dynamic injection is used in order not to trigger creation of dependency beans if we are not
 * on the configure screen.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ViewScoped
@Named
public class ConfigureEEESwitchHandler implements Serializable {
    @Inject
    private transient Instance<IocsManager> iocsManager;

    @Inject
    private transient Instance<ConfigurationsManager> configsManager;

    @Inject
    private transient Instance<IOCConfigsCache> iocConfigCache;

    @Inject
    private transient Instance<DetailsManager> detailsManager;

    @Inject
    @ConfigDirty
    private transient Instance<MutableBoolean> configDirty;

    private List<Configuration> selectedConfigs = Collections.emptyList();

    public void handleEEESwitch(@Observes @EeeSwitched final EEEType type) {
        if (Util.getViewId().endsWith("configure.xhtml")) {
            handleConfigureScreenSwitch();
        }
    }

    public void handleSelectedConfigs(@Observes @ConfigsSelected List<Configuration> selectedConfigs) {
        this.selectedConfigs = selectedConfigs;
    }

    public void saveConfiguration() {
        detailsManager.get().saveConfiguration(false);
        reloadIocsConfigs();
        detailsManager.get().reloadConfiguration();
    }

    public void discardConfiguration() {
        detailsManager.get().discardConfiguration(false);
        reloadIocsConfigs();
        detailsManager.get().reloadConfiguration();
    }

    private void handleConfigureScreenSwitch() {
        if (isSingleConfigSelected()) {
            if (!configDirty.get().booleanValue()) {
                reloadIocsConfigs();
                detailsManager.get().reloadConfiguration();
                requestConfigureComponentUpdates();
            } else {
                RequestContext.getCurrentInstance().execute("handleEEESwitch();");
            }
        } else {
            reloadIocsConfigs();
            requestConfigureComponentUpdates();
        }
    }

    private void reloadIocsConfigs() {
        iocConfigCache.get().clear();
        iocsManager.get().reload();
        configsManager.get().reload();
    }

    private void requestConfigureComponentUpdates() {
        final RequestContext reqCtx = RequestContext.getCurrentInstance();
        reqCtx.update(Arrays.asList(
                "iocsForm:iocs", "configsForm:configs", "globalsForm:globals",
                "devicesForm:devices", "paramsForm:params"));
        reqCtx.execute("refreshAll();");
    }

    private boolean isSingleConfigSelected() {
        return selectedConfigs != null && selectedConfigs.size() == 1;
    }

}
