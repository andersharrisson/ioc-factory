/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import se.esss.ics.iocfactory.util.Util;


/**
 * UI class (JSF backing bean) to handle url links to CCDB and Naming System
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class LinkManager implements Serializable {
    private static final String CCDB_URL_PROPERTY = "iocfactory.ccdb.link.url";
    private static final String CCDB_URL_DEFAULT = "http://localhost:8080/ccdb/";

    private static final String NS_URL_PROPERTY = "iocfactory.ns.link.url";
    private static final String NS_URL_DEFAULT = "http://localhost:8080/names/";

    /** The name of the device for which to retrieve the links*/
    private String slot;

    public String getSlot() { return slot; }
    public void setSlot(String slot) { this.slot = slot; }

    /**
     * Get a CCDB link to the {@link LinkManager#slot}
     * @return
     */
    public String getCCDBLink() {
        return String.format("%s?name=%s",
                Util.ensureTrailingSlash(System.getProperty(CCDB_URL_PROPERTY, CCDB_URL_DEFAULT)),
                Util.urlEncode(slot));
    }

    /**
     * Get a Naming System link to the {@link LinkManager#slot}
     * @return
     */
    public String getNamingLink() {
        return String.format("%sdevices.xhtml?i=2&deviceName=%s",
                Util.ensureTrailingSlash(System.getProperty(NS_URL_PROPERTY, NS_URL_DEFAULT)),
                Util.urlEncode(slot));
    }
}
