/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui.converters;

import java.util.EnumSet;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;

import static se.esss.ics.iocfactory.model.PlaceholderTrait.*;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@FacesConverter("globalHintConverter")
public class GlobalHintConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        final String[] items = value.split("\\#\\#\\#");
        if (items.length != 3) {
            return new ParameterPlaceholder(value, ParameterType.STRING, "", EnumSet.of(GLOBAL_HINT));
        } else {
            return new ParameterPlaceholder(items[0], ParameterType.valueOf(items[1]), items[2], 
                    EnumSet.of(GLOBAL_HINT));
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value==null) {
            return null;
        }

        final ParameterPlaceholder placeholder = (ParameterPlaceholder) value;
        return String.format("%s###%s###%s", placeholder.getName(), placeholder.getType(), placeholder.getDesc());
    }

}
