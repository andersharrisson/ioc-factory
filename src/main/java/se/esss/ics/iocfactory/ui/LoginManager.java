/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.util.Util;


/**
 * UI class (JSF backing bean) to handle the Login functionality
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class LoginManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(LoginManager.class.getName());

    private String username;
    private String password;

    @Inject
    private transient SecurityService securityService;

    @PostConstruct
    void init() {
        // Initializes username if already logged in
        // Makes sense in the case of the DummierSecurityService
        if (StringUtils.isEmpty(username) && securityService.isLoggedIn()) {
            username = securityService.getUsername();
        }
    }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public boolean isLoggedIn() { return securityService.isLoggedIn(); }

    /** Handles the login request */
    public void onLogin()
    {
        try {
            if (!securityService.doLogin(username, password)) {
                Util.addGlobalError("Login not Sucessful. Please try again.");
                username = "";
                password = "";
            } else {
                username = securityService.getUsername();
                Util.addGlobalInfo("Login Sucessful. Welcome " + username);
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "IOC Factory Login failed!", e);
            Util.addGlobalError("Internal error while logging in. Please report to the administrator.");
        }
    }

    /** Handles the logout request */
    public void onLogout()
    {
        try {
            securityService.doLogout();
            Util.addGlobalInfo("Logout Sucessful");
        } catch(Exception e) {
            LOG.log(Level.SEVERE, "IOC Factory Logout failed!", e);
            Util.addGlobalError("Internal error while logging off. Please report to the administrator.");
        }
    }
}
