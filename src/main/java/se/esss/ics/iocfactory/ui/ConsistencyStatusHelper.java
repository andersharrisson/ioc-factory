/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Stream;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatus;
import se.esss.ics.iocfactory.model.ConsistencyStatusState;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.HasConsistencyStatus;
import se.esss.ics.iocfactory.model.Parameter;

/**
 * A UI tool for formating and displaying {@link ConsistencyStatus} for various entities
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ViewScoped
@Named("csHelper")
public class ConsistencyStatusHelper
    implements Serializable {
    private static final String BLACK = "color: black;";
    private static final String GREEN = "color: darkgreen;";
    private static final String YELLOW = "color: #B8860B;";
    private static final String RED = "color: red;";

    private static final String ERROR_CSS = "errorHighlight";
    private static final String WARN_CSS = "warnHighlight";
    private static final String HIGHLIGHT_CSS = "highlightHighlight";

    private static final String UNKNOWN_ENUM_VALUE = "Unknown enum value for " + ConsistencyStatusState.class.getName();

    private final Map<HasConsistencyStatus, ConsistencyStatusState> statusCache =
            new IdentityHashMap<>();
    private final Map<ConsistencyStatus, ConsistencyStatusState> singleStatusCache =
            new IdentityHashMap<>();

    /**
     * Retrieves a rendered description, {@link ConsistencyStatusState} for a given entity that contains
     * {@link ConsistencyStatus} information - ie. implements {@link HasConsistencyStatus} interface.
     *
     * Entries are cached in {@link ViewScoped} scope for faster access.
     * @param entity
     * @return
     */
    public ConsistencyStatusState getStatusFor(HasConsistencyStatus entity) {
        if (entity==null) {
            return null;
        }

        final ConsistencyStatusState cached = statusCache.get(entity);
        if (cached == null) {
            final ConsistencyStatusState newState = createConsistencyStatusState(entity);
            statusCache.put(entity, newState);
            return newState;
        } else {
            return cached;
        }
    }

    public ConsistencyStatusState getStatusFor(ConsistencyStatus cs) {
        final ConsistencyStatusState cached = singleStatusCache.get(cs);
        if (cached == null) {
            final ConsistencyStatusState newState = createConsistencyStatusState(cs);
            singleStatusCache.put(cs, newState);
            return newState;
        } else {
            return cached;
        }
    }

    /**
     * Removes an entry from the {@link ConsistencyStatusState} cache (invalidates the cache for the entry)
     *
     * @param entity
     */
    public void removeCacheEntry(HasConsistencyStatus entity) {
        statusCache.remove(entity);
    }

    /** Shortcut for invoking
     * {@link ConsistencyStatusHelper#getStatusFor(se.esss.ics.iocfactory.model.HasConsistencyStatus)} and
     * {@link ConsistencyStatusState#getRenderedDesc()}
     * @param entity
     * @return
     */
    public String getRenderedDesc(HasConsistencyStatus entity) {
        return getStatusFor(entity) != null ? getStatusFor(entity).getRenderedDesc() : "";
    }

    /** Shortcut for invoking
     * {@link ConsistencyStatusHelper#getStatusFor(se.esss.ics.iocfactory.model.HasConsistencyStatus)} and
     * {@link ConsistencyStatusState#getSingleLineDesc()}
     * @param entity
     * @return
     */
    public String getSingleLineDesc(HasConsistencyStatus entity) {
        return getStatusFor(entity) != null ? getStatusFor(entity).getSingleLineDesc() : "";
    }

    /** Checks whether an entity has {@link ConsistencyMsgLevel#ERROR} or {@link ConsistencyMsgLevel#WARN} level entries
     * Returns true if doesn't. False otherwise.
     * @param entity
     * @return
     */
    public boolean isStatusOk(HasConsistencyStatus entity) {
        return !entity.getConsistencyStatus().stream().anyMatch(cs -> cs.getLevel()==ConsistencyMsgLevel.ERROR ||
                cs.getLevel()==ConsistencyMsgLevel.WARN);
    }

    private static Stream<ConsistencyStatus> getMergedConsistencyStatusAndMacroErrStream(
            HasConsistencyStatus entity) {
        if (entity instanceof Parameter) {
            final Parameter param = (Parameter) entity;
            return Stream.concat(
                    entity.getConsistencyStatus().stream(),
                    param.getExpandedValues().stream().
                            filter(expValue -> expValue.isError()).
                            map(expValue -> new ConsistencyStatusValue(ConsistencyMsgLevel.WARN, expValue.getError()))
                    );
        } else {
            return entity.getConsistencyStatus().stream();
        }
    }

    private static ConsistencyStatusState createConsistencyStatusState(HasConsistencyStatus
            entity) {
        final ConsistencyStatusState css = new ConsistencyStatusState();

        final StringBuilder formatedBuilder = new StringBuilder();
        final StringJoiner singleLineJoiner = new StringJoiner(";  ");
        final MutableBoolean first = new MutableBoolean(true);
        getMergedConsistencyStatusAndMacroErrStream(entity).sorted(
                (l,r) -> ObjectUtils.compare(r.getLevel(), l.getLevel())).
                forEach(cs ->
        {
            if (first.isTrue()) {
                first.setValue(false);
                css.setRowStyle(resolveRowStyle(cs));
            }

            singleLineJoiner.add(cs.getDesc());
            addFormatedDesc(cs, formatedBuilder);

        });
        final String builtMsg = singleLineJoiner.toString();
        final String builtFormatedMsg = formatedBuilder.toString();

        css.setSingleLineDesc(StringUtils.isEmpty(builtMsg) ? "Clean" : builtMsg);
        css.setRenderedDesc(StringUtils.isEmpty(builtFormatedMsg) ? "Clean" : builtFormatedMsg);

        return css;
    }

    private static ConsistencyStatusState createConsistencyStatusState(ConsistencyStatus cs) {
        final ConsistencyStatusState css = new ConsistencyStatusState();
        css.setRenderedDesc(String.format("<span style='%s'>%s</span>", resolveColor(cs), cs.getDesc()));
        css.setSingleLineDesc(cs.getDesc());
        css.setRowStyle(resolveRowStyle(cs));
        return css;
    }

    private static void addFormatedDesc(ConsistencyStatus cs, StringBuilder builder) {
        builder.append("<span style='").
        append(resolveColor(cs)).
        append("'>").
        append(cs.getDesc()).
        append("</span><br />");
    }

    private static String resolveRowStyle(ConsistencyStatus cs) {
        // Because it is sorted stream, worse levels will overwrite lower error levels
        switch (cs.getLevel()) {
            case WARN:
                return WARN_CSS;
            case ERROR:
                return ERROR_CSS;
            case HIGHLIGHT:
                return HIGHLIGHT_CSS;
            case INFO:
                return "";
            default:
                throw new UnsupportedOperationException(UNKNOWN_ENUM_VALUE);
        }
    }

    private static String resolveColor(ConsistencyStatus cs) {
        if (cs == null || cs.getLevel()==null) {
            return BLACK;
        }
        switch (cs.getLevel()) {
            case HIGHLIGHT:
                return GREEN;
            case INFO:
                return BLACK;
            case WARN:
                return YELLOW;
            case ERROR:
                return RED;
            default:
                throw new UnsupportedOperationException(UNKNOWN_ENUM_VALUE);
        }
    }
}
