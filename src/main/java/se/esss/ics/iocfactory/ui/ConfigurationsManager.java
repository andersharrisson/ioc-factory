/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.mutable.MutableBoolean;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;
import se.esss.ics.iocfactory.ui.cdievents.ConfigCreated;
import se.esss.ics.iocfactory.ui.cdievents.ConfigSaved;
import se.esss.ics.iocfactory.ui.cdievents.ConfigsSelected;
import se.esss.ics.iocfactory.ui.cdievents.IocsSelected;
import se.esss.ics.iocfactory.ui.cdiqualifiers.ConfigDirty;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling the Configurations on the Configure screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class ConfigurationsManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(ConfigurationsManager.class.getCanonicalName());

    @Inject private transient ConfigurationService configService;
    @Inject private transient SecurityService securityService;

    @Inject
    @ConfigDirty
    private transient MutableBoolean configDirty;

    @Inject private transient IOCConfigsCache configCache;

    @Inject
    @ConfigsSelected
    private transient Event<List<Configuration>> configsSelectEvent;

    @Inject
    @IOCUpdated
    private transient Event<IOC> iocUpdated;

    private boolean userCanEdit;

    private List<IOC> selectedIOCs = Collections.emptyList();

    private List<Configuration> configs = null;

    private List<Configuration> selectedConfigs = Collections.emptyList();
    private Long copiedConfigId = null;

    /**
     * Initializes the configure IOC backing bean.
     */
    @PostConstruct
    public void init() {
        userCanEdit = securityService.canConfigureIOCs();
    }

    public void handleIocsSelected(@Observes @IocsSelected List<IOC> selectedIOCs) {
        this.selectedIOCs = selectedIOCs;
        this.configs = null;
    }

    /**
     * Returns true if the user can edit configurations
     *
     * @return
     */
    public boolean userCanEdit() { return userCanEdit; }

    /**
     * Getter that returns a {@link Confiuration} if a single is selected
     * @return
     */
    public Configuration getSelectedConfig() {
        if (selectedConfigs!=null && selectedConfigs.size()==1) {
            return selectedConfigs.get(0);
        } else {
            return null;
        }
    }

    public List<Configuration> getSelectedConfigs() { return selectedConfigs; }

    /**
     * Setter used when configurations are selected in the multiple selection list view. Used to fire a
     * {@link ConfigsSelected} event for consumers.
     * The selection will be changed only if the current config is not dirty (being edited).
     * @param selectedConfigs
     */
    public void setSelectedConfigs(List<Configuration> selectedConfigs) {
        if (configDirty.isFalse()) {
            this.selectedConfigs = selectedConfigs;
            configsSelectEvent.fire(selectedConfigs);
        }
    }

    public Long getCopiedConfigId() { return copiedConfigId; }
    public void setCopiedConfigId(Long copiedConfigId) { this.copiedConfigId = copiedConfigId; }

    /**
     * Retrieves a list of configurations to show in the configurations list based on the selected IOCs.
     *
     * @return
     */
    public List<Configuration> getConfigurations() {
        if (configs == null) {
            configs = selectedIOCs == null ? Collections.emptyList() :
                    selectedIOCs.stream().
                    filter(ioc -> !ioc.getConsistencyStatus().isError()).
                    flatMap(ioc -> configCache.getConfigurationsForIOC(ioc).stream()).
                    collect(Collectors.toList());
        }
        return configs;
    }

    /**
     * Reloads keeping selection
     */
    public void reload() {
        final List<Configuration> prevSelected = selectedConfigs;
        configs = null;
        getConfigurations();
        selectedConfigs = configs.stream().filter(conf -> prevSelected.contains(conf)).collect(Collectors.toList());
        configsSelectEvent.fire(selectedConfigs);
    }

    /** Handles the "Delete" Configuration operation */
    public void deleteConfiguration() {
        if (selectedConfigs==null || selectedConfigs.isEmpty()) {
            Util.addGlobalInfo("No configuration selected.");
            return;
        }

        if (configService.deleteConfigurations(selectedConfigs)) {
            clearCacheForConfigs(selectedConfigs);
            this.configs = null;
            this.selectedConfigs.clear();
            Util.addGlobalInfo("Configuration deleted.");
        } else {
            Util.addGlobalError("Commited configuration selected. No configurations deleted.");
        }
    }

    /**
     * Clears the cache for the IOCs configs in the {@link IOCConfigsCache} when a config for the IOC is saved.
     * Invoked when a {@link ConfigSaved} event is generated.
     * @param config
     */
    public void handleConfigSaved(@Observes @ConfigSaved Configuration config) {
        clearCacheForConfigs(ImmutableList.of(config));
        this.configs = null;
    }

    public void handleConfigCreated(@Observes @ConfigCreated Configuration config) {
        this.configs = null;
        getConfigurations();
        if (configs != null && configs.size()>=1) {
            setSelectedConfigs(ImmutableList.of(configs.get(0)));
        }
    }

    /** Handles a change in the selection of the IOCs table. */
    public void handleIocSwitch() {
        if (configDirty.isTrue()) {
            return;
        }

        this.configs = null;

        if (selectedConfigs != null) {
            selectedConfigs = ImmutableList.of();
        }
        configsSelectEvent.fire(selectedConfigs);
    }

    /** Handles the "Copy" configuration operation */
    public void copySelectedConfig() {
        if (selectedConfigs == null || selectedConfigs.size() != 1) {
            Util.addGlobalWarn("Only single configuration can be copied.");
            return;
        }

        final Configuration copiedConfig = selectedConfigs.get(0);
        copiedConfigId = copiedConfig.getId();
        Util.addGlobalInfo(String.format("Configuration \"%s\", revision %d from IOC %s successfully copied.",
                copiedConfig.getComment(), copiedConfig.getRevision(), copiedConfig.getIoc().getName()));
    }

    /** Handles the "Paste" configuration operation */
    public void pasteConfig() {
        final List<String> failedIOCNames = new ArrayList();

        if (copiedConfigId == null) {
            Util.addGlobalWarn("No configuration copied yet");
            return;
        } else if (selectedIOCs == null || selectedIOCs.isEmpty()) {
            Util.addGlobalWarn("Please select destination IOCs");
            return;
        }

        final String username = securityService.getUsername();
        MutableBoolean ccdbClientError = new MutableBoolean(false);
        selectedIOCs.forEach(ioc -> {
            try {
                configService.copyConfiguration(copiedConfigId, ioc.getName(), username);

                this.configs = null;
                
                iocUpdated.fire(ioc);
            } catch (ConfigClientException ex) {
                LOG.log(Level.SEVERE, null, ex);
                ccdbClientError.setTrue();
                failedIOCNames.add(ioc.getName());
            } catch (Exception e) {
                LOG.log(Level.SEVERE, null, e);
                failedIOCNames.add(ioc.getName());
            }
        });

        if (failedIOCNames.isEmpty()) {
            Util.addGlobalInfo("Paste successful.");
        } else {
            Util.addGlobalError(String.format("Pasting failed for the following IOCs: %s%n%s",
                    failedIOCNames.stream().collect(Collectors.joining(", ")),
                    ccdbClientError.isTrue() ? ConfigClientException.ERR_MSG  :
                            "System error, please contact the administrator."));
        }
    }

    /**
     * Clears the {@link IOCConfigsCache} for the given configurations
     * @param configs
     */
    private void clearCacheForConfigs(List<Configuration> configs) {
        // clear cache for iocs
        configs.stream().map(Configuration::getIoc).distinct().
                forEach(ioc -> iocUpdated.fire(ioc));
    }
}
