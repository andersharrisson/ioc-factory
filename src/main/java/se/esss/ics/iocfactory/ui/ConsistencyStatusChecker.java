/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.service.IOCConfigsCache;

/**
 * Checks generate entry consistency against configuration cache. (Hash value based)
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */

@Named("csChecker")
public class ConsistencyStatusChecker implements Serializable {

    private static final long serialVersionUID = -2929763873011609317L;

    @Inject
    private transient IOCConfigsCache configCache;

    /**
     * Checks whether the configuration in the entry differs from the configuration saved, or if it was deleted The checks are done against the
     * {@link IOCConfigsCache}.
     *
     * @param entry
     * @return
     */
    public void handleConfigDiffersOrDeleted(GenerateEntry entry) {
        if (entry == null) {
            return;
        }

        // config deleted
        if (configDeleted(entry)) {
            entry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.WARN, "Configuration was deleted"));
            return;
        }

        // config differs
        if (configDiffers(entry)) {
            entry.getConsistencyStatus().add(new ConsistencyStatusValue(ConsistencyMsgLevel.WARN, "Configurations differ"));
            return;
        }
    }

    /**
     * Checks whether the configuration in the entry differs from the configuration saved. The checks are done against the {@link IOCConfigsCache}.
     *
     * @param entry
     * @return
     */
    public boolean configDiffers(GenerateEntry entry) {
        if (entry == null) {
            return false;
        }

        final Configuration config = configCache.getConfigurationById(entry.getConfigId());
        if (config == null) {
            // config does not exist anymore
            return true;
        }
        return !StringUtils.equals(config.getHash(), entry.getGeneratedHash());
    }

    /**
     * Checks whether the configuration that was used to generate this IOC was deleted from the database.
     *
     * @param entry
     * @return
     */
    public boolean configDeleted(GenerateEntry entry) {
        if (entry == null) {
            return false;
        }
        final Configuration config = configCache.getConfigurationById(entry.getConfigId());
        return config == null;
    }
}
