/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.GenerateEntryLazyDataModel;
import se.esss.ics.iocfactory.service.GenerateEntryConsistencyStatus;

/**
 * A JSF backing bean for handling the Browse IOC screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class BrowseManager implements Serializable {

    private static final long serialVersionUID = -1915130885658651253L;
    private static final Logger LOG = Logger.getLogger(BrowseManager.class.getName());

    @Inject
    private transient GenerateEntryLazyDataModel geLazyEntires;

    private List<GenerateEntry> selectedEntries = Collections.emptyList();
    private Map<String, Object> consistStatusList = new HashMap<String, Object>();

    /**
     * Initializes the BrowseManager IOC bean
     */
    @PostConstruct
    public void init() {
        // populate DDL consistency status
        for (GenerateEntryConsistencyStatus le : GenerateEntryConsistencyStatus.values()) {
            consistStatusList.put(le.toString(), le.name());
        }
    }

    public List<GenerateEntry> getSelectedEntries() {
        return selectedEntries;
    }

    public void setSelectedEntries(List<GenerateEntry> selectedEntries) {
        this.selectedEntries = selectedEntries;
    }

    public GenerateEntryLazyDataModel getGeLazyEntires() {
        return geLazyEntires;
    }

    public Map<String, Object> getConsistStatusList() {
        return consistStatusList;
    }

}
