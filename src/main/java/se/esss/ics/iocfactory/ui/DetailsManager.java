/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import se.esss.ics.iocfactory.model.*;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.MacroResolver;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.ui.cdievents.ConfigSaved;
import se.esss.ics.iocfactory.ui.cdievents.ConfigsSelected;
import se.esss.ics.iocfactory.ui.cdiqualifiers.ConfigDirty;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling the configuration details (globals, devices, macros) on the Configure screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class DetailsManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(DetailsManager.class.getName());

    private static final String NO_CONFIGURATION_SELECTED = "No configuration selected.";

    @Inject private transient ConfigurationService configService;
    @Inject private transient ConsistencyStatusHelper csHelper;
    @Inject private transient SecurityService securityService;

    @Inject @ConfigSaved private transient Event<Configuration> configSavedEvent;

    /** This configuration is selected in the ConfigurationsManager and updated by the selectedConfigObserver method */
    private Configuration selectedConfig;
    private Configuration config;

    private final MutableBoolean dirty = new MutableBoolean(false);

    private List<DeviceConfiguration> selectedDevices = Collections.emptyList();

    private List<Parameter> parameters = Collections.emptyList();
    private List<Parameter> selectedParameters = Collections.emptyList();

    private List<Parameter> selectedGlobals = Collections.emptyList();

    private boolean canDeleteParameters = false;
    private boolean canDeleteDevices = false;

    private boolean userCanEdit;

    private final MacroResolver macroResolver = new MacroResolver();

    /** Stateful device name used for the edit dialog box */
    private DeviceConfiguration lastDevice;

    private String deviceModuleVersion;


    private Parameter lastParameter = null;
    private final transient ParameterValueAccessor paramValueAccessor = new ParameterValueAccessor();

    /** An utility class used to access as a bean property the value of the {@link DetailsManager#lastParameter} */
    public class ParameterValueAccessor {
        public String getParamValue() {
            return lastParameter != null ? lastParameter.getValue() : null;
        }

        public void setParamValue(String value) {
            if (lastParameter != null) {
                lastParameter.setValue(value);
            }
        }
    }

    /**
     * This can be used to initialize the structure for other means than the Configure screen, for example in the
     * Generate screen "Details" dialog.
     * For the Configure screen the bean is initialized through the selectedConfigObserrver
     *
     * @param config
     */
    public void initWithConfiguration(Configuration config) {
        selectedConfigsObserver(ImmutableList.of(config));
    }

    public String getDeviceModuleVersion() { return deviceModuleVersion; }
    public void setDeviceModuleVersion(String deviceModuleVersion) { this.deviceModuleVersion = deviceModuleVersion; }

    /**
     * This observer is triggered by changes in the selected configurations in the ConfigurationsManager
     *
     * @param configs
     */
    public void selectedConfigsObserver(@Observes @ConfigsSelected List<Configuration> configs) {
        selectedConfig = configs != null && configs.size() == 1 ? configs.get(0) : null;
        reloadConfiguration();
    }

    /** Reloads the currently loaded configuration. */
    public void reloadConfiguration() {
        userCanEdit = securityService.canConfigureIOCs();

        if (selectedConfig == null || selectedConfig.getConsistencyStatus().isError()) {
            config = null;
            clearDetails();
            return;
        }

        // Initialize configuration for editing
        try {
            config = configService.loadConfiguration(selectedConfig.getId());

            renumerateAndPrepareConfig();
        } catch(ConfigClientException ce) {
            LOG.log(Level.SEVERE, "Failed accessing CCDB in reloadConfiguration", ce);
            Util.addGlobalError(ce.getMessage());
            config = null;
        }

        clearDetails();
    }

    public Configuration getConfig() { return config; }

    public boolean isConfigEditable() {
       return config != null && !config.isCommited() && userCanEdit;
    }

    /** Moves device "UP" or "DOWN" according to the parameter dir(ection)
     * @param dir
     */
    public void moveDevice(final String dir) {
        if (config == null) {
            return;
        }
        LOG.info("moveDevice -> config.getDevices()");
        final List<DeviceConfiguration> devices = config.getDevices();

        final DeviceConfiguration device = getSelectedDevice();

        int pos = devices.indexOf(device);
        if (pos>0 && "UP".equals(dir)) {
            DeviceConfiguration other = devices.get(pos-1);
            devices.set(pos-1, device);
            devices.set(pos, other);
            makeConfigDirty();

            renumerateAndPrepareConfig();
        } else if (pos>=0 && pos<devices.size()-1 && "DOWN".equals(dir)) {
            DeviceConfiguration other = devices.get(pos+1);
            devices.set(pos+1, device);
            devices.set(pos, other);
            makeConfigDirty();

            renumerateAndPrepareConfig();
        }
    }

    public List<DeviceConfiguration> getSelectedDevices() { return selectedDevices; }

    /**
     * Setter invoked when user changes the selection of devices on the Configure screen. It updates the
     * list of displayed parameters and the flag that indicates whether the selected devices can be deleted (if all are
     * in ERROR status) - {@link DetailsManager#canDeleteDevices}
     * @param selectedDevices
     */
    public void setSelectedDevices(List<DeviceConfiguration> selectedDevices) {
        this.selectedDevices = selectedDevices;
        updateParamsList();
        updateCanDeleteDevices();
    }

    /** Getter that returns non-null if a single device is selected in the devices table
     * @return
     */
    public DeviceConfiguration getSelectedDevice() {
        return selectedDevices != null && selectedDevices.size() == 1 ? selectedDevices.get(0) : null;
    }

    public List<Parameter> getParameters() { return parameters; }

    public List<Parameter> getSelectedParameters() { return selectedParameters;}

    /**
     * Setter used when selecting parameters in the parameters table. Updates the flag that determines whether the
     * parameters can be deleted (all are in error) {@link DetailsManager#canDeleteParameters}
     * @param selectedParameters
     */
    public void setSelectedParameters(List<Parameter> selectedParameters) {
        this.selectedParameters = selectedParameters;
        updateCanDeleteParameters();
    }

    public List<Parameter> getSelectedGlobals() {
        return selectedGlobals;
    }

    public void setSelectedGlobals(List<Parameter> selectedGlobals) {
        this.selectedGlobals = selectedGlobals;
    }

    public Parameter getSelectedGlobal() {
        return selectedGlobals != null && selectedGlobals.size() == 1 ? selectedGlobals.get(0) : null;
    }

    public List<Module> getAvailableModuleVersions(Module module) {
        List<Module> result = Util.getRepository().getModulesForSpec(module.getName(), module.getOs(), 
                module.getEpicsVersion());
        result.sort((l, r) -> l.getModuleVersion().compareTo(r.getModuleVersion()));
        return result;
    }

    public void prepareForEdit() {
        lastDevice = selectedDevices.get(0);
        deviceModuleVersion = lastDevice.getSupportModule().getModuleVersion();
    }

    public DeviceConfiguration getDevice() { return lastDevice; }

    /** Getter for getting the embedded {@link ParameterValueAccessor}, it updates the
     * {@link DetailsManager#lastParameter} so that the accessor uses the right {@link Parameter}.
     *
     * Used when editing parameters / globals on the Configure screen.
     *
     * @param param
     * @return
     */
    public ParameterValueAccessor getParameterValueAccessor(Parameter param) {
        lastParameter = param;
        return paramValueAccessor;
    }

    public boolean canDeleteParameters() { return canDeleteParameters; }

    public boolean canDeleteDevices() { return canDeleteDevices; }

    public void saveConfiguration() { saveConfiguration(true); }
    /**
     * Handles the "Save" configuration button
     *
     * @param reload whether to reload the config after save, this is usually needed by default, only case it is not
     * needed is when changing EEE environments after the save, where an caller will do the reload after EEE is switched
     */
    public void saveConfiguration(boolean reload) {
        if (!config.isCommited()) {
            final List<ConsistencyStatusValue> validationResults = configService.validateConfiguration(config);
            if (!validationResults.isEmpty()) {
                Util.addGlobalError(
                        String.format("%s\n\nThe configuration WAS NOT saved.",
                                validationResults.stream().
                                        map(ConsistencyStatusValue::getDesc).
                                        collect(Collectors.joining("\n"))
                        )
                );
            } else {
                Util.callSecuredEJB(LOG, () -> {
                    configService.updateConfigurationSecured(config);
                }).ifSuccessful(() -> {
                    clearDirty();
                    // will force reload to prevent concurent edit exceptions
                    if (reload) {
                        reloadConfiguration();
                    }

                    Util.addGlobalInfo("Configuration saved.");
                });
            }
        } else {
            Util.addGlobalInfo("Configuration already commited. Save failed.");
        }

    }

    public void discardConfiguration() { discardConfiguration(true); }

    /**
     * Handles the "Discard" configuration button
     *
     * @param reload indicates whether to do a reload after config is discarded, usually needed except when EEE is
     * switched. In that case the caller will do the reload after EEE is switched
     */
    public void discardConfiguration(boolean reload) {
        config = null;
        clearDetails();

        // reload current
        reloadConfiguration();
        Util.addGlobalInfo("Discarded changes to the configuration.");
    }

    public static String formatModuleDependencies(Module module) {
        return Util.formatModuleDependenceies(module);
    }

    /**
     * A CDI producer method that returns a {@link MutableBoolean} reference signifying whether the current
     * configuration is dirty - edited but not saved.
     *
     * {@link MutableBoolean} is used because ordinary {@link Boolean} was causing CDI injection problems.
     * @return
     */
    @Produces @ConfigDirty @ViewScoped
    public MutableBoolean dirtyProducer() { return dirty; }

    public boolean isDirty() { return dirty.booleanValue(); }
    public void makeConfigDirty() { dirty.setTrue(); }
    public void clearDirty() { dirty.setFalse(); }

    /**
     * Handles the "Delete" parameters button
     */
    public void deleteParams() {
        LOG.info("deleteParams");

        if (config == null) {
            Util.addGlobalWarn(NO_CONFIGURATION_SELECTED);
        } else if (config.isCommited()) {
            Util.addGlobalWarn("Configuration is commited, removing parameters is not possible");
        } else if (selectedParameters == null || selectedParameters.isEmpty()) {
            Util.addGlobalWarn("No parameters selected for removal.");
        } else {
            long deleted = selectedParameters.stream().filter(param -> param.getConsistencyStatus().isError()).
                    map(paramToRemove -> {
                        // ToDo: use param position in matching instead of List.remove
                        paramToRemove.getDeviceConfiguration().getParameters().remove(paramToRemove);
                        deleteParam(paramToRemove);
                        return paramToRemove;
                    }).count();

            if (deleted>0) {
                makeConfigDirty();
                selectedParameters = Collections.emptyList();

                Util.addGlobalInfo(String.format("Deleted %d parameter %s with errors", deleted, deleted == 1 ?
                        "" : "s"));
            } else {
                Util.addGlobalInfo("None of the selected parameters has errors.");
                FacesContext.getCurrentInstance().validationFailed();
            }
        }
    }

    /**
     * Handles the "Delete" devices button
     */
    public void deleteDevices() {
        if (config == null) {
            Util.addGlobalWarn(NO_CONFIGURATION_SELECTED);
        } else if (config.isCommited()) {
            Util.addGlobalWarn("Configuration is commited, removing devices is not possible");
        } else if (selectedDevices == null || selectedDevices.isEmpty()) {
            Util.addGlobalWarn("No devices selected for removal.");
        } else {
            try {
                if (configService.deleteInvalidDevices(selectedDevices)) {
                    selectedDevices = Collections.emptyList();

                    makeConfigDirty();
                    renumerateAndPrepareConfig();

                    Util.addGlobalInfo("Succesfully deleted the invalid devices.");
                }
            } catch (ConfigClientException ex) {
                LOG.log(Level.SEVERE, "Failed while accessing the CCDB in deleteDevices", ex);
                Util.addGlobalError(ex.getMessage());
            }
        }
    }

    /**
     * Handles the "Delete" global macros button
     */
    public void deleteSelectedGlobals() {
        if (config == null) {
            Util.addGlobalWarn(NO_CONFIGURATION_SELECTED);
        } else if (config.isCommited()) {
            Util.addGlobalWarn("Cannot change a commited configuration");
        } else {
            config.getGlobals().removeAll(selectedGlobals);
            selectedGlobals = Collections.emptyList();
            makeConfigDirty();
        }
    }

    /** Handles the cell-edit event that is an event fired when a global macro is being edited
     *
     * @param envt
     */
    public void handleGlobalCellEdit(CellEditEvent envt) {
        handleParamCellEdit(selectedGlobals, envt);
        selectedGlobals = null;
    }

    static int updateNo = 0;

    /**
     * Handles the cell-edit event fired when a device macro parameter is being edited
     * @param envt
     */
    public void handleDeviceParamCellEdit(CellEditEvent envt) {
        handleParamCellEdit(selectedParameters, envt);
        //RequestContext.getCurrentInstance().execute(String.format("alert('update %d');", updateNo++));
        final RequestContext reqContext = RequestContext.getCurrentInstance();

        reqContext.addCallbackParam("paramsUpdate", constructParamsJson(parameters));
    }


    private String constructParamsJson(final List<Parameter> params) {
        final StringBuilder builder = new StringBuilder();

        builder.append("{");
        for (final Parameter p : params) {
            final String paramValue = p.getValue();
            final String expValue =  p.getExpandedValues() != null && !p.getExpandedValues().isEmpty() ?
                    p.getExpandedValues().get(0).getExpandedValue() : "";

            builder.append(String.format("\"%d\" : { "
                    + "\"value\" : \"%s\", "
                    + "\"expValue\" : \"%s\", "
                    + "\"csStatus\" : \"%s\" , "
                    + "\"csStatusRendered\" : \"%s\" , "
                    + "\"rowStyle\" : \"%s\" , "
                    + "\"expValueHtml\" : \"%s\" },",
                    p.getParamPosition(),
                    paramValue != null ? paramValue : "",
                    expValue != null ? expValue : "",
                    csHelper.getSingleLineDesc(p),
                    csHelper.getRenderedDesc(p),
                    csHelper.getStatusFor(p).getRowStyle(),
                    buildExpValueTable(p)));
        }
        // Cut last comma
        builder.setLength(builder.length() - 1);
        builder.append("}");
        final String result = builder.toString();
        return result;
    }

    private static String buildExpValueTable(Parameter p) {
        final StringBuilder builder = new StringBuilder();
        if (p.getExpandedValues() != null && !p.getExpandedValues().isEmpty()) {
            builder.append("<table><tbody>");
            for (final MacroExpansion ev : p.getExpandedValues()) {
                builder.append("<tr><td>");
                if (ev.getOccurrence() != null && !StringUtils.isEmpty(ev.getOccurrence().getDefValue().
                        getRawValue())) {
                    builder.append("<span class='ui-icon ui-icon-pencil'>");
                    builder.append(ev.getOccurrence().getDefValue().getRawValue());
                    builder.append("</span>");
                }
                builder.append("</td><td>");
                builder.append(ev.getExpandedValue());
                builder.append("</td></tr>");
            }
            builder.append("</tbody></table>");
        }
        return builder.toString();
    }



    /** Retrieves the sorted (older to newer) list of module versions for a device
     *
     * @param device
     * @return
     */
    public List<String> getModuleVersionsForDevice(final DeviceConfiguration device) {
        if (device == null || device.getSupportModule() == null) {
            return Collections.emptyList();
        }

        final Module module = device.getSupportModule();

        return Util.getRepository().getModulesForSpec(module.getName(), module.getOs(),
                module.getEpicsVersion()).stream().map(Module::getModuleVersion).collect(Collectors.toList());
    }


    public void handleDeviceEdit() {
        try {
            if (configService.changeDeviceModuleVersion(lastDevice, deviceModuleVersion)) {
                makeConfigDirty();

                // Renumerate params & devices
                renumerateAndPrepareConfig();
            } else {
                Util.addGlobalWarn("Cannot change module version for undefined module");
            }
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed accessing the CCDB in handleDeviceEdit", ex);
            Util.addGlobalError(ex.getMessage());
        }
    }

    public void handleGlobalChange(Parameter param) {
        makeConfigDirty();
        macroResolver.resolveMacrosAfter(param);
    }

    public boolean canEditDevice() {
        final DeviceConfiguration selectedDevice = getSelectedDevice();

        // Can edit the device if no error and editable or editable and error is invalid snippet
        return selectedDevice != null && isConfigEditable() &&
                ( (!selectedDevice.getConsistencyStatus().isError()) ||
                        (selectedDevice.getConsistencyStatus().isError() &&
                        selectedDevice.getConsistencyStatus().contains(DeviceConsistencyStatus.INVALID_SNIPPET) &&
                        selectedDevice.getConsistencyStatus().size() == 1)
                );
    }

    private void clearDetails() {
        parameters = Collections.emptyList();

        selectedParameters = Collections.emptyList();
        selectedDevices = Collections.emptyList();
        selectedGlobals = Collections.emptyList();

        clearDirty();

        canDeleteParameters = false;
    }

    private void handleParamCellEdit(List<Parameter> selected, CellEditEvent evnt) {
        String newValue = (String) evnt.getNewValue();
        String oldValue = (String) evnt.getOldValue();

        // If single selection check if value changed before proceeding
        if ((selected == null || selected.size()==1) && StringUtils.equals(newValue, oldValue)) {
            return;
        } else if (config.isCommited()) {
            return;
        }

        makeConfigDirty();

        final MutableObject<Parameter> firstParam = new MutableObject(lastParameter);
        if (selected != null && selected.size()>1) {
            // Copy this param's value
            selected.stream().
                    filter(selectedParam -> lastParameter != selectedParam).
                    forEach(selParam -> {
                        // Update firstParam to get the parameter with the smallest position
                        if (selParam.getParamPosition() < firstParam.getValue().getParamPosition()
                                && !selParam.getConsistencyStatus().isError()) {
                            firstParam.setValue(selParam);
                        }
                        selParam.setValue(newValue);
                    });
        }

        macroResolver.resolveMacrosAfter(firstParam.getValue(), p -> csHelper.removeCacheEntry(p));
    }

    private void updateParamsList() {
        parameters = getSelectedDevices() != null ?
                selectedDevices.stream().flatMap(dev -> dev.getParameters().stream()).collect(Collectors.toList())
                        : Collections.emptyList();
    }

    private void updateCanDeleteParameters() {
        canDeleteParameters = isConfigEditable() && selectedParameters != null && !selectedParameters.isEmpty() &&
                selectedParameters.stream().allMatch(param -> param.getConsistencyStatus().isError());
    }

    private void updateCanDeleteDevices() {
        canDeleteDevices = isConfigEditable() && selectedDevices != null && !selectedDevices.isEmpty() &&
                selectedDevices.stream().allMatch(dev -> dev.getConsistencyStatus().isError());
    }

    private void deleteParam(Parameter param) {
        final Iterator<Parameter> iter = parameters.iterator();
        while (iter.hasNext()) {
            final Parameter other = iter.next();

            if (Objects.equals(param.getDeviceConfiguration().getName(), other.getDeviceConfiguration().getName()) &&
                    Objects.equals(param, other)) {
                iter.remove();
            }
        }
    }

    void renumerateAndPrepareConfig() {
        ConfigurationUtil.renumerateConfigEntities(config);

        // Reload the macro resolver
        macroResolver.setConfiguration(config);
        macroResolver.resolveMacros();
    }

    /**
     * Returns {@code true} if a device is found with a support module that has different module version than the
     * deviceModuleVersion parameter
     *
     * @param checkDev the device for which we check (other than this will be checked)
     * @param deviceModuleVersion
     * @return
     */
    private Optional<DeviceConfiguration> findConflictingDevice(DeviceConfiguration checkDev, Long deviceModuleVersion) {
        final Module deviceModule = checkDev.getSupportModule();
        Optional<DeviceConfiguration> offending = config.getDevices().stream().
                filter(dev -> {
                    final Module otherModule = dev.getSupportModule();
                    return dev != checkDev && otherModule != null &&
                            Objects.equals(otherModule.getName(), deviceModule.getName()) &&
                            !Objects.equals(otherModule.getModuleVersion(), deviceModuleVersion);
                }).findAny();
        return offending;
    }
}
