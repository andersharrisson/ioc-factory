/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.tuple.Pair;
import org.primefaces.context.RequestContext;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.ConfigVerifyStatus;
import se.esss.ics.iocfactory.model.ConfigVerifyUIStatus;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.service.CachedSetupService;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.GenerateService;
import se.esss.ics.iocfactory.service.GenerateService.GenerationException;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.VerifyService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling the Generate screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class GenerateManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(GenerateManager.class.getCanonicalName());

    private static final String GENERATE_TITLE = "Generate";

    @Inject private transient ConfigurationService configService;
    @Inject private transient IOCConfigsCache configCache;
    @Inject private transient CachedSetupService setupService;
    @Inject private transient GenerateService generateService;
    @Inject private transient RepositorySelector repoSelector;
    @Inject private transient VerifyService verifyService;


    @Inject
    @IOCUpdated
    private transient Event<IOC> iocUpdated;

    @Inject private transient SecurityService securityService;

    private List<IOC> iocs = Collections.emptyList();
    private List<Configuration> configs = null;

    private List<IOC> selectedIocs = Collections.emptyList();

    private List<Configuration> selectedConfigs = Collections.emptyList();
    private Map<String, Configuration> lastConfigs = new HashMap<>();

    private List<IOCEnvironment> envs = Collections.emptyList();
    private IOCEnvironment selectedEnv;

    private String generateMessage;

    private String previewEnv;
    private String previewSt;

    private boolean canDeployNonProduction;
    private boolean canDeployProduction;

    private String desc;

    private List<ConfigVerifyUIStatus> verifyMessages = Collections.emptyList();
    private boolean verifyWarnings = false;
    private boolean verifyErrors = false;



    /**
     * Initializes the Generate IOC backing bean.
     */
    @PostConstruct
    public void init() {
        canDeployNonProduction = securityService.canDeployNonProduction();
        canDeployProduction = securityService.canDeployProduction();

        loadIocsAndEnvs();
    }

    public void reload() {
        final List<IOC> prevSelectedIocs = selectedIocs;
        final List<Configuration> prevSelectedConfigs = selectedConfigs;

        selectedEnv = null;

        loadIocsAndEnvs();

        selectedIocs = iocs.stream().filter(ioc -> prevSelectedIocs.contains(ioc)).collect(Collectors.toList());
        getConfigurations();
        selectedConfigs = configs.stream().filter(conf -> prevSelectedConfigs.contains(conf)).
                collect(Collectors.toList());

        verifyMessages = Collections.emptyList();
        verifyErrors = verifyWarnings = false;
    }

    public List<IOC> getIocs() { return iocs; }
    public void setIocs(List<IOC> iocs) { this.iocs = iocs; }

    public List<IOC> getSelectedIocs() { return selectedIocs; }
    public void setSelectedIocs(List<IOC> selectedIocs) {
        this.selectedIocs = selectedIocs;
        this.configs = null;
    }

    public void clearConfigSelection() { this.configs = null; }

    public IOC getSelectedIoc() {
        return selectedIocs != null && selectedIocs.size()==1 ? selectedIocs.get(0) : null;
    }

    public boolean canGenerate() {
        return canDeployNonProduction || canDeployNonProduction;
    }

    /** Getter for the list of configurations
     * Loads the configurations for the selected iocs via the {@link IOCConfigsCache}
     * @return
     */
    public List<Configuration> getConfigurations() {
        if (configs == null) {
            configs = selectedIocs == null ? Collections.emptyList() :
                    selectedIocs.stream().flatMap(ioc -> configCache.getConfigurationsForIOC(ioc).stream()).
                    collect(Collectors.toList());
        }

        return configs;
    }

    public List<Configuration> getSelectedConfigs() { return selectedConfigs; }
    public void setSelectedConfigs(List<Configuration> selectedConfigs) { this.selectedConfigs = selectedConfigs; }

    public List<IOCEnvironment> getEnvs() { return envs; }

    public IOCEnvironment getSelectedEnv() { return selectedEnv; }
    public void setSelectedEnv(IOCEnvironment selectedEnv) { this.selectedEnv = selectedEnv; }

    public String getGenerateMessage() { return generateMessage; }
    public void setGenerateMessage(String generateMessage) { this.generateMessage = generateMessage; }

    public String getPreviewEnv() { return previewEnv; }

    public String getPreviewSt() { return previewSt; }

    public Map<String, Configuration> getLastConfigs() { return lastConfigs; }
    public void setLastConfigs(Map<String, Configuration> lastConfigs) { this.lastConfigs = lastConfigs; }

    public String getDesc() { return desc; }
    public void setDesc(String desc) { this.desc = desc; }

    public List<ConfigVerifyUIStatus> getVerifyMessages() { return verifyMessages; }

    public boolean hasVerifyWarnings() { return verifyWarnings; }
    public boolean hasVerifyErrors() { return verifyErrors; }

    public void verify() {
        try {
            verifyInternal(selectedConfigs);
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed to access the CCDB in verify.", ex);
            Util.addGlobalError(ex.getMessage());
        }
    }

    public void verifyLastConfigs() {
        try {
            verifyInternal(lastConfigs.values());
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed to access the CCDB in verifyLastConfigs");
        }
    }

    private void verifyInternal(final Collection<Configuration> cfgs) throws ConfigClientException {
        if (cfgs != null && !cfgs.isEmpty()) {
            verifyMessages = new ArrayList<>();
            for (final Configuration config : cfgs) {
                final Configuration loadedConfig = configService.loadConfiguration(config.getId());

                verifyErrors = verifyWarnings = false;
                for (final ConfigVerifyStatus msg : verifyService.verifyConfiguration(loadedConfig, selectedEnv)) {
                    if (msg.getLevel() == ConsistencyMsgLevel.ERROR) {
                        verifyErrors = true;
                    } else if (msg.getLevel() == ConsistencyMsgLevel.WARN) {
                        verifyWarnings = true;
                    }

                    verifyMessages.add(new ConfigVerifyUIStatus(loadedConfig.getIoc().getName(),
                            loadedConfig.getRevision(), msg));
                };
            }
        }
    }

    /**
     * Check configuration and prepare a list of configuration with highest revision number
     */
    public void prepareAndCheckConfigs() {
        try {
            prepareLastConfigs();
        } catch (GenerationException e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            Util.addGlobalError(e.getMessage());
            return;
        }

        // Prepare dialog message
        final StringBuilder dialogMessage = new StringBuilder("Following configurations will be generated:<br/>");

        getLastConfigs().entrySet().stream().forEach(entry ->
            dialogMessage.append(String.format("<br/>%s, rev: %d", entry.getKey(),  entry.getValue().getRevision())) );

        this.setGenerateMessage(dialogMessage.toString());

        final RequestContext context = RequestContext.getCurrentInstance();

        this.desc = "";

        verifyLastConfigs();
        context.execute("PF('confirmDlg').show()");
    }


    /**
     * Generate selected configurations
     */
    public void generate() {
        if (!canDeployNonProduction && !canDeployProduction) {
            Util.addGlobalError("You don't have permissions to generate IOCs.");
            return;
        }

        try {
            final StringBuilder generatedMessage = new StringBuilder();

            generatedMessage.append("<p>Following configurations were generated:</p>");
            generatedMessage.append("<ul>");

            // Generate configurations
            for(Map.Entry<String, Configuration> entry : this.getLastConfigs().entrySet()) {
                Configuration config = entry.getValue();

                final boolean prodEnv = selectedEnv.isProduction();

                if ((!prodEnv && canDeployNonProduction) ||
                        (prodEnv && canDeployProduction)) {
                    final Configuration loadedConfig = configService.loadConfiguration(config.getId());
                    generateService.generateConfiguration(loadedConfig, selectedEnv, desc);

                    generatedMessage.append( "<li>" +
                            (prodEnv ? "Production configuration " : " Configuration ") +  config.getIoc() + ", " +
                            "rev:" + config.getRevision() + " generated." +
                            "<br/>Directory: " +
                            GenerateService.getDestFolder(loadedConfig, selectedEnv.getDirectory()) +
                            (prodEnv ? "<br/>Configuration commited (frozen)." : "") + "</li>");

                    if (prodEnv) {
                        iocUpdated.fire(config.getIoc());
                    }
                }
            }

            generatedMessage.append("</ul>");

            // Clear table selection
            selectedConfigs.clear();
            selectedIocs.clear();

            Util.showDialogInfo(GENERATE_TITLE,  generatedMessage.toString());
        } catch (GenerateService.GenerationException ge) {
            LOG.log(Level.SEVERE, ge.getMessage(), ge);
            Util.showDialogError(GENERATE_TITLE, "<p>Error while generating IOC configuration:</p>" + ge.getMessage());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception during configuration generation.", e);
            Util.showDialogError(GENERATE_TITLE, "Unexpected error happened during IOC script generation, "
                    + "please notify administrator.<br/>"
                    + "The exception has been logged.");
        }
    }

    /**
     * Dry runs generation and show the results.
     */
    public void preview()
    {
        if (selectedConfigs == null || selectedConfigs.size() != 1) {
            Util.showDialogInfo("Preview", "Please select only a single configuration.");
            return;
        }

        final Configuration config = selectedConfigs.get(0);

        try {
            final Pair<String, String> previews =
                    generateService.previewConfiguration(configService.loadConfiguration(config.getId()));
            previewEnv = previews.getLeft();
            previewSt = previews.getRight();

            final RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('generatePreviewDlg').show()");
        } catch (GenerateService.GenerationException ge) {
            LOG.log(Level.SEVERE, ge.getMessage(), ge);
            Util.showDialogError(GENERATE_TITLE, "<p>Error while previewing IOC configuration:</p>" + ge.getMessage());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception during configuration generation.", e);
            Util.showDialogError(GENERATE_TITLE, "Unexpected error happened during IOC script generation, "
                    + "please notify administrator.<br/>"
                    + "The exception has been logged.");
        }
    }

    private void loadIocsAndEnvs() {
        try {
            iocs = configCache.getIOCs().stream().filter(ioc -> !ioc.getConsistencyStatus().
                    isError()).collect(Collectors.toList());
            configs = null;
        } catch (ConfigClientException ce) {
            LOG.log(Level.SEVERE, "Failed accessing the CCDB in init()", ce);
            Util.addGlobalError(ce.getMessage());
        }

        iocs.sort((l, r) -> l.getName().compareTo(r.getName()));

        List<IOCEnvironment> loadedEnvs = setupService.loadEnvs();
        if (loadedEnvs == null) {
            loadedEnvs = Collections.emptyList();
        }

        envs = loadedEnvs.stream().
                filter(env -> env.getEeeType() == repoSelector.getSelectedEee()).
                filter(env -> (env.isProduction() && canDeployProduction)
                        || (!env.isProduction() && canDeployNonProduction)).
                collect(Collectors.toList());

        selectedEnv = envs.isEmpty() ? null : envs.get(0);
    }

    private void prepareLastConfigs() throws GenerationException {
        Map<String, Configuration> map = this.getLastConfigs();
        map.clear();

        // Go through selected configurations list and return if there are commited configurations
        for (Configuration conf : selectedConfigs) {

            if (conf.isCommited()) {
                continue;
            }

            // Make a map. Key should be the IOC name and value should be configuration with biggest revision number
            if (map.get(conf.getIoc().getName()) == null) {
                map.put(conf.getIoc().getName(), conf);

            } else {
                Configuration lastConf = map.get(conf.getIoc().getName());

                if (conf.getRevision() > lastConf.getRevision()) {
                    map.put(conf.getIoc().getName(), conf);
                }
            }
        }
    }
}
