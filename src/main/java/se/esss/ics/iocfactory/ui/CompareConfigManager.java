/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.YamlHelper;
import se.esss.ics.iocfactory.util.Util;

/**
 * Backing bean supporting the "Compare Configurations" dialog
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@ViewScoped
public class CompareConfigManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(CompareConfigManager.class.getName());

    private final List<String> title = Arrays.asList("", "");
    private final List<String> configText = Arrays.asList("", "");

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper(true);

    @Inject private transient ConfigurationService configService;

    /**
     * Initializes the bean by using a list of exactly 2 items
     * Example usage is while two configurations are selected on the Configure screen.
     * @param configs
     */
    public void initFromConfigs(final List<Configuration> configs) {
        if (configs == null || configs.size() != 2) {
            return;
        }

        try {
            for (int i=0; i < configs.size(); i++) {
                final Configuration loadedConfig = configService.loadConfiguration(configs.get(i).getId());
                configText.set(i, YAML_HELPER.getEntityDumpQuiet(loadedConfig));
                title.set(i, String.format("%s - Rev. %d", loadedConfig.getIoc().getName(),
                        loadedConfig.getRevision()));
            }
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "CCDB access exception in initFromConfigs", ex);
            Util.addGlobalError(ex.getMessage());
        }
    }

    /**
     * Initializes the bean from a list of exactly 2 items
     * Example usage is in the Browse screen when two entries are selected
     * @param entries
     */
    public void initFromEntries(final List<GenerateEntry> entries) {
        if (entries == null || entries.size() != 2) {
            return;
        }

        for (int i = 0; i < entries.size(); i++) {
            final GenerateEntry ge = entries.get(i);

            configText.set(i, ge.getDump());
            title.set(i, String.format("%s - Rev. %d Date %s", ge.getIocName(), ge.getConfigRevision(),
                    TimestampFormatter.format(ge.getGeneratedDate()) ) );
        }
    }

    /**
     * Initialize from a {@link GenerateEntry}, to check the configuration at the time of generating and the saved
     * configuration which might have evolved in the meantime.
     * @param ge
     */
    public void initFromEntry(GenerateEntry ge) {
        final Configuration loadedConfig;
        try {
            loadedConfig = configService.loadConfiguration(ge.getConfigId());
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed accessing CCDB in initFromEntry", ex);
            Util.addGlobalError(ex.getMessage());
            return;
        }

        if (loadedConfig == null) {
            return;
        }
        configText.set(0, YAML_HELPER.getEntityDumpQuiet(loadedConfig));
        title.set(0, String.format("%s - Rev. %d [SAVED]",
                loadedConfig.getIoc().getName(), loadedConfig.getRevision()) );

        configText.set(1, ge.getDump());
        title.set(1,  String.format("%s - Rev. %d Date %s [GENERATED]", ge.getIocName(), ge.getConfigRevision(),
                    TimestampFormatter.format(ge.getGeneratedDate()) ) );
     }

    public List<String> getTitle() { return title; }
    public List<String> getConfigText() { return configText; }
}
