/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.jsonserializers.ParameterSerializer;
import se.esss.ics.macrolib.MacroEntry;


/**
 * An entity bean representing a macro parameter either in a {@link DeviceConfiguration} object as a macro defined
 * per device or either in a {@link Configuraiton} as a global macro.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Entity
@Table( name = "device_parameter",
        indexes = {
            @Index(name = "idx_device_parameter_configuration", columnList = "configuration_id", unique = false),
            @Index(name = "idx_device_parameter_device_configuration", columnList = "device_configuration_id",
                    unique = false),
        }
)
@JsonSerialize(using = ParameterSerializer.class)
public class Parameter
    implements Serializable, HasConsistencyStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "param_name")
    @Basic
    protected String name;
    
    @Column(name = "snippet_name", nullable = true)
    @Basic
    protected String snippetName;

    @Column(name = "param_value")
    @Basic
    protected String value;

    @NotNull
    @Column(name = "param_type")
    @Enumerated(EnumType.STRING)
    protected ParameterType type;

    @Transient
    @JsonIgnore
    protected List<MacroExpansion> expandedValues;

    @Transient
    @JsonIgnore
    protected MacroEntry macroEntry;
    
    @Transient
    @JsonIgnore
    protected Set<PlaceholderTrait> traits;

    @Transient
    @JsonIgnore
    protected String desc;

    @Transient
    @JsonIgnore
    protected ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "device_configuration_id", referencedColumnName = "id")
    @JsonIgnore
    private DeviceConfiguration deviceConfiguration;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "configuration_id", referencedColumnName = "id")
    @JsonIgnore
    private Configuration configuration;

    /** Uniquely identifies a param within a configuration */
    @Transient
    @JsonIgnore
    private int paramPosition;

    @Version
    @Column(name = "optimistic_version")
    @JsonIgnore
    private Long optimisticVersion;
    
    @Transient
    @JsonIgnore
    private Snippet snippet;

    protected Parameter() {}

    /**
     * Constructs an entity with the given name & parameter type ({@link ParameterType})
     *
     * @param name
     * @param snippetName 
     * @param type
     * @param traits
     */
    public Parameter(String name, String snippetName, ParameterType type, Set<PlaceholderTrait> traits) {
        this.name = name;
        this.snippetName = snippetName;
        this.type = type;
        this.traits = traits;
    }

    /**
     * Constructs an entity by using a {@link ParameterPlaceholder} as a model
     *
     * @param placeholder
     * @param snippet
     */
    public Parameter(ParameterPlaceholder placeholder, Snippet snippet) {
        this.name = placeholder.getName();
        this.snippetName = snippet != null ? snippet.getName() : null;
        this.snippet = snippet;
        this.type = placeholder.getType();
        this.desc = placeholder.getDesc();
        this.traits = placeholder.getTraits();
        this.macroEntry = placeholder.getMacroEntry();
    }

    public Long getId() { return id; }
    public String getName() { return name; }
    public String getSnippetName() { return snippetName; }
    public String getDesc() { return desc; }
    public void setDesc(String desc) { this.desc = desc; }
    public ParameterType getType() { return type; }
    public Set<PlaceholderTrait> getTraits() { return traits; }
    public void setTraits(Set<PlaceholderTrait> traits) { this.traits = traits; }
    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    public List<MacroExpansion> getExpandedValues() {
        if (expandedValues == null) {
            expandedValues = Collections.emptyList();
        }
        return expandedValues;
    }

    public void setExpandedValue(List<MacroExpansion> expandedValues) {
        this.expandedValues = expandedValues;
    }

    public MacroEntry getMacroEntry() { return macroEntry; }
    public void setMacroEntry(MacroEntry macroEntry) { this.macroEntry = macroEntry; }

    public DeviceConfiguration getDeviceConfiguration() { return deviceConfiguration; }
    public void setDeviceConfiguration(DeviceConfiguration deviceConfiguration) {
        this.deviceConfiguration = deviceConfiguration;
    }

    public String getDeviceName() { 
        return deviceConfiguration != null ? deviceConfiguration.getName() : 
                configuration != null ? this.configuration.getIoc().getName() : null; 
    }
    
    public Configuration getConfiguration() { return configuration; }
    public void setConfiguration(Configuration configuration) { this.configuration = configuration; }

    public int getParamPosition() { return paramPosition; }
    public void setParamPosition(int paramPosition) { this.paramPosition = paramPosition; }

    public Snippet getSnippet() { return snippet; }
    public void setSnippet(Snippet snippet) { 
        this.snippet = snippet; 
        if (snippet != null) {
            this.snippetName = snippet.getName();
        }
    }
    
    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(StringUtils.upperCase(this.name));        
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + Objects.hashCode(this.getSnippetName());
        hash = 97 * hash + Objects.hashCode(this.getValue());
        hash = 97 * hash + Objects.hashCode(this.getDeviceName());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parameter other = (Parameter) obj;
        return StringUtils.equalsIgnoreCase(this.name, other.name) && this.type == other.type &&
                Objects.equals(this.getSnippetName(), other.getSnippetName()) &&
                Objects.equals(this.getDeviceName(), other.getDeviceName()) &&
                Objects.equals(this.getValue(), other.getValue());
    }
}
