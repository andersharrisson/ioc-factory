/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Date;

/**
 * A model value class that represents an Audit entry for an IOC
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class AuditEntry implements Serializable, HasConsistencyStatus {
    /** used as unique id in the UI logic, not as a db key*/
    private final long id;
    private final IOC ioc;
    private final String environment;
    private final GenerateEntry generateEntry;
    private boolean configDifferent = true;
    private Date diskModificationTime = null;

    private final ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    /** Constructor needed for view state save restore */
    public AuditEntry() {
        id = 0;
        ioc = null;
        environment = null;
        generateEntry = null;
    }

    /**
     * Constructs an entity with all fields initialized
     * @param id an unique long id
     * @param ioc
     * @param environment
     * @param generateEntry optional can be {@code null} to signify that there is no generate entry for this IOC
     */
    public AuditEntry(long id, IOC ioc, String environment, GenerateEntry generateEntry) {
        this.id = id;
        this.ioc = ioc;
        this.environment = environment;
        this.generateEntry = generateEntry;
    }

    public long getId() { return id; }
    public IOC getIoc() { return ioc; }
    public String getEnvironment() { return environment; }
    public GenerateEntry getGenerateEntry() { return generateEntry; }

    public boolean isConfigDifferent() { return configDifferent; }
    public void setConfigDifferent(boolean configDifferent) { this.configDifferent = configDifferent; }

    public Date getDiskModificationTime() { return diskModificationTime; }
    public void setDiskModificationTime(Date diskModificationTime) { this.diskModificationTime = diskModificationTime; }

    /**
     * Shortcut getter for {@link AuditEntry#ioc} {@link IOC#getName()}
     *
     * @return
     */
    public String getIocName() { return ioc.getName(); }

    /**
     * Returns {@code true} if there is a generate entry for this IOC
     * @return
     */
    public boolean hasGenerateEntry() { return generateEntry != null; }

    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }
}
