/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import se.esss.ics.iocfactory.service.LogFilterValues;
import se.esss.ics.iocfactory.service.LogService;
import se.esss.ics.iocfactory.service.LogSortColumns;
import se.esss.ics.iocfactory.util.LazyLoadUtil;

/**
 *
 * Lazy data model for Log entries -- lazy connection with primefaces
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class LogLazyDataModel extends LazyDataModel<LogEntry> {

    private static final long serialVersionUID = 2465722903992953339L;
    private static final Logger LOG = Logger.getLogger(LogLazyDataModel.class.getName());

    @Inject
    private LogService logService;

    @Override
    public LogEntry getRowData(String rowkey) {
        return logService.getLogEntryById(Long.parseLong(rowkey));
    }

    @Override
    public Object getRowKey(LogEntry logEnt) {
        return logEnt.getId();
    }

    /**
     * Main lazy loading method. RowCount property must be set to tell datatables count of all rows in a table. 
     * Returns limited list of rows.
     * 
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return 
     */
    @Override
    public List<LogEntry> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
            Map<String, Object> filters) {
        final LogFilterValues filterVal = handleFiltration(filters);
        final Long allRowsCount = logService.getLogEntryCount(filterVal);
        final List<LogEntry> entries = logService.getLogEntries(first, pageSize, handleSortColumns(sortField), 
                LazyLoadUtil.castFromPfSortOrder(sortOrder), filterVal);
        this.setRowCount(allRowsCount.intValue());

        LOG.fine(String.format("All rows count: %s, first page:%s, page size: %s, fetch entites: %s", allRowsCount, 
                first, pageSize, entries.size()));
        return entries;
    }

  
    /***
     * Casts sort columns to LogSortColumns type
     *  
     * @param sortField PF lazydatamodel column for ordering
     * @return LogSortColumns column for ordering 
     */
    
    public static LogSortColumns handleSortColumns(String sortField) {
        if (sortField == null) {
            return null;
        }

        switch (sortField) {
        case "user":
            return LogSortColumns.USER;
        case "details":
            return LogSortColumns.DETAILS;      
        case "type":
            return LogSortColumns.TYPE;
        case "timestamp":
            return LogSortColumns.TIMESTAMP;
        default:
            return null;
        }
    }

    /***
     * Maps Primefaces table column filter values to LogFilterValues type. This way table names are independent of 
     * underlying implementation.
     * 
     * @param filters Map of fiter name and value pair
     * @return wrapped filter values (nullable)
     */
    private LogFilterValues handleFiltration(Map<String, Object> filters) {
        if (filters.size() < 1) {
            return null;
        }
        return new LogFilterValues(
                //string 
                LazyLoadUtil.getFilterValueToString("details", filters),
                LazyLoadUtil.getFilterValueToString("user", filters),
                //date
                LazyLoadUtil.getFilterValueToDate("timestamp", filters),
                //eval
                LazyLoadUtil.getFilterValueToString("type", filters) == null ? null : 
                        LogEntryType.valueOf(LazyLoadUtil.getFilterValueToString("type", filters)));
    }
}
