/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;
import se.esss.ics.iocfactory.model.jsonserializers.ModuleDepsSerializer;

/**
 * A model of an EPICS module for a given version, containing its name, EPICS base version and OS
 * It is an value class.
 *
 * Besides the specification, some instances of the class, notably the ones retrieved via the {@link Repository} class
 * contain non-persistent run-time determined data such as a list of module dependencies ({@link ModuleDependency}),
 * and a list of snippets ({@link Snippet})
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Embeddable
@JsonPropertyOrder({"name", "moduleVersion", "epicsVersion", "os", "deoendencies"})
public class Module implements Serializable {
    @Column(name = "module_name")
    @JsonProperty("name")
    private final String name;

    @Column(name = "module_version")
    @JsonProperty("version")
    private final String moduleVersion;

    @Transient
    @JsonIgnore
    private Long epicsVersion;

    @Transient
    @JsonIgnore
    private String os;

    @Transient
    @JsonSerialize(using = ModuleDepsSerializer.class)
    private final List<ModuleDependency> dependencies;

    @Transient
    @XmlTransient
    @JsonIgnore
    private final List<Snippet> snippets;

    protected Module() {
        this.name = null;
        this.moduleVersion = "0.0.0";
        this.epicsVersion = 0L;
        this.os = null;
        this.dependencies = Collections.emptyList();
        this.snippets = Collections.emptyList();
    }

    /**
     * Constructs only a named module, used for displaying a module on UI for devices with invalid modules
     *
     * @param name
     */
    public Module(String name) {
        this.name = name;
        this.moduleVersion = "0.0.0";
        this.epicsVersion = 0L;
        this.os = null;
        this.dependencies = Collections.emptyList();
        this.snippets = Collections.emptyList();
    }


    /**
     * Construct a new {@link Module} value object
     *
     * @param name name of the module
     * @param version version of the module
     * @param epicsVersion EPICS base version of the module
     * @param os operating system
     * @param dependencies a list of immediate dependencies
     * @param snippets a list of snippets
     */
    public Module(String name, String version, Long epicsVersion, String os,
            List<ModuleDependency> dependencies, List<Snippet> snippets) {
        this.name = name;
        this.moduleVersion = version;
        this.epicsVersion = epicsVersion;
        this.os = os;
        this.dependencies = ImmutableList.copyOf(dependencies);
        this.snippets = ImmutableList.copyOf(snippets);
    }

    public String getName() { return name; }
    public String getModuleVersion() { return moduleVersion; }

    public Long getEpicsVersion() { return epicsVersion; }
    public void setEpicsVersion(Long epicsVersion) { this.epicsVersion = epicsVersion; }

    public String getOs() { return os; }
    public void setOs(String os) { this.os = os; }

    public List<ModuleDependency> getDependencies() { return dependencies; }
    public List<Snippet> getSnippets() { return snippets; }

    public String getOsEpicsPair() {
        return String.format("%s-%s", os, epicsVersion.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, moduleVersion, epicsVersion, os);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Module other = (Module) obj;
        return Objects.equals(this.name, other.name)
                && Objects.equals(this.moduleVersion, other.moduleVersion)
                && Objects.equals(this.epicsVersion, other.epicsVersion)
                && Objects.equals(this.os, other.os);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.format("Module: %s Version: %s%n"
                + "\tBase: %s%n"
                + "\tOS: %s%n"
                + "\tDependencies:%n", name,
                moduleVersion,
                VersionConverters.getEpicsConverter().toString(epicsVersion),
                os));
        dependencies.forEach(dep -> sb.append(String.format("\t\t%s%n", dep.toString())));

        sb.append(String.format("\tSnippets:%n"));
        snippets.forEach(snippet ->
            sb.append(String.format("\t\t%s - %s", snippet.toString(),
                snippet.getPlaceholders().stream().map(Object::toString).collect(Collectors.joining(", "))))
        );

        return sb.toString();
    }
}
