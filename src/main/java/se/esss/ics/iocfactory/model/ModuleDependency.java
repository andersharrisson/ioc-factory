/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import se.esss.ics.iocfactory.util.ModuleVersionsComparator;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class representing a module dependency in form name, version and whether a greater version is expected
 * or not.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class ModuleDependency implements Serializable {
    private String name;
    private String version;
    private boolean greater;

    protected ModuleDependency() {}

    /**
     * Constructs a {@link ModuleDependency} pair
     *
     * @param name name of the module
     * @param version module version
     * @param greater <code>true</code> if this version and greater are allowed
     */
    public ModuleDependency(String name, String version, boolean greater) {
        this.name = name;
        this.version = version;
        this.greater = greater;
    }

    public String getName() { return name; }
    public String getVersion() { return version; }
    public boolean isGreater() { return greater; }

    /**
     * Check whether a {@link Module} satisfies this dependency
     *
     * @param module
     * @return
     */
    public boolean isSatisfiedBy(Module module) {
        if (name.equals(module.getName())) {
            return greater ? ModuleVersionsComparator.compare(module.getModuleVersion(), version) >= 0 :
                    ModuleVersionsComparator.compare(version, module.getModuleVersion()) == 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        final ModuleDependency other = (ModuleDependency) obj;

        return Objects.equals(this.name, other.name) &&
                ModuleVersionsComparator.compare(this.version, other.version) == 0 &&
                this.greater==other.greater;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, version, greater);
    }

    @Override
    public String toString() {
        return String.format("%s%s%s", name, "".equals(version) && !greater ? "" : ","  ,version + (greater ? "+" : ""));
    }
}
