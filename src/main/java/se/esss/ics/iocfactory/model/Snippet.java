/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.google.common.collect.ImmutableList;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import se.esss.ics.iocfactory.service.RepositoryScanner;
import se.esss.ics.macrolib.MacroStorage;

/**
 * Value class representing a module snippet in the repository
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class Snippet implements Serializable {
    private final String name;
    private final File file;
    private final List<ParameterPlaceholder> placeholders;
    private final MacroStorage macros;

    /**
     * Constructs a snippet object by providing all fields
     *
     * @param name the name of the snippet
     * @param file the file as scanned by the {@link RepositoryScanner{
     * @param macros the MacroLib list of macros
     * @param placeholders the placeholders for the macros that contain additional metadata (such as tyoe information)
     */
    public Snippet(String name, File file, MacroStorage macros, List<ParameterPlaceholder> placeholders) {
        this.name = name;
        this.file = file;
        this.placeholders = ImmutableList.copyOf(placeholders);
        this.macros = macros;
    }

    public String getName() { return name; }
    public File getFile() { return file; }
    public List<ParameterPlaceholder> getPlaceholders() { return placeholders; }
    public MacroStorage getMacros() { return macros; }

    @Override
    public int hashCode() { return Objects.hash(this.file); }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        } else {
            final Snippet other = (Snippet) obj;
            return Objects.equals(this.file, other.file);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(name + ": ");
        placeholders.forEach(p -> sb.append(String.format("%s:%s", p.getName(),p.getType().toString())));
        return sb.toString();
    }
}
