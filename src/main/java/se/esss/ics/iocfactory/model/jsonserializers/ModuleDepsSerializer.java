/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.jsonserializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.List;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.util.Util;

/**
 * @{@link JsonSerializer} used for Module dependencies 
 *
 * To be used for JSON conversion
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@SuppressWarnings("squid:RedundantThrowsDeclarationCheck")
public class ModuleDepsSerializer extends JsonSerializer<List<ModuleDependency>> {
    @Override
    public void serialize(List<ModuleDependency> value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException,
            JsonProcessingException {
        if (value != null) {
            gen.writeString( Util.formatModuleDependenceies(value) );
        }
    }
}
