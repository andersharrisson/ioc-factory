/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package se.esss.ics.iocfactory.model;

import com.google.common.math.LongMath;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Named;

/**
 * This class provides conversion between {@link Long} representations of EPICS & Module versions to/from {@link String}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
public class VersionConverters {
    public static final Pattern MODULE_PATTERN = Pattern.compile("(\\d{1,3})(?:\\.(\\d{1,3})){1}(?:\\.(\\d{1,3})){1}");
    public static final Pattern EPICS_PATTERN = Pattern.compile(
            "(\\d{1,3})(?:\\.(\\d{1,3})){1}(?:\\.(\\d{1,3})){1}(?:\\.(\\d{1,3}))?");

    private static final VersionConverter MODULE_CONVERTER = new VersionConverter(
            MODULE_PATTERN, 3, 3);
    private static final VersionConverter EPICS_CONVERTER = new VersionConverter(
            EPICS_PATTERN, 3, 4);

    /** Getter of a static instance of a module version converter
     * @return
     */
    public static VersionConverter getModuleConverter() {
        return MODULE_CONVERTER;
    }

    /** Getter of a static instance of a epics version converter
     * @return
     */
    public static VersionConverter getEpicsConverter() {
        return EPICS_CONVERTER;
    }

    /**
     * Converts a {@link Long} module version to a formated string
     * @param version
     * @return
     */
    public static String moduleVersionToString(Long version) {
        return version != null && version != 0 ? MODULE_CONVERTER.toString(version) : "";
    }

    /**
     * Checks whether module version string is named version
     * 
     * @param modVersion
     * @return 
     */
    public static boolean isModuleVersionNamed(String modVersion) {
        final Matcher matcher = VersionConverter.OPTIONAL_MODULE_PATTERN.matcher(modVersion);
        return !matcher.matches();
    }
    
    /**
     * Converts a {@link Long} epics version to a formated string
     * @param version
     * @return
     */
    public static String epicsVersionToString(Long version) {
        return version != null && version != 0 ? EPICS_CONVERTER.toString(version) : null;
    }

    /**
     * Embedded class actually implementing a converter for versions in the form x.y.z
     */
    public static class VersionConverter {

        public static final Pattern OPTIONAL_MODULE_PATTERN
                = Pattern.compile("(\\d{1,3}){1}" +
                        "(?:\\.(\\d{1,3}))?" +
                        "(?:\\.(\\d{1,3}))?" +
                        "(?:\\.(\\d{1,3}))?" +
                        "(?:\\.(\\d{1,3}))?" +
                        "(?:\\.(\\d{1,3}))?");

        private final Pattern pattern;
        private final int minFields;
        private final int maxFields;
        private final long[] fieldMultipliers;

        /**
         * Constructs a {@link VersionConverter}
         *
         * @param pattern - the Regex pattern for the string
         * @param minFields - minimum number of numeric fields
         * @param maxFields  - maximum number of numeric fields
         */
        public VersionConverter(Pattern pattern, int minFields, int maxFields) {
            // Initialize multipliers
            fieldMultipliers = new long[maxFields];
            for (int i = 0; i < maxFields; i++) {
                fieldMultipliers[i] = LongMath.checkedPow(1000, maxFields - i - 1);
            }

            this.pattern = pattern;
            this.minFields = minFields;
            this.maxFields = maxFields;
        }

        /**
         * Converts a long integer representation of version to a string
         * @param version
         * @return
         */
        public String toString(long version) {
            final StringBuilder sb = new StringBuilder();

            final int[] versions = getVersionsArray(version);
            for (int i = 0; i < versions.length; i++) {
                // Prevents printing of trailing zeroes, when getMinFields is less(not equal) to getMaxFields
                if (i >= minFields && i < maxFields && versions[i] == 0) {
                    continue;
                }

                if (i != 0) {
                    sb.append('.');
                }

                sb.append(Integer.toString(versions[i]));
            }
            return sb.toString();
        }

        /** Converts a string representation to a long integer version representation
         * @param versionString
         * @param strict whether the version should explicitly contain all numeric fields (the maximum number of
         * fields)
         * @return
         */
        public long fromString(String versionString, boolean strict) {
            final Matcher matcher = (strict ? pattern : OPTIONAL_MODULE_PATTERN).matcher(versionString);

            if (!matcher.matches()) {
                throw new IllegalArgumentException("Version '" + versionString + "' string invalid");
            }

            int[] versions = new int[matcher.groupCount()];
            for (int i = 1; i <= matcher.groupCount(); ++i) {
                final String group = matcher.group(i);

                if (group != null) {
                    versions[i - 1] = Integer.parseInt(group);
                } else {
                    // Exit condition, there are no actual more items inside
                    versions = Arrays.copyOf(versions, i - 1);
                    break;
                }
            }

            validateNumFields(versions, strict);

            long result = 0;
            for (int i = 0; i < versions.length; ++i) {
                result += versions[i] * fieldMultipliers[i];
            }
            return result;
        }

        /**
         * A conversion from string version to a long integer representation while onoring the strict parameter, see
         * {@link VersionConverter#fromString(java.lang.String, boolean)}
         * @param versionString
         * @return
         */
        public long fromString(String versionString) {
            return fromString(versionString, true);
        }

        private int[] getVersionsArray(long versions) {
            int[] result = new int[maxFields];
            for (int i = 0; i < maxFields; ++i) {
                final long remainingVersion = (i == 0) ? versions : versions % fieldMultipliers[i - 1];

                // result of division is always in [0..999]
                result[i] = (int) (remainingVersion / fieldMultipliers[i]);
            }
            return result;
        }

        private void validateNumFields(int[] arg, boolean strict) {
            if ((strict && arg.length < minFields) || arg.length > maxFields) {
                throw new IllegalArgumentException(
                        String.format("%s can have a minimum of %d fields and maximum of %d fields, args were %s",
                                this.getClass().getName(), minFields, maxFields, Arrays.toString(arg)));
            }
            for (int versionField : arg) {
                if (versionField < 0 || versionField > 999) {
                    throw new IllegalArgumentException("Version number can only contain positive integer <= 999");
                }
            }
        }
    }
}
