/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

/**
 * This class models an IOC as persisted in our DB.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Entity
@Table(name = "ioc")
public class IOC implements Serializable,
    HasConsistencyStatus {
    @Id
    @Column(name = "ioc_name")
    private String name;

    @Transient
    private String type;

    @Transient
    private String os;

    @Transient
    private String hostname;

    @Transient
    private String desc;

    @Basic
    @Column(name = "last_config_revision")
    @JsonIgnore
    private int lastConfigRevision;

    @Transient
    @JsonIgnore
    private ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    @Version
    @Column(name = "optimistic_version")
    private Long optimisticVersion;

    protected IOC() { lastConfigRevision = 0; }

    public IOC(final String name) { this.name = name; }

    /**
     * Constructs an IOC from parameters
     *
     * @param name name of the IOC
     * @param type CCDB type of the Installation Slot in CCDB
     * @param os OS of the IOC
     * @param hostname  the hostname of the hosting machine (cpu(
     * @param desc Description describing this IOC
     */
    public IOC(String name, String type, String os, String hostname, String desc) {
        super();
        this.name = name;
        this.type = type;
        this.os = os;
        this.hostname = hostname;
        this.desc = desc;
        this.lastConfigRevision = 0;
    }

    public String getName() { return name; }

    public String getType() { return type; }
    public void setType(String type) { this.type = type; }

    public String getOs() { return os; }
    public void setOs(String os) { this.os = os; }

    public String getHostname() { return hostname; }
    public void setHostname(String hostname) { this.hostname = hostname; }

    public String getDesc() { return desc; }
    public void setDesc(String desc) { this.desc = desc; }

    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }

    public int getLastConfigRevision() { return lastConfigRevision; }
    public void setLastConfigRevision(int lastConfigRevision) { this.lastConfigRevision = lastConfigRevision; }

    @Override public String toString() { return name; }

    @Override
    public int hashCode() { return Objects.hashCode(name); }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IOC other = (IOC) obj;
        return Objects.equals(name, other.name);
    }
}
