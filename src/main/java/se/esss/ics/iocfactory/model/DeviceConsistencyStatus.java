/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;


import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.ERROR;
import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.INFO;

/**
 * {@link ConsistencyStatus} enum definitions for {@link DeviceConfiguration} entities
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public enum DeviceConsistencyStatus implements ConsistencyStatus {
    MODULE_DOES_NOT_EXIST("Module does not exist in the EEE", ERROR),
    MODULE_NOT_DEFINED("No module defined for the device", ERROR),
    DEVICE_IS_ORPHANED("Device does not exist in CCDB anymore", ERROR),
    INVALID_SNIPPET("The specified snippet is not valid for the module version", ERROR),
    DIFFERENT_MODULE("The module in the configuration differs from the one reported by the CCDB", ERROR),
    DEVICE_IS_NEW("New device added in CCDB", INFO);

    private final String desc;
    private final ConsistencyMsgLevel level;

    private DeviceConsistencyStatus(String desc, ConsistencyMsgLevel level)
    {
        this.desc = desc;
        this.level = level;
    }

    public String getDesc() { return desc; }
    public ConsistencyMsgLevel getLevel() { return level; }
}
