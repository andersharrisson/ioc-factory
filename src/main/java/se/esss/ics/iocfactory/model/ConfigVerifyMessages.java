/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a collection of {@link ConfigVerifyStatus} messages
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConfigVerifyMessages {
    private List<ConfigVerifyStatus> messages = new ArrayList<>();

    /**
     * Add a new formated message with defined level, category & format, 
     * per {@link String#format(java.lang.String, java.lang.Object...) 
     * 
     * @param msgLevel message level
     * @param category String describing message category to be shown on UI
     * @param msg message format
     * @param args message arguments
     * @return the formated message string 
     */
    public String addVerifyStatus(final ConsistencyMsgLevel msgLevel, final String category,
            final String msg, final Object... args) {
        final String formattedMsg = String.format(msg, args);
        messages.add(new ConfigVerifyStatus(msgLevel, formattedMsg, category));
        return formattedMsg;
    }
    
    public List<ConfigVerifyStatus> getMessages() { return messages; }    
}
