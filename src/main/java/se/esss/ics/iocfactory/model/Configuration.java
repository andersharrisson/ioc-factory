/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.Version;
import se.esss.ics.iocfactory.model.jsonserializers.ConfigSerializer;

/**
 * Represents a IOC configuration as persisted in database and as used in processing.
 *
 * The properties that are not stored in the database are marked {@link Transient}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Entity
@Table(name = "configuration", indexes = { @Index(name = "idx_configuration_ioc", columnList = "ioc_name", unique = false) })
@JsonSerialize(using = ConfigSerializer.class)
public class Configuration implements
    Serializable,
    HasConsistencyStatus {
    public static final int DEFAULT_PROC_SERV_PORT = 2000;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ioc_name", nullable = false)
    private IOC ioc;

    @Basic
    @Column(name = "revision")
    private int revision;

    @Basic
    @Column(name = "os")
    private String os;

    @Column(name = "edit_comment")
    @Basic
    private String comment;

    @Basic
    @Column(name = "username")
    private String user;

    @Basic
    @Column(name = "commited")
    private boolean commited;

    @Basic
    @Column(name = "last_edit")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastEdit;

    @Basic
    @Column(name = "epics_version")
    private Long epicsVersion;

    @Basic
    @Column(name = "env_version")
    private String envVersion;

    @Basic
    @Column(name = "proc_serv_port")
    private int procServPort = DEFAULT_PROC_SERV_PORT;

    /** Yaml serializaiton based SHA-1 */
    @Basic
    @Column(name = "config_hash", length = 40)
    private String hash;

    /** Generated output calculated hash. Might be {@code null} if the config was just created */
    @Basic
    @Column(name = "output_hash", length = 40)
    private String outputHash;

    @OneToMany(mappedBy="configuration", cascade = { CascadeType.ALL }, orphanRemoval = true)
    @OrderColumn(name = "item_no")
    private List<DeviceConfiguration> devices = new ArrayList<>();

    @OneToMany(mappedBy = "configuration", cascade = { CascadeType.ALL }, orphanRemoval = true)
    @OrderColumn(name = "global_item_no")
    private List<Parameter> globals = new ArrayList<>();

    @Version
    @Column(name = "optimistic_version", length = 40)
    private Long optimisticVersion;

    @Transient
    private final ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    public Long getId() { return id; }

    public IOC getIoc() { return ioc; }
    public void setIoc(IOC ioc) { this.ioc = ioc; }

    public int getRevision() { return revision; }
    public void setRevision(int revision) { this.revision = revision; }

    public List<DeviceConfiguration> getDevices() { return devices; }
    public void setDevices(List<DeviceConfiguration> devices) { this.devices = devices; }

    public List<Parameter> getGlobals() { return globals; }
    public void setGlobals(List<Parameter> globals) { this.globals = globals; }

    public String getComment() { return comment; }
    public void setComment(String comment) { this.comment = comment; }

    public String getUser() { return user; }
    public void setUser(String user) { this.user = user; }

    public boolean isCommited() { return commited; }
    public void setCommited(boolean commited) { this.commited = commited; }

    public Date getLastEdit() { return lastEdit != null ? new Date(lastEdit.getTime()) : null; }
    public void setLastEdit(Date lastEdit) { this.lastEdit = lastEdit != null ? new Date(lastEdit.getTime()) : null; }

    public Long getEpicsVersion() { return epicsVersion; }
    public void setEpicsVersion(Long epicsVersion) { this.epicsVersion = epicsVersion; }

    public String getEnvVersion() { return envVersion; }
    public void setEnvVersion(String envVersion) { this.envVersion = envVersion; }

    public String getOs() { return os; }
    public void setOs(String os) { this.os = os; }

    public int getProcServPort() { return procServPort; }
    public void setProcServPort(final int procServPort) { this.procServPort = procServPort; }

    public String getHash() { return hash; }
    public void setHash(String jsonHash) { this.hash = jsonHash; }

    public String getOutputHash() { return outputHash; }
    public void setOutputHash(String outputHash) { this.outputHash = outputHash; }

    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Configuration other = (Configuration) obj;
        return Objects.equals(this.id, other.id);
    }
}
