/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 * 
 * This file is part of IOC Factory.
 * 
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.jsonserializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.Snippet;


/**
 *
 * @author mpavleski
 */
public class DeviceSerializer extends JsonSerializer<DeviceConfiguration> {

    @Override
    public void serialize(DeviceConfiguration dev, JsonGenerator gen, SerializerProvider sp) 
            throws IOException, JsonProcessingException {
        if (dev != null && !StringUtils.isEmpty(dev.getName()) && dev.getSupportModule() != null) {
            gen.writeStartObject();
            
            gen.writeObjectField("deviceName", dev.getName());
            gen.writeObjectField("supportModule", dev.getSupportModule());
            
            gen.writeArrayFieldStart("snippets");
            for (Snippet sn : dev.getSnippets()){
                gen.writeStartObject();
                gen.writeObjectField("snippetName", sn.getName());
                
                gen.writeFieldName("parameters");
                gen.writeStartObject();
                for (Parameter p : dev.getParameters()) {
                    if (Objects.equals(p.getSnippetName(), sn.getName())) {
                        gen.writeObject(p);
                    }
                }
                gen.writeEndObject();;
                
                gen.writeEndObject();
            }
            gen.writeEndArray();
            
            gen.writeEndObject();
        }
    } 
}
