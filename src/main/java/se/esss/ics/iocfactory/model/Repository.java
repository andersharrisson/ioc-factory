/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Vetoed;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;

/**
 * A repository model. Contains all EPICS versions, operating systems and modules supported
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@Vetoed
public class Repository implements Serializable {
    private static final Logger LOG = Logger.getLogger(Repository.class.getName());

    private Set<Long> epicsVersions = new HashSet<>();
    private Set<String> osVersions = new HashSet<>();

    private Set<String> moduleNames = new TreeSet<>();

    /** By module name all modules for all oses & epics versions*/
    private Map<String, List<Module>> modulesByName = new TreeMap<>();

    private List<RepositoryMessage> messages = new ArrayList<>();
    private boolean error = false;

    private Date lastScanTime;
    private String lastScanUser;

    /**
     *  Adds a module to the repository
     *
     * @param module the {@link Module} to be added
     */
    public void addModule(Module module) {
        final Long epicsVersion = module.getEpicsVersion();
        if (!epicsVersions.contains(epicsVersion)) {
            throw new RuntimeException("Unknown module EPICS version, such base does not exist in repo.");
        }

        // Encountered a new OS, add it to the set of known OSes
        final String osVersion = module.getOs();
        if (!osVersions.contains(osVersion)) {
            osVersions.add(osVersion);
        }

        moduleNames.add(module.getName());

        // Add to the modules map
        addModuleToModulesByName(module);
    }

    /**
     * Gets a {@link Set} of EPICS versions available in the repository
     *
     * @return result a {@link Set} of versions as {@code Long}
     */
    public Set<Long> getEpicsVersions() { return ImmutableSet.copyOf(epicsVersions); }

    /**
     * Gets all the OS versions available in the repository
     *
     * @return a {@link Set} of OS versions as {@link String}
     */
    public Set<String> getOsVersions() { return ImmutableSet.copyOf(osVersions); }

    private void addModuleToModulesByName(Module module) {
        final String moduleName = module.getName();

        List<Module> moduleList = modulesByName.get(moduleName);
        if (moduleList == null) {
            moduleList = new ArrayList<>();
            modulesByName.put(moduleName, moduleList);
        }
        moduleList.add(module);
    }


    /**
     * Adds an known OS Version to the repository
     *
     * @param osVersion {@link String} OS version
     */
    public void addOsVersion(String osVersion) {
        osVersions.add(osVersion);
    }

    /**
     * Adds an EPICS version to the repository
     *
     * @param epicsVersion {@link EpicsVersion} version of the repository
     */
    public void addEPICSVersion(Long epicsVersion) {
        epicsVersions.add(epicsVersion);
    }

    /**
     *  Gets all known module names
     *
     * @return a set of module names
     */
    public Set<String> getModuleNames() {
        return ImmutableSet.copyOf(moduleNames);
    }

    /**
     * Gets all modules for given name
     *
     * @param moduleName
     * @return
     */
    public List<Module> getModulesForName(final String moduleName) {
        final List<Module> result = modulesByName.get(moduleName);
        return result != null ? ImmutableList.copyOf(result) : ImmutableList.of();
    }

    private Stream<Module> filterModulesByNameOsEpicsVersion(final String moduleName,
        final String os, final Long epicsVersion) {
        final List<Module> moduleList = modulesByName.get(moduleName);
        if (moduleList == null) {
            return Stream.empty();
        } else {
            LOG.fine(String.format("filterModulesByNameOsEpicsVersion moduleList for spec %s, %s, %s is %s",
                    moduleName, os, VersionConverters.epicsVersionToString(epicsVersion),
                    moduleList.stream().map(module -> String.format("Name: %s, OS: %s, EPICS: %s, Version: %s",
                            module.getName(), module.getOs(),
                            VersionConverters.epicsVersionToString(module.getEpicsVersion()),
                            module.getModuleVersion())).
                    collect(Collectors.joining("; "))));
            return moduleList.stream().filter(mod -> StringUtils.equals(mod.getOs(), os)
                    && Objects.equal(mod.getEpicsVersion(), epicsVersion));
        }
    }

    /**
     * Given a specification of a module in terms of name, OS and EPICS version, return a {@link List} for all
     * conforming modules
     *
     * @param moduleName specification for a module name
     * @param os specification for a module Operating System
     * @param epicsVersion specification for a module EPICS base version
     * @return a list of modules matching the specification
     */
    public List<Module> getModulesForSpec(final String moduleName, final String os,
            final Long epicsVersion) {
        return filterModulesByNameOsEpicsVersion(moduleName, os, epicsVersion).collect(Collectors.toList());
    }

    /**
     * Given a specification of a module in terms of name, OS and EPICS version, return the latest module (if any)
     * that matches that specification
     *
     * @param moduleName specification for a module name
     * @param os specification for a module Operating System
     * @param epicsVersion specification for a module EPICS base version
     * @param comparator an object implementing the {@link ModuleVersionsComparator.ModuleComparator}
     * @return an {@link Optional} that might contain the latest module matching the specification
     */
    public Optional<Module> getLatestModuleForSpec(final String moduleName, final String os,
                                                   final Long epicsVersion,
                                                   final ModuleVersionsComparator.ModuleComparator comparator) {
        return filterModulesByNameOsEpicsVersion(moduleName, os, epicsVersion).max((l,r) ->
                comparator.compare(l.getModuleVersion(), r.getModuleVersion()));
    }

    /**
     * Given a specification of a module in terms of name, OS and EPICS version, return the latest module (if any)
     * that matches that specification. Uses default comparator
     *
     * @param moduleName specification for a module name
     * @param os specification for a module Operating System
     * @param epicsVersion specification for a module EPICS base version
     * @return an {@link Optional} that might contain the latest module matching the specification
     */
    public Optional<Module> getLatestModuleForSpec(final String moduleName, final String os,
                                                   final Long epicsVersion) {
        return getLatestModuleForSpec(moduleName, os, epicsVersion, (a, b) -> ModuleVersionsComparator.compare(a,b));
    }



   /**
    * Retrieves a scanned {@link Module} instance given an exact module specification in terms name, OS, EPICS version
    * and module version, if it exists
    *
    * @param moduleName specification for a module name
    * @param os specification for the module Operating System
    * @param epicsVersion specification for the module EPICS base version
    * @param moduleVersion specification or the module version
    * @return an {@link Optional} containing a scanned {@link Module} instance
    */
    public Optional<Module> getModuleForSpec(final String moduleName, final String os,
            final Long epicsVersion, final String moduleVersion) {
            if (epicsVersion == null || moduleVersion == null || moduleName == null) {
                return Optional.empty();
            }

            return filterModulesByNameOsEpicsVersion(moduleName, os, epicsVersion).
                    filter(mod -> ModuleVersionsComparator.compare(mod.getModuleVersion(), moduleVersion) == 0).
                        findAny();
    }

    /**
     * Returns the module instance in the repository given a module with the same spec
     *
     * This is used because persisted modules do not contain information such as placeholders and dependencies.
     *
     * @param module
     * @return
     */
    public Optional<Module> getModuleFromRepo(Module module) {
        return getModuleForSpec(module.getName(), module.getOs(), module.getEpicsVersion(), module.getModuleVersion());
    }

    /**
     * Clears the repository (removes all data). Needed as we keep it in {@link SessionScoped} scope, we cannot replace
     * the reference once produced, but we can definitely clear all data.
     */
    public void clear() {
        epicsVersions.clear();
        osVersions.clear();
        moduleNames.clear();
        modulesByName.clear();
        messages.clear();
        error = false;
    }

    public Date getLastScanTime() { return lastScanTime != null ? new Date(lastScanTime.getTime()) : null; }
    public void setLastScanTime(Date lastScanTime) {
        this.lastScanTime = lastScanTime != null ? new Date(lastScanTime.getTime()) : null;
    }

    public String getLastScanUser() { return lastScanUser; }
    public void setLastScanUser(String lastScanUser) { this.lastScanUser = lastScanUser; }

    public List<RepositoryMessage> getMessages() { return messages; }

    /**
     * Returns true if an error has occurred while the repository was being scanned
     * @return
     */
    public boolean hasError() { return error; }

    /**
     * Setter for the error status as returned by {@link Repository#hasError()}
     * @param error
     */
    public void setError(boolean error) { this.error = error; }
}
