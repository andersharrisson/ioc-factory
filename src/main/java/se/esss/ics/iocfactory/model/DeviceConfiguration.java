/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import se.esss.ics.iocfactory.model.jsonserializers.DeviceSerializer;


/**
 * An entity bean used for storing and processing per device configuration within a given {@link Configuration} object
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Entity
@Table( name = "device_configuration",
        indexes = @Index(name = "idx_device_configuration_configuration", columnList = "configuration_id",
                unique = false)
)
@JsonSerialize(using = DeviceSerializer.class)
public class DeviceConfiguration implements
        Serializable, HasConsistencyStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "configuration_id", nullable = false, referencedColumnName = "id")
    private Configuration configuration;

    @Column(name = "device_name")
    private String name;

    @Embedded
    private Module supportModule;

    @OneToMany(mappedBy = "deviceConfiguration", cascade = { CascadeType.ALL }, orphanRemoval = true)
    private List<Parameter> parameters = new ArrayList<>();

    /** Uniquely identifies a device config. within a configuration */
    @Transient
    private int devicePosition = -1;

    @Transient
    private ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    @Transient
    private List<Snippet> snippets = Collections.emptyList();

    @Version
    @Column(name = "optimistic_version")
    private Long optimisticVersion;
    
    @Transient
    private IOCDevice ccdbDevice;

    /** Constructs an empty {@link DeviceConfiguration} instance */
    public DeviceConfiguration() {}

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String deviceName) { this.name = deviceName; }

    public Module getSupportModule() { return supportModule; }
    public void setSupportModule(Module supportModule) { this.supportModule = supportModule; }

    public List<Parameter> getParameters() { return parameters; }
    public void setParameters(List<Parameter> parameters) { this.parameters = parameters; }

    public Configuration getConfiguration() { return configuration; }
    public void setConfiguration(Configuration configuration) { this.configuration = configuration; }

    public int getDevicePosition() { return devicePosition; }
    public void setDevicePosition(int devicePosition) { this.devicePosition = devicePosition; }

    public List<Snippet> getSnippets() { return snippets; }
    public void setSnippets(List<Snippet> snippets) { this.snippets = snippets; }
    
    public IOCDevice getCcdbDevice() { return ccdbDevice; }
    public void setCcdbDevice(IOCDevice ccdbDevice) { this.ccdbDevice = ccdbDevice;}

    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }

    @Override
    public String toString() { return name; }

    @Override
    public int hashCode() { return Objects.hashCode(this.id); }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceConfiguration other = (DeviceConfiguration) obj;
        return Objects.equals(this.id, other.id);
    }
}
