/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * A set that has only two operations allowed, add and clear
 *
 * Maintains whether there has been an error consistency status added
 *
 * @param <E>
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConsistencyStatusSet
    implements Serializable {
    private final Set<ConsistencyStatus> csSet = new HashSet<>();
    private boolean error = false;

    /**
     * Add element to the set
     * see {@link Set#add(java.lang.Object)}
     *
     * @param val
     */
    public void add(ConsistencyStatus val) {
        csSet.add(val);
        if (!error) {
            error = val.getLevel() == ConsistencyMsgLevel.ERROR;
        }
    }

    /**
     * see {@link Set#clear()}
     */
    public void clear() {
        csSet.clear();
        error = false;
    }

    /**
     * Returns true if any of the elements of the set has {@link ConsistencyMsgLevel#ERROR} level
     * @return
     */
    public boolean isError() { return error; }

    /**
     * see {@link Set#size()}
     * @return
     */
    public int size() {
        return csSet.size();
    }

    /**
     * see {@link Set#isEmpty()}
     * @return
     */
    public boolean isEmpty() {
        return csSet.isEmpty();
    }

    /**
     * see {@link Set#contains(java.lang.Object)}
     * @param o
     * @return
     */
    public boolean contains(ConsistencyStatus o) {
        return csSet.contains(o);
    }

    /**
     * see {@link Set#containsAll(java.util.Collection)}
     * @param c
     * @return
     */
    public boolean containsAll(Collection<ConsistencyStatus> c) {
        return csSet.containsAll(c);
    }

    /**
     * see {@link Set#stream()}
     * @return
     */
    public Stream<ConsistencyStatus> stream() {
        return csSet.stream();
    }

    /**
     * see {@link Set#forEach(java.util.function.Consumer)
     * @param action
     */
    public void forEach(Consumer<? super ConsistencyStatus> action) {
        csSet.forEach(action);
    }
}
