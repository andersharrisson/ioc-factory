/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;


import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.ERROR;

/**
 * {@link ConsistencyStatus} enum definition for {@link Configuration} entities
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public enum ConfigurationConsistencyStatus implements ConsistencyStatus {
    INVALID_EPICS_VERSION("Configuration is for non-existant EPICS version", ERROR),
    INVALID_IOC("The IOC specified in the configuraton is not in the CCDB", ERROR),
    INVALID_OS_VERSION("Configuration is for non-existant OS version", ERROR),
    NO_ENVIRONMENTS_FOR_OS_EPICS("No environments for the given EPICS & OS version exist in EEE", ERROR),
    INVALID_ENV_VERSION("The EEE version is invalid for the OS & EPICS version", ERROR);

    private final String desc;
    private final ConsistencyMsgLevel level;

    private ConfigurationConsistencyStatus(String desc, ConsistencyMsgLevel level)
    {
        this.desc = desc;
        this.level = level;
    }

    public String getDesc() { return desc; }
    public ConsistencyMsgLevel getLevel() { return level; }
}
