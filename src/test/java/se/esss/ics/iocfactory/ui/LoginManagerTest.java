/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.Sets;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.injection.InjectionProvider;
import org.needle4j.injection.InjectionProviderInstancesSupplier;
import org.needle4j.injection.InjectionProviders;
import org.needle4j.junit.NeedleRule;
import org.needle4j.junit.NeedleRuleBuilder;
import se.esss.ics.iocfactory.service.DummySecurityService;

public class LoginManagerTest {
    private final DummySecurityService securityService = new DummySecurityService();


    @Rule
    public NeedleRule needleRule = (new NeedleRuleBuilder()).addSupplier(new InjectionProviderInstancesSupplier() {
        private Set<InjectionProvider<?>> providers;

        {
            providers = Sets.newHashSet(InjectionProviders.providerForInstance(securityService));
        }

        @Override
        public Set<InjectionProvider<?>> get() {
            return providers;
        }
    }).build();

    @ObjectUnderTest
    private LoginManager loginManager;

    @Test
    public void testSetGetUsername() {
        loginManager.setUsername("user");
        assertEquals("user", loginManager.getUsername());
    }

    @Test
    public void testSetGetPassword() {
        loginManager.setPassword("pass");
        assertEquals("pass", loginManager.getPassword());
    }

    @Test
    public void testIsLoggedIn() {
        assertEquals(false, loginManager.isLoggedIn());
    }

    /* Mocking problems with the MockingContext
    @Test
    public void testOnLoginOnLogout() {
        final FacesContext ctx = MockedFacesContext.setMockedContext();
        EasyMock.expect(ctx.isReleased()).andReturn(true);
        loginManager.setUsername("user");
        loginManager.setPassword("user12");
        loginManager.onLogin();
        assertTrue(securityService.isLoggedIn());

        EasyMock.expect(FacesContext.getCurrentInstance().isReleased()).andReturn(true);
        loginManager.onLogout();
        assertFalse(securityService.isLoggedIn());
    }

    @Test
    public void testOnLoginWithFail() {
        final FacesContext ctx = MockedFacesContext.setMockedContext();
        EasyMock.expect(ctx.isReleased()).andReturn(true);
        loginManager.setUsername("usedsadadr");
        loginManager.setPassword("usesadr12");
        loginManager.onLogin();
        assertFalse(securityService.isLoggedIn());
    } */
}
