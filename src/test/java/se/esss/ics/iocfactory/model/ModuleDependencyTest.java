/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import com.google.common.collect.ImmutableList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ModuleDependencyTest {

    @Test
    public void testEqualsAndHashcode() {
        ModuleDependency left = new ModuleDependency("asyn", "2.3.4", false);
        ModuleDependency right = new ModuleDependency("asyn", "2.3.4", false);

        assertEquals(left, right);
        assertEquals(left.hashCode(), right.hashCode());

        assertNotEquals(left, new ModuleDependency("asyn", "2.3.4", true));
        assertNotEquals(left, new ModuleDependency("asynasfd", "2.3.4", false));
        assertNotEquals(left, new ModuleDependency("asyn", "2.3.5", false));
        assertNotEquals(left, new ModuleDependency("asyn", "12.33.34", false));
    }

    @Test
    public void testIsSatisfiedBy() {
        Module refModule = new Module("asyn", "2.2.3", VersionConverters.getEpicsConverter().fromString("3.14.3"),
                "centos7", ImmutableList.of(), ImmutableList.of());

        Module difModule = new Module("asyn1", "2.2.3", VersionConverters.getEpicsConverter().fromString("3.14.3"),
                "centos7", ImmutableList.of(), ImmutableList.of());

        ModuleDependency dep = new ModuleDependency("asyn", "2.2.3", true);

        assertTrue(dep.isSatisfiedBy(refModule));
        assertFalse(dep.isSatisfiedBy(difModule));

        assertTrue((new ModuleDependency("asyn", "2.2.3", true).isSatisfiedBy(refModule)));
        assertTrue((new ModuleDependency("asyn", "2.2", true).isSatisfiedBy(refModule)));
        assertTrue((new ModuleDependency("asyn", "2", true).isSatisfiedBy(refModule)));
        assertTrue((new ModuleDependency("asyn", "2.2.3", false).isSatisfiedBy(refModule)));

        assertFalse((new ModuleDependency("asyn", "2.2.2", false).isSatisfiedBy(refModule)));

        assertFalse((new ModuleDependency("asyn", "3.2.3", true).isSatisfiedBy(refModule)));
        assertFalse((new ModuleDependency("asyn", "3.2", false).isSatisfiedBy(refModule)));
        assertFalse((new ModuleDependency("asyn", "3", false).isSatisfiedBy(refModule)));
    }
}
